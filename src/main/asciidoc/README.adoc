////
DO NOT MODIFY the README.adoc in the base directory.
Modify src/main/asciidoc/README.adoc, which is what the README.adoc
in the base directory is generated from. 
////
:toc:
[[connect-api-readme]]
= connect-api

:toc: left
:toc-title: Contents
:toclevels: 6

ifndef::env-gitlab[]
include::ADMINMANUAL.adoc[leveloffset=+1]

include::USERGUIDE.adoc[leveloffset=+1]

include::swagger.adoc[leveloffset=+1]
endif::[]


