/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Configuration;
import com.configureone.ws.GetConfigurationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author nkolker
 */
public class GetConfigurationResponseToConfigurationTransformer {
    private static final Logger LOGGER = LogManager.getLogger(GetConfigurationResponseToConfigurationTransformer.class);

    public Configuration transform(GetConfigurationResponse getConfigurationResponse) {
        return new Configuration(getConfigurationResponse.getGetConfigurationReturn());
    }
}
