/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.routes.groups;

import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRouteDefinition;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRoutesGroup;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRoutesGroupConfiguration;

//import com.configureone.integration.connect.api.routes.groups.GetProductsRoute;
import com.configureone.integration.connect.api.routes.groups.GetUsersRoute;

/**
 * ConnectRoutesGroup for the Salesforce routes. Provides static access to available Camel routes for Salesforce.
 * @author rmoutray
 */
public class APIConnectRoutes extends ConnectRoutesGroup {
    
    public static final ConnectRoutesGroupConfiguration GROUP = new ConnectRoutesGroupConfiguration("connect-api");
    //public static final GetProductsRoute GET_PRODUCTS_ROUTE = new GetProductsRoute(GROUP);
    public static final GetUsersRoute GET_USERS_ROUTE = new GetUsersRoute(GROUP);
    
    
    @Override
    public ConnectRouteDefinition[] getConnectRoutes() {
        return new ConnectRouteDefinition[] {  GET_USERS_ROUTE };
/*        
        return new ConnectRouteDefinition[] {CREATE_NEW_SF_PRODUCTS_FROM_C1_OPPORTUNITY_ROUTE, 
            CREATE_OPPORTUNITY_LINE_ITEMS_ROUTE, CREATE_PRICEBOOK_ENTRIES_FOR_LINE_ITEMS_ROUTE, 
            DELETE_ATTACHMENTS_FOR_QUOTE_ROUTE, DELETE_DOCUMENTS_FOR_QUOTE_ROUTE, DELETE_OPPORTUNITY_LINE_ITEMS_ROUTE, 
            GET_C1_DOCUMENT_CONTENTS_FOR_UPLOAD_ROUTE, GET_PRICEBOOK_FOR_OPPORTUNITY_ROUTE, 
            POPULATE_QUOTE_FROM_SFOPP_ID_ROUTE, SF_ATTACH_QUOTE_ROUTE, SF_CALL_WITH_AUTH_ROUTE, SF_COMPOSITE_REST_ROUTE, 
            SF_COMPOSITE_REST_VERIFY_RESULTS_ROUTE, SF_GET_ATTACHMENTS_FOR_OPPORTUNITY_ROUTE, 
            SF_GET_DOC_OPP_ID_FOR_PRIMARY_QUOTE_ROUTE, SF_GET_DOCUMENT_FOLDER_ID_ROUTE, SF_GET_DOCUMENTS_BY_NAME_ROUTE, 
            SF_GET_OPPORTUNITY_ROUTE, SF_GET_RESOURCE_PATH_ROUTE, SF_LARGE_BATCH_COMPOSITE_REST_ROUTE, 
            SF_MAP_QUOTE_TO_OPPORTUNITY_ROUTE, SF_OAUTH_TOKEN_INTERNAL_ROUTE, SF_OAUTH_TOKEN_ROUTE, 
            SF_PROCESS_PRODUCTS_ROUTE, SF_QUERY_FOR_SINGLE_REST_ENTITY_ROUTE, SF_QUERY_ROUTE, SF_RESOURCES_ROUTE, 
            SF_REST_ROUTE, SF_UPDATE_OPPORTUNITY_ROUTE, SYNCHRONIZE_PRICEBOOKS_WITH_LINE_ITEMS_ROUTE, 
            UPLOAD_QUOTE_ATTACHMENT_TO_SFROUTE, UPLOAD_QUOTE_DOC_TO_SFROUTE, UPDATE_SF_OPP_DIRECT_ROUTE,
            SF_LARGE_BATCH_COMPOSITE_REST_VERIFY_RESULTS_ROUTE, SF_FIRE_QUOTE_EVENT_ROUTE, 
            UPDATE_SF_DOC_OPP_VALIDITY_ROUTE,SF_CREATE_NEW_CONTENT_VERSION_ROUTE,SF_GET_CONTENT_VERSION_DATA_ROUTE,SF_CONTENT_DOCUMENT_LINK_ROUTE,
            SF_DELETE_CONTENT_DOCUMENT_ROUTE,SF_QUERY_OPPORTUNITY_CONTENT_DOCUMENT_LINK_ROUTE,SF_QUERY_CONTENT_DOCUMENT_ID_ROUTE,SF_TOOLING_API_CALL_ROUTE,SF_GET_INTEGRATION_USER_ROUTE,
            SF_REFRESH_INTEGRATION_USER_ROUTE, SF_AUTH_TEST_ROUTE, SF_DOCUMENT_REQUEST_ROUTE};
*/
    }

    @Override
    public ConnectRoutesGroupConfiguration getConnectRoutesGroupConfiguration() {
        return GROUP;
    }
 
}
