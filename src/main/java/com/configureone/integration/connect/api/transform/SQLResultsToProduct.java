/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Product;

import java.util.Map;
import java.util.List;

import org.apache.camel.Handler;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author rolson
 */
public class SQLResultsToProduct{

    @Handler
    public Product transform(List<Map> body) throws Exception {
        Product product = null;
        if (!CollectionUtils.isEmpty(body)) {
            product = ProductMapper.mapRowToProduct(body.get(0));
        }
        return product;
    }
}