/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.orders.routes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.processors.SetInToOutProcessor;
import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Order;
import com.configureone.integration.connect.core.model.OrderStatusRequest;
import com.configureone.integration.connect.core.model.OrderStatusResponse;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.api.transform.GetOrderResponseToOrderTransformer;
import com.configureone.integration.connect.core.model.OrderDetail;
import com.configureone.ws.GetOrder;
import com.configureone.ws.GetOrderResponse;

import com.configureone.ws.UpdateOrder;
import com.configureone.ws.UpdateOrderResponse;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
//import org.apache.camel.component.gson.GsonDataFormat;
import org.apache.camel.dataformat.soap.name.QNameStrategy;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class OrderRouteBuilder extends RouteBuilder {
   
    protected Logger logger = LogManager.getLogger(getClass());
    
    SoapJaxbDataFormat soapGetOrder;
    SoapJaxbDataFormat soapGetOrderResponse;
    SoapJaxbDataFormat soapUpdateOrder;
    SoapJaxbDataFormat soapUpdateOrderResponse;

    public static final String ORDER_NUM_EXCHANGE_HEADER = "on";
    public static final String DESIGN_ID_EXCHANGE_HEADER = "di";
    public static final String SMARTPART_NUM_EXCHANGE_HEADER = "spn";
    private final ConnectExchangeProperty<List<Map<String, Object>>> queryResult = new ConnectExchangeProperty("sqlOrderQueryResult", List.class);
    private final ConnectExchangeProperty<Order> parentOrder = new ConnectExchangeProperty("parentOrder", Order.class);

    
    @Override
    public void configure() throws Exception {
        
        errorHandler(noErrorHandler());
        onException(Exception.class)
            .process(new OnExceptionProcessor());

        
        soapGetOrder = new SoapJaxbDataFormat(GetOrder.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getOrder")));
        soapGetOrderResponse = new SoapJaxbDataFormat(GetOrderResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getOrderResponse")));
        soapUpdateOrder = new SoapJaxbDataFormat(GetOrder.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "updateOrder")));
        soapUpdateOrderResponse = new SoapJaxbDataFormat(GetOrderResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "updateOrderResponse")));
        //GsonDataFormat gsonDataFormat = new GsonDataFormat(Order.class);
    
        // Enter route definition here
        getOrderByOrderNumber();
        enrichOrderDetailWithProductId();
        updateOrderStatus();
    }


    protected void getOrderByOrderNumber() throws Exception {
        from("direct-vm:getOrderByOrderNumber").routeId("getOrderByOrderNumberDirectVM")
                .streamCaching()
                .process(this::getOrderSOAPProcessor)
                // Format into a SOAP message using the standard Soap DataFormat
                // provided with camel
                .marshal(soapGetOrder)
                //.to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
                .to("direct-vm:callConfigureOneDirectVM")
                .convertBodyTo(String.class)
                .process(this::checkForHttpResponseErrors)
                .removeHeaders("CamelHttp*")
                .choice()
                    .when(simple("${body} == '' || ${body} == '[]' || ${body} contains 'SERIAL_NUMBER xsi:nil'"))
                        .log(LoggingLevel.INFO, "getOrder response was empty")
                        .setProperty("errorCode", simple("404"))
                        .setProperty("errorMessage", simple("Order not found"))
                        .process(this:: nullResponse)
                    .endChoice()
                    .otherwise()
                        .log(LoggingLevel.INFO, "getOrderBySerialNumber: order message contains data")
                        .process(this::pullGetOrderResponse)
                        .unmarshal(soapGetOrderResponse)
                        //Turn the GetOrderResponse into our standard order object.
                        .log(LoggingLevel.INFO, "getOrderBySerialNumber: calling bean - GetOrderResponseToOrderTransformer")
                        .bean(GetOrderResponseToOrderTransformer.class, "transform(${body})")
                        .setProperty("orderClass",simple("${body}"))
                        .log(LoggingLevel.INFO, "getOrderBySerialNumber: finished processor")
                        //.marshal(gsonDataFormat)
                        //.process(this::jsonbody)
                        //.to("file:C:/cca-4.0.11-temp/connect-advanced-4.0.11-SNAPSHOT/bin?fileName=ordered.txt")
                        .setBody(simple("${exchangeProperty.orderClass}",Order.class))
                        //.log(LoggingLevel.INFO, "getOrderBySerialNumber: call enritch")

                        //orderStatusCd = 'FM', orderProcessCd = 1
                        .enrich("sql:SELECT QUOTE_VALID FROM CO_DOC WHERE orderNumber = :#${header." + ORDER_NUM_EXCHANGE_HEADER + "}?dataSource=c1SQLDataSource", this::setSqlResultToPropertyAggregator)
                        //.log(LoggingLevel.INFO, "getOrderBySerialNumber: enritch finish")

                        .process(this::appendSqlToObjProcessor)
                        // Split out OrderDetail objects to enrich with the productid
                        .split(simple("${body.getDetails}"), this::orderDetailAggregator).parallelProcessing().shareUnitOfWork()
                            .to("direct-vm:enrichOrderDetailWithProductId")
                        .end()
                        .process(this::setOrderDetailOnOrder)
                        .removeHeaders("*") // TODO: Figure out what we need to remove
                        .process(new SetInToOutProcessor())
                    .endChoice()
                .end()
                ;  
    }
    
      /**
     * 
     * Route to enrich the Order details with a 
     * @throws java.lang.Exception
     */
    protected void enrichOrderDetailWithProductId() throws Exception {
        from("direct-vm:enrichOrderDetailWithProductId").routeId("enrichOrderDetailWithProductId")
            .choice()
                .when(simple("${body.getType} == 'C'"))
                    .process(this::setDesignIdAndSmartPartNumProcessor)
                    .enrich("sql:SELECT PRODUCT_ID FROM CO_DES WHERE DESIGN_ID = :#${header." + DESIGN_ID_EXCHANGE_HEADER + "}?dataSource=c1SQLDataSource", this::setProductIdAggregator);
    }


    private void updateOrderStatus() throws Exception {
        from("direct-vm:updateOrderStatus").routeId("updateOrderStatusDirectVM")
                .removeHeaders("*")
                .log("Received updateOrderStatus request.")
                .process(this::createUpdateOrderStatusSOAPRequestProcessor)

                // Format into a SOAP message using the standard Soap DataFormat
                .marshal(soapUpdateOrder)
                .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
                .process(this::checkForHttpResponseErrors)
                .removeHeaders("*")
                .unmarshal(soapUpdateOrderResponse)
                .process(this::setOrderUpdateStatusResponse);
    }

    protected Exchange setOrderDetailOnOrder(Exchange exchange) {
        Order q = parentOrder.getPropertyData(exchange);
        List<OrderDetail> qdlist = exchange.getIn().getBody(List.class);
        q.setDetails(qdlist);
        exchange.getIn().setBody(q);
        return exchange; 
    }
    
    
 /**
     * Processor to Create a GetOrder object from the message header.
     * @param exchange
     * @throws Exception
     */
    protected void getOrderSOAPProcessor(Exchange exchange) throws Exception {
        String serialNumber =
                exchange.getIn().getHeader(ORDER_NUM_EXCHANGE_HEADER,
                        String.class);

        GetOrder getOrder = new GetOrder();
        getOrder.setIn0(serialNumber);
        Message inMsg = exchange.getIn();
        inMsg.setBody(getOrder);
    }
    
    protected void nullResponse(Exchange exchange) {
        String errorCodeString = (String) exchange.getProperty("errorCode");
        int errorCode = Integer.parseInt(errorCodeString);
        String errorMessage = (String) exchange.getProperty("errorMessage");

        RestErrorResponse rer = new RestErrorResponse(errorCode, errorMessage);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }
    protected void checkForHttpResponseErrors(Exchange exchange) throws ConnectBusinessException, HttpException {
        int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
        String body = exchange.getIn().getBody(String.class);

        if (responseCode >= 300) {
            exchange.getIn().removeHeaders("CamelHttp*");
            exchange.getIn().setBody(null);
            throw new HttpException(body);
        }
    }
    protected Exchange pullGetOrderResponse (Exchange exchange) {
        String newbody = "";
        try {
            String msgbody = exchange.getIn().getBody(String.class);
            newbody = msgbody.replaceAll("http://model.ws.configureone.com","http://ws.configureone.com");
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        exchange.getIn().setBody(newbody);

        return exchange;
    }

    protected Exchange setDesignIdAndSmartPartNumProcessor(Exchange exchange) {
        OrderDetail qd = exchange.getIn().getBody(OrderDetail.class);
        long designid = Long.parseLong(qd.getId());
        String smartpartnum = qd.getSmartpartnum();
        exchange.getIn().setHeader(DESIGN_ID_EXCHANGE_HEADER, designid);
        exchange.getIn().setHeader(SMARTPART_NUM_EXCHANGE_HEADER, smartpartnum);
        return exchange;
    }
    protected Exchange setProductIdAggregator(Exchange original, Exchange resource) {
        List<Map<String,Object>> sqlresult = resource.getIn().getBody(List.class);
        // if (sqlresult.size() == 0 || sqlresult.size() > 1) {
        //     throw new Exception("Could not retrieve additional order detail information from C1 database. [" + sqlresult.size() + "] rows received from database, but expected 1.");
        // }
        OrderDetail od = original.getIn().getBody(OrderDetail.class);
        if (sqlresult.size() >= 1) {
            od.setProductid(((Integer)sqlresult.get(0).get("PRODUCT_ID")).longValue());
        }
        original.getIn().setBody(od);
        return original;
    }

    protected Exchange orderDetailAggregator (Exchange oldex, Exchange newex) {
        List<OrderDetail> newexbody = (oldex == null ? new ArrayList() : oldex.getIn().getBody(List.class));
        newexbody.add(newex.getIn().getBody(OrderDetail.class));
        newex.getIn().setBody(newexbody);
        return newex;
    }

    protected Exchange setSqlResultToPropertyAggregator(Exchange original, Exchange resource) {
        queryResult.setPropertyData(original, resource.getIn().getBody(List.class));
        return original;
    }
    
    protected Exchange appendSqlToObjProcessor(Exchange exchange) throws Exception {
        List<Map<String, Object>> sqlResult = queryResult.getPropertyData(exchange);
        if (sqlResult == null) {
            throw new Exception("Could not retrieve additional order information from C1 database. SQL result was not present.");
        }
        if (sqlResult.size() != 1) {
            throw new Exception("Could not retrieve additional order information from C1 database. [" + sqlResult.size() + "] rows received from database, but expected 1.");
        }
        boolean quoteValid = (boolean) sqlResult.get(0).get("QUOTE_VALID");
        
        Order orderObj = exchange.getIn().getBody(Order.class);
        orderObj.setValid(quoteValid);

        // Store the order object 
        parentOrder.setPropertyData(exchange, orderObj);

        return exchange;
    }


    // Create the request
    private void createUpdateOrderStatusSOAPRequestProcessor(Exchange exchange) throws Exception {
        OrderStatusRequest statusReq = exchange.getIn().getBody(OrderStatusRequest.class);
        UpdateOrder updateOrder = new UpdateOrder();

        updateOrder.setORDERNUMBER(statusReq.getOrderNumber());
        updateOrder.setREFERENCENUMBER(statusReq.getReferenceNumber());
        updateOrder.setSTATUS(statusReq.getStatus());

        logger.debug("Order update request = "+updateOrder.toString());
        exchange.getIn().setBody(updateOrder);
    }

    // Update order response
    private void setOrderUpdateStatusResponse(Exchange exchange){
        UpdateOrderResponse orderResponse = exchange.getIn().getBody(UpdateOrderResponse.class);

        OrderStatusResponse statusRes = new OrderStatusResponse();
        statusRes.setMessage(orderResponse.getUpdateOrderReturn().getMessage().getValue());
        if(orderResponse.getUpdateOrderReturn().getKey() !=null ) {
            statusRes.setKey(orderResponse.getUpdateOrderReturn().getKey().getValue());
        }
        statusRes.setSuccess(String.valueOf(orderResponse.getUpdateOrderReturn().isSuccess()));

        exchange.getIn().setBody(statusRes);
    }
}
