/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Product;
import java.util.Map;

/**
 *
 * @author rolson
 */
public class ProductMapper {
    
    /**
     * Mapping for the results from the SQL query to a Product object. 
     * @param row A Map object from a List&lt;Map&gt; result set from the SQL component
     * @return The mapped product object
     */
    public static Product mapRowToProduct (Map row) {
        Product product = new Product();
        
        product.setPRODUCT_ID(((Integer)row.get("PRODUCT_ID")).toString());
        product.setPRODUCT_DESC((String)row.get("PRODUCT_DESC"));
        product.setTEMPLATE_INPUT_FILE((String)row.get("TEMPLATE_INPUT_FILE"));
        product.setPRODUCT_SCRIPT_FILE((String)row.get("PRODUCT_SCRIPT_FILE"));
        product.setPRODIN_PIXEL_WIDTH((String)row.get("PRODIN_PIXEL_WIDTH"));
        product.setSHOW_PREV_UPCO_OPTIONS((String)row.get("SHOW_PREV_UPCO_OPTIONS"));
        product.setPRODUCT_REV_LEVEL_NUM((String)row.get("PRODUCT_REV_LEVEL_NUM"));
        product.setPRODUCT_IMG_FILE((String)row.get("PRODUCT_IMG_FILE"));
        product.setWATERMARK_IMG_FILE((String)row.get("WATERMARK_IMG_FILE"));
        product.setWATERMARK_LOCATION((String)row.get("WATERMARK_LOCATION"));
        product.setCOLLAB_INTRO((String)row.get("COLLAB_INTRO"));
        product.setCOLLAB_NOTIF((String)row.get("COLLAB_NOTIF"));
        product.setENFORCE_BUILD_ZERO((String)row.get("ENFORCE_BUILD_ZERO"));
        product.setDESIGN_INTERFACE_MSG((String)row.get("DESIGN_INTERFACE_MSG"));
        product.setSHOW_ALL_INPUT_OPTIONS((String)row.get("SHOW_ALL_INPUT_OPTIONS"));
        product.setDESIGN_INTERFACE_PRICING((String)row.get("DESIGN_INTERFACE_PRICING"));
        product.setBASE_PRICE((String)row.get("BASE_PRICE"));
        product.setBOM_TYPE_CD((String)row.get("BOM_TYPE_CD"));
        product.setOUTPUT_TYPE_CD((String)row.get("OUTPUT_TYPE_CD"));
        product.setPRICING_HEADER((String)row.get("PRICING_HEADER"));
        product.setPRICING_FACTOR((String)row.get("PRICING_FACTOR"));
        product.setPRICING_CD((String)row.get("PRICING_CD"));
        product.setUPLOADING_DOCS_INSTR((String)row.get("UPLOADING_DOCS_INSTR"));
        product.setUPCO_OPTIONS_TXT((String)row.get("UPCO_OPTIONS_TXT"));
        product.setPREV_OPTIONS_TXT((String)row.get("PREV_OPTIONS_TXT"));
        product.setTAB_FIELD_LABEL_HEADER((String)row.get("TAB_FIELD_LABEL_HEADER"));
        product.setTAB_FIELD_DESC_HEADER((String)row.get("TAB_FIELD_DESC_HEADER"));
        product.setTAB_FIELD_VALUE_HEADER((String)row.get("TAB_FIELD_VALUE_HEADER"));
        product.setTAB_FIELD_LABEL_JUSTIFY((String)row.get("TAB_FIELD_LABEL_JUSTIFY"));
        product.setTAB_FIELD_DESC_JUSTIFY((String)row.get("TAB_FIELD_DESC_JUSTIFY"));
        product.setTAB_FIELD_VALUE_JUSTIFY((String)row.get("TAB_FIELD_VALUE_JUSTIFY"));
        product.setTAB_FIELD_DISPLAY_IND((String)row.get("TAB_FIELD_DISPLAY_IND"));
        product.setGENERATE_ROUTINGS((String)row.get("GENERATE_ROUTINGS"));
        product.setDOC_INTERFACE_BOM_ID(((Integer)row.get("DOC_INTERFACE_BOM_ID")).toString());

        if (row.get("saleable") == null) {
            row.put("saleable", "false");
        }
        product.setSaleable(String.valueOf(Boolean.valueOf(row.get("saleable").toString())));

        product.setShortDesc((String)row.get("shortDesc"));
        product.setLongDesc((String)row.get("longDesc"));
        Integer eCatBOM = (Integer) row.get("eCatBOM");
        product.seteCatBOM(eCatBOM != null ? eCatBOM.toString() : null);
        product.setKeyword((String)row.get("keyword"));

        if (row.get("QEActive") == null) {
            row.put("QEActive", "false");
        }

        product.setQEActive(String.valueOf(Boolean.valueOf(row.get("QEActive").toString())));

        product.setGlobal_ind(((Short)row.get("global_ind")).toString());
        product.setAssignedItem((String)row.get("AssignedItem"));
        product.setCONFIRM_DESIGN_TXT((String)row.get("CONFIRM_DESIGN_TXT"));
        
        return product;
    }
}
