/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Handler;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author rolson
 */
public class SQLResultsToProductsList {

    @Handler
    public List<Product> transform(List<Map> body) throws Exception {
        List<Product> productlist = new ArrayList<>();
        if (!CollectionUtils.isEmpty(body)) {
            body.forEach((row) -> {
                productlist.add(ProductMapper.mapRowToProduct(row));
            });
        }
        return productlist;
    }
}