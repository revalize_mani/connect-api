/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.search.routes;

import com.configureone.integration.connect.core.properties.ConnectCoreProperties;
import com.configureone.integration.connect.core.properties.RestartBundleOnExternalPropertyUpdateActivator;

/**
 *  Bundle activator used to register this bundle with karaf so when the core
 *  properties file is updated, the bundle will be restarted with the new
 *  property values.
 */
public class ProjectSpecificActivator extends RestartBundleOnExternalPropertyUpdateActivator {

    /**
     * Default constructor used to register an event listener for the core
     * properties.
     */
    public ProjectSpecificActivator () {
        super(ConnectCoreProperties.class);
    }
}
