/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.customers.routes;

import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.routes.builders.helpers.C1SOAPEntityRepositoryRouteBuilder;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Contact;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.integration.connect.core.services.ConnectRoute;
import com.configureone.ws.Address;
import com.configureone.ws.Customer;
import com.configureone.ws.GetCustomer;
import com.configureone.ws.GetCustomerResponse;
import com.configureone.ws.ObjectFactory;
import com.configureone.ws.ProcessCustomer;
import com.configureone.ws.ProcessCustomerResponse;
import com.configureone.ws.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.xml.namespace.QName;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world! route provided with the archetype.  This is used as a
 * placeholder for the routes package.  It is also used in some example unit
 * tests.
 */
public class ContactsRouteBuilder extends RouteBuilder {

    /**
     * Logger for the class.
     */
    protected final Logger logger = LogManager.getLogger(getClass());
    
    /**
     * Connect-core shared properties.
     */
    @BeanInject("connectCoreSharedProperties")
    public ConnectCorePropertiesService connectCoreProperties;
    
    public static final String CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER = "contactRefNum";
    ConnectExchangeProperty<String> custRefNum = new ConnectExchangeProperty("custRefNum", String.class);
    ConnectExchangeProperty<String> custRefNumForCreate = new ConnectExchangeProperty("custRefNumForCreate", String.class);
    public static final ConnectRoute GET_CONTACT_ROUTE = new ConnectRoute("direct-vm:getContactByAddressRefNum", "direct-vm:getContactByAddressRefNum", "getContactByAddressRefNumDirectVM");
    /**
     * The route that merely performs the process customer action for an update, without checking first if the 
     * customer actually exists. If the customer does not exist, it will be created.
     */
    public static final ConnectRoute PROCESS_ADD_CONTACT_ROUTE_INTERNAL = new ConnectRoute("direct-vm:processContactInternal", "direct-vm:processContactInternal", "processContactDirect");
    /**
     * The ConnectRoute used to add a contact in Configure One. This route first checks if a customer 
     * exists, before calling the internal route to process the update.
     */
    public static final ConnectRoute ADD_CONTACT_ROUTE = new ConnectRoute("direct-vm:processContact", "direct-vm:processContact", "processContactDirectVM");
    /**
     * The route that merely performs the process contact action for an update, without checking first if the 
     * contact actually exists. If the contact does not exist, it will be created.
     */
    public static final ConnectRoute PROCESS_UPDATE_CONTACT_ROUTE_INTERNAL = new ConnectRoute("direct-vm:processContactUpdateInternal", "direct-vm:processContactUpdateInternal", "processContactUpdateInternal");
    /**
     * The ConnectRoute used to update an existing contact in Configure One. This route first checks if a contact 
     * exists, before calling the internal route to process the update.
     */
    public static final ConnectRoute UPDATE_CONTACT_ROUTE = new ConnectRoute("direct-vm:updateContact", "direct-vm:updateContact", "updateContactDirectVM");
    
    /**
     * Configuration of the connect-contacts routes.
     * @throws  Exception   Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(noErrorHandler());
        onException(Exception.class)
            .process(new OnExceptionProcessor());
                
        QName getCustomerQName = new QName("http://ws.configureone.com", "getCustomer");
        QName getCustomerResponseQName = new QName("http://ws.configureone.com", "getCustomerResponse");
        QName processCustomerQName = new QName("http://ws.configureone.com", "processCustomer");
        QName processCustomerResponseQName = new QName("http://ws.configureone.com", "processCustomerResponse");
        
        C1SOAPEntityRepositoryRouteBuilder soapRouteBuilder = new C1SOAPEntityRepositoryRouteBuilder(this, connectCoreProperties);
        
        soapRouteBuilder.getC1SOAPEntityByIDWithHeadersFunction(GET_CONTACT_ROUTE, Contact.class, GetCustomer.class, 
                GetCustomerResponse.class, getCustomerQName, getCustomerResponseQName, 
                CustomersRouteBuilder::generateGetCustomerSOAPObjFromHeaderRefNum, this::getContactByRefNumFromCustomer);
        
        from(ADD_CONTACT_ROUTE.getDefaultURI()).routeId(ADD_CONTACT_ROUTE.getRouteID())
            .enrich("direct-vm:verifyCustomerExists", (Exchange original, Exchange resource) -> original)
            .to(PROCESS_ADD_CONTACT_ROUTE_INTERNAL.getConfiguredURI());
        
        soapRouteBuilder.addC1SOAPEntity(PROCESS_ADD_CONTACT_ROUTE_INTERNAL, GET_CONTACT_ROUTE, Contact.class, ProcessCustomer.class, 
                ProcessCustomerResponse.class, processCustomerQName, processCustomerResponseQName, 
                this::createProcessCustomerAddressSOAPRequest, this::parseProcessCustomerResponseProcessor);
        
        from(UPDATE_CONTACT_ROUTE.getDefaultURI()).routeId(UPDATE_CONTACT_ROUTE.getRouteID())
            .enrich("direct-vm:verifyContactExists",  (Exchange original, Exchange resource) -> original)
            .to(PROCESS_UPDATE_CONTACT_ROUTE_INTERNAL.getConfiguredURI());
        
        from("direct-vm:verifyContactExists").routeId("verifyContactExistsDirect")
            .to(GET_CONTACT_ROUTE.getConfiguredURI())
            .process(this::verifyContactResultExists);
        
        soapRouteBuilder.addC1SOAPEntity(PROCESS_UPDATE_CONTACT_ROUTE_INTERNAL, GET_CONTACT_ROUTE, Contact.class, ProcessCustomer.class, 
                ProcessCustomerResponse.class, processCustomerQName, processCustomerResponseQName, 
                this::createUpdateCustomerAddressSOAPRequest, this::parseProcessCustomerResponseProcessor);
    }
    
    private void verifyContactResultExists(Exchange exchange) throws Exception {
        if (exchange.getIn().getBody() == null) {
            throw new Exception("Error attempting to retrive existing object. Message body was null.");
        }
        
        Optional<Contact> opt = exchange.getIn().getBody(Optional.class);
        
        if (opt == null) {
            throw new Exception("Error attempting to retrive existing object. Message body was not of the expected type "
                + "(Optional).");
        }
        
        if (!opt.isPresent()) {
            throw new ConnectBusinessException(Contact.class.getSimpleName() + " does not exist, and so cannot be updated.");
        }
        
        if (opt.isPresent() && !Contact.class.isAssignableFrom(opt.get().getClass())) {
            throw new Exception("Object was retrieved but an Optional of an unexpected type ["
                + opt.get().getClass() + "]. It must be assignable to [" + Contact.class + "]");
        }
        
        
    }
    
    /**
     * Creates a ProcessCustomer object from a Contact object.
     * @param contact The contact object to use.
     * @return The ProcessCustomer object
    */
    protected ProcessCustomer createProcessCustomerAddressSOAPRequest(Contact contact, Map<String, Object> headers) throws ConnectBusinessException {
        
        String custRefNumHeaderVal = (String) headers.get(CustomersRouteBuilder.CUST_REF_NUM_EXCHANGE_HEADER);
        
        if (contact != null && !StringUtils.isEmpty(contact.getCustrefnum()) && !StringUtils.equals(contact.getCustrefnum(), custRefNumHeaderVal)) {
            throw new ConnectBusinessException("Field [custrefnum] provided in object ([" + contact.getCustrefnum() + "]) "
                + "must be blank or match the one provided by the header value ([" + custRefNumHeaderVal + "]).");
        }
        
        if (contact != null && contact.getTypecd() == null) {
            throw new ConnectBusinessException("Field [typecd] is required and must be provided.");
        }
        
        if (contact != null && !StringUtils.isEmpty(contact.getAddrrefnum())) {
            throw new ConnectBusinessException("Field [addrrefnum] must be blank, as it will be generated automatically.");
        }
        
        ObjectFactory objectFactory = new ObjectFactory();
        ProcessCustomer processCust = objectFactory.createProcessCustomer();
        Customer cust = objectFactory.createCustomer();
        cust.setCUSTREFNUM(custRefNumHeaderVal);
        contact.setCustrefnum(custRefNumHeaderVal);
               
        cust.getAddress().add(contact.toAddress());
        processCust.setIn0(cust);
        return processCust;
    }
    
    /**
     * Creates a ProcessCustomer object from a Contact object to update it.
     * @param contact The updated {@link Address} object to use
     * @param headers Which must include the headers {@link CustomersRouteBuilder#CUST_REF_NUM_EXCHANGE_HEADER} and {@link #CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER}
     * @return The {@link ProcessCustomer} object to use for the update request
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException
    */
    protected ProcessCustomer createUpdateCustomerAddressSOAPRequest(Contact contact, Map<String, Object> headers) throws ConnectBusinessException {
        
        String custRefNumHeaderVal = (String) headers.get(CustomersRouteBuilder.CUST_REF_NUM_EXCHANGE_HEADER);
        String addrRefNumHeaderVal = (String) headers.get(CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER);
        
        if (custRefNumHeaderVal == null || StringUtils.isEmpty(custRefNumHeaderVal)) {
            throw new ConnectBusinessException("Customer Reference Number must be provided in Header [" + CustomersRouteBuilder.CUST_REF_NUM_EXCHANGE_HEADER + "].");
        }
        
        if (addrRefNumHeaderVal == null || StringUtils.isEmpty(addrRefNumHeaderVal)) {
            throw new ConnectBusinessException("Address Reference Number must be provided in Header [" + CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER + "].");
        }
        
        if (contact != null && !StringUtils.isEmpty(contact.getCustrefnum()) && !StringUtils.equals(contact.getCustrefnum(), custRefNumHeaderVal)) {
            throw new ConnectBusinessException("Field [custrefnum] in object ([" + contact.getCustrefnum() + "]) "
                + "must be blank or match the one provided by the header value ([" + custRefNumHeaderVal + "]).");
        }
        
        if (contact != null && !StringUtils.isEmpty(contact.getAddrrefnum()) && !StringUtils.equals(contact.getAddrrefnum(), addrRefNumHeaderVal)) {
            throw new ConnectBusinessException("Field [addrrefnum] in object ([" + contact.getAddrrefnum() + "]) "
                + "must be blank or match the one provided by the header value ([" + addrRefNumHeaderVal + "]).");
        }
        
        ObjectFactory objectFactory = new ObjectFactory();
        ProcessCustomer processCust = objectFactory.createProcessCustomer();
        Customer cust = objectFactory.createCustomer();
        cust.setCUSTREFNUM(custRefNumHeaderVal);
        contact.setCustrefnum(custRefNumHeaderVal);
        contact.setAddrrefnum(addrRefNumHeaderVal);
        
        cust.getAddress().add(contact.toAddress());
        processCust.setIn0(cust);
        return processCust;
    }
    
    /**
     * Method used to find the specific contact we're looking for inside the GetCustomerResponse object.
     * @param getCustomerResponse The GetCustomerResponse object that contains our address
     * @param headers The message headers, containing {@link #CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER} header with value.
     * @throws Exception when GetCustomerResponse is null.
     * @return The found address from the Customer.
     */
    protected Contact getContactByRefNumFromCustomer(GetCustomerResponse getCustomerResponse, Map<String, Object> headers) throws Exception {
        String requestedAddressRefNum = (String) headers.get(CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER);
        logger.info("ADDRESS_REF_NUM TO FIND: " + requestedAddressRefNum);
        if (getCustomerResponse == null) {
            throw new Exception("Unexpected Null response from Configure One.");
        }
        if (getCustomerResponse.getGetCustomerReturn() == null
                    || getCustomerResponse.getGetCustomerReturn().getAddress() == null) {
            return null;
        }
        
        logger.info("ADDRESS COUNT: " + getCustomerResponse.getGetCustomerReturn().getAddress().size());
        for (Address address : getCustomerResponse.getGetCustomerReturn().getAddress()) {
            if (address != null && address.getADDRREFNUM() != null && StringUtils.equals(requestedAddressRefNum, address.getADDRREFNUM())) {
                logger.info("ADDRESS MATCH: " + address.getADDRREFNUM());
                return new Contact(address);
            }
            else {
                logger.info("ADDRESS NO MATCH: " + address != null ? address.getADDRREFNUM() : null + "");
            }
        }
        return null;
    }
        
    /**
     * Processes the customer response returned by configure one. Sets the ref num on the exchange if successful.
     * @param pcr
     * @return
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException If the soap response is empty or 
     * fails to provided a created ID.
     */
    protected Message parseProcessCustomerResponseProcessor(ProcessCustomerResponse pcr) throws ConnectBusinessException {
        Response processCustomerReturn = pcr.getProcessCustomerReturn();
        boolean success = processCustomerReturn.isSuccess();
        String message = processCustomerReturn.getMessage().getValue();
        String key = processCustomerReturn.getKey().getValue();
        
        //Creating a new message to use to get the Contact from the GetContact Route. Requires a 
        //customer ID and a customer contact address ID.
        Message getContactMessage = new DefaultMessage(this.getContext());
        
        //The returned Key from Configure One is a list of IDs separated by the '||' characters. So we parse that into a list.
        List<String> customerReturnRefIDs = Arrays.asList(key.split("\\|\\|"));
        //In that list, the 2nd element represents the customer ref num, so we get that and set it on the 
        //required header of the message we're creating. Otherwise throw an exception.
        if (success && customerReturnRefIDs != null && customerReturnRefIDs.size() >= 2 && customerReturnRefIDs.get(1) != null && customerReturnRefIDs.get(1) != "null") {
            logger.info("Success.");
            String createdCustomerRefNum = customerReturnRefIDs.get(1);
            getContactMessage.setHeader(CustomersRouteBuilder.CUST_REF_NUM_EXCHANGE_HEADER, createdCustomerRefNum);
        } else {
            ConnectBusinessException cbe = new ConnectBusinessException("Error from Configure One while attempting to process request. See log for details.", "MESSAGE: " + message + "KEY: " + key);
            logger.error(cbe.toString());
            throw cbe;
        }
        
        //The returned 'Message" from Configure One is a list separated by the '|' character. So we parse that into a list.
        List<String> custAddressReturnMessage = Arrays.asList(message.split("\\|"));
        //In that list, the 2nd element contains a message with the customer contact address id, 
        //so we get that and set it on the required header of the message we're creating. Otherwise throw an exception.
        if (success && custAddressReturnMessage != null && custAddressReturnMessage.size() >= 2 && custAddressReturnMessage.get(1) != null && custAddressReturnMessage.get(1) != "null") {
            logger.info("Success: " + message);
            String createdAddressIDMessageText = custAddressReturnMessage.get(1);
            String messageTextPrefix = "Address (";
            String messageTextSuffix = ") Created Successfully";
            String createdCustAddressID = createdAddressIDMessageText.substring(messageTextPrefix.length(), createdAddressIDMessageText.length() - messageTextSuffix.length());
            
            getContactMessage.setHeader(CONTACT_ADDRESS_REF_NUM_EXCHANGE_HEADER, createdCustAddressID);
            
        } else {
            ConnectBusinessException cbe = new ConnectBusinessException("Error from Configure One while attempting to process request. See log for details.", "MESSAGE: " + message + "KEY: " + key);
            logger.error(cbe.toString());
            throw cbe;
        }
        
        return getContactMessage;
    }
    
    /**
     * Checks an exchange containing a SQL response for errors (more than 1 result retrieved).
     * If there are errors present, an exception is thrown.
     * @param exchange The exchange to check.
     * @throws IllegalStateException if multiple records were in the list.
     */
    protected void verifySingleSQLResult(Exchange exchange) throws IllegalStateException {
        List<Map> sqlResultMaps = exchange.getIn().getBody(List.class);
        
        if (sqlResultMaps != null && sqlResultMaps.size() > 1) {
            throw new IllegalStateException("Multiple records were found with the same ID.");
        }
    }
    
    /**
     * Checks an exchange containing an HTTP response for errors (response code not in the 200s or body is null/empty).
     * If there are any present, it is logged and the exchange is set to a 500 error with a json error response body.
     * @param exchange The exchange to check.
     * @throws org.apache.http.HttpException If the response is greater than 299
     */
    protected void checkForHttpResponseErrors(Exchange exchange) throws HttpException {
        int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
        String body = exchange.getIn().getBody(String.class);

        if (responseCode >= 300 || StringUtils.isEmpty(body)) {
            exchange.getIn().removeHeaders("CamelHttp*");
            throw new HttpException(body);
        }
    }

}
