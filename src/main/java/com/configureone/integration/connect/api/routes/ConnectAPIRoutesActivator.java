/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.properties.ConnectCoreProperties;
import com.configureone.integration.connect.core.properties.ConnectRoutesGroupActivatorForCompatibility;
import com.configureone.integration.connect.core.properties.OSGIConnectRouteConfigurationManager;
import com.configureone.integration.connect.api.routes.groups.APIConnectRoutes;

/**
 * <B>Restarts this bundle whenever ConnectCoreProperties or ConnectSFProperties are updated.</B>
 * Referenced by the pom file Bundle-Activator.
 * @author rmoutray
 */
public class ConnectAPIRoutesActivator extends ConnectRoutesGroupActivatorForCompatibility
{

    /**
     * Creates the bundle activator to restart this bundle whenever {@link CoreConnectRoutes}, 
     * {@link ConnectCoreProperties}, or is updated. Additionally 
     * uses base class {@link ConnectRoutesGroupActivatorForCompatibility} to publish OSGI 
     * events when this class is started.
     */
    public ConnectAPIRoutesActivator() {
	super(new APIConnectRoutes(), new OSGIConnectRouteConfigurationManager("com.configureone.integration.connect.api.routes"), CoreConnectRoutes.class, 
            ConnectCoreProperties.class);
    }   
}
