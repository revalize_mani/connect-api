package com.configureone.integration.connect.api.users.routes;

import com.configureone.integration.connect.core.model.User;
import com.configureone.integration.connect.api.transform.SQLResultsToUserList;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRouteBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.util.CollectionUtils;


import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRouteDefinition;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRoutesGroupConfiguration;
import com.configureone.integration.connect.api.routes.groups.APIConnectRoutes;
import com.configureone.integration.connect.api.routes.groups.GetUsersRoute;

public class GetUsersRouteBuilder extends  RouteBuilder { // ConnectRouteBuilder<GetUsersRoute> {
    // ConnectRouteBuilder<GetUsersRoute> {
    // extends RouteBuilder { 
 
    /*
    public GetUsersRouteBuilder() throws Exception {
        super(APIConnectRoutes.GET_USERS_ROUTE);
    }
    */    
    
    // @Override
    public void configure() throws Exception {
        // Enter route definition here
        getUsers();
        getFilteredUsers();
    }
    

    /**
     * This direct route returns all of the Configure One users.
     */
    public void getUsers() {
        from("direct-vm:getUsers").routeId("getUsersDirectVM")
                .to("sql:select USER_ID,USER_FIRST_NAME,USER_EMAIL_ADDR,USER_LAST_NAME,COMPANY_NAME,SECURITY_LEVEL_NUM,"
                        + "USER_STATUS_CD,USER_TYPE_CD,USER_NAME from CO_USER_VIEW?dataSource=c1SQLDataSource")
                .setProperty("UserData", simple("${body}"))
                .to("sql:select USER_ID,USER_GROUP_ID from CO_USER?dataSource=c1SQLDataSource")
                .setProperty("GroupData", simple("${body}"))
                .process(this::addGroupData)
                .bean(new SQLResultsToUserList())
                .to("mock:PreReturnEndpoint");
    }

    public void getFilteredUsers(){
        from("direct-vm:getFilteredUsers").routeId("getFilteredUsersDirectVM")
            .to("sql:select USER_ID,USER_FIRST_NAME,USER_EMAIL_ADDR,USER_LAST_NAME,COMPANY_NAME,SECURITY_LEVEL_NUM,"
                + "USER_STATUS_CD,USER_TYPE_CD,USER_NAME from CO_USER_VIEW?dataSource=c1SQLDataSource")
            .setProperty("UserData", simple("${body}"))
            .to("sql:select USER_ID,USER_GROUP_ID from CO_USER?dataSource=c1SQLDataSource")
            .setProperty("GroupData", simple("${body}"))
            .process(this::addGroupData)
            .bean(new SQLResultsToUserList())
            .to("mock:PreReturnEndpoint");
    }
    
    protected void addGroupData(Exchange e){
        List<Map<String,Object>> usersList = e.getProperty("UserData", List.class);
        List<Map<String,Object>> groupList = e.getProperty("GroupData", List.class);
        usersList.forEach((_user) -> {
            for(Map<String,Object> groupData:groupList){
                if(_user.get("USER_ID").toString().compareTo(groupData.get("USER_ID").toString()) == 0){    //if user ID matches...
                    _user.put("USER_GROUP_ID", groupData.get("USER_GROUP_ID"));
                }
            }
        });
        e.getOut().setBody(usersList, List.class);
    }

}
