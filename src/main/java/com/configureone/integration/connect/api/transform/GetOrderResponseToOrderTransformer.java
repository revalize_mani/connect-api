/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Order;
import com.configureone.ws.GetOrderResponse;
import org.apache.logging.log4j.Level;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author tgunton
 */
public class GetOrderResponseToOrderTransformer {
     private static final Logger LOGGER =
            LogManager.getLogger(GetOrderResponseToOrderTransformer.class);

    public Order transform(GetOrderResponse getOrderResponse) {
        return new Order(getOrderResponse.getGetOrderReturn());
        }

}
