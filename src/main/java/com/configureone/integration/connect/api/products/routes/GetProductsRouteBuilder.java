/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.products.routes;


import com.configureone.integration.connect.api.transform.SQLResultsToProduct;
import com.configureone.integration.connect.api.transform.SQLResultsToProductsList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.component.jackson.JacksonDataFormat;

import org.apache.camel.builder.RouteBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetProductsRouteBuilder extends RouteBuilder {
    
    protected Logger logger = LogManager.getLogger(getClass());

    private static final String GET_PRODUCTS_QUERY =
            "select PRODUCT_ID,PRODUCT_DESC,TEMPLATE_INPUT_FILE,PRODUCT_SCRIPT_FILE,PRODIN_PIXEL_WIDTH,SHOW_PREV_UPCO_OPTIONS,PRODUCT_REV_LEVEL_NUM,PRODUCT_IMG_FILE,WATERMARK_IMG_FILE,WATERMARK_LOCATION,COLLAB_INTRO,COLLAB_NOTIF,ENFORCE_BUILD_ZERO,DESIGN_INTERFACE_MSG,SHOW_ALL_INPUT_OPTIONS,DESIGN_INTERFACE_PRICING,BASE_PRICE,BOM_TYPE_CD,OUTPUT_TYPE_CD,PRICING_HEADER,PRICING_FACTOR,PRICING_CD,UPLOADING_DOCS_INSTR,UPCO_OPTIONS_TXT,PREV_OPTIONS_TXT,TAB_FIELD_LABEL_HEADER,TAB_FIELD_DESC_HEADER,TAB_FIELD_VALUE_HEADER,TAB_FIELD_LABEL_JUSTIFY,TAB_FIELD_DESC_JUSTIFY,TAB_FIELD_VALUE_JUSTIFY,TAB_FIELD_DISPLAY_IND,GENERATE_ROUTINGS,DOC_INTERFACE_BOM_ID,saleable,shortDesc,longDesc,eCatBOM,keyword,QEActive,global_ind,AssignedItem,CONFIRM_DESIGN_TXT from CO_PROD";

    @Override
    public void configure() throws Exception {
        // Enter route definition here
        getProducts();
        getProductById();
        getProductPermissions();
    }

    /**
     * This direct route returns a List of Product objects containing all the products
     * retrieved from the Configure One database.
     */
    public void getProducts() {
        from("direct-vm:getProducts").routeId("getProductsDirectVM")
                .to(String.format("sql:%s?dataSource=c1SQLDataSource", GET_PRODUCTS_QUERY))
                .bean(new SQLResultsToProductsList())
                .to("mock:PreReturnEndpoint");
    }

    /**
     * This direct route returns a Product from the Configure One database
     * using the required header "id", which represents the ID of the product.
     */
    public void getProductById() {
        from("direct-vm:getProductById").routeId("getProductByIdDirectVM")
                .to(String.format("sql:%s WHERE PRODUCT_ID = :#${header.id}?dataSource=c1SQLDataSource", GET_PRODUCTS_QUERY))
                .bean(new SQLResultsToProduct())
                .to("mock:PreReturnEndpoint");
    }
    
    public void getProductPermissions(){
        from("direct-vm:getProductPermissions").routeId("getProductPermissionsDirectVM")
                .log("body = " + simple("${body}").getText())
                .unmarshal(new JacksonDataFormat(ProductsAndCategoriesRequest.class))
                .process(this::getProdAndCategories)
                .split(simple("${exchangeProperty.productIds}"), this::productStatusPermissionAggregator)//.parallelProcessing().shareUnitOfWork()
                    .setProperty("PRODUCT_ID", simple("${body}"))
                    .to("sql:SELECT * FROM CO_STATUS_GROUP WHERE (PRODUCT_ID = :#${body} AND USER_GROUP_ID = :#${exchangeProperty.userGroupId})?dataSource=c1SQLDataSource")
                .end()
                .setProperty("productStatusGroupData", simple("${body}"))
                .split(simple("${exchangeProperty.productIds}"), this::productGroupPermissionsAggregator)//.parallelProcessing().shareUnitOfWork()
                    .to("sql:SELECT * FROM CO_PROD_GROUP WHERE (PRODUCT_ID = :#${body} AND USER_GROUP_ID = :#${exchangeProperty.userGroupId})?dataSource=c1SQLDataSource")
                .end()
                .setProperty("productGroupData", simple("${body}"))
                .setProperty("configurationPermissionName", constant("DESIGNACC"))  //sql query doesn't seem to like the format of this string unless the parameter is passed in
                .to("sql:SELECT * FROM CO_FUNCTION_GROUP WHERE (USER_GROUP_ID = :#${exchangeProperty.userGroupId} AND FUNCTION_ID = :#${exchangeProperty.configurationPermissionName})?dataSource=c1SQLDataSource")
                .setProperty("configPermissionQueryResult", simple("${body}"))
                .process(this::createResponseBody);
    }
    
    protected void createResponseBody(Exchange exchange) throws JsonProcessingException, Exception{
        List<List<Map<String, Object>>> productStatusGroupData = exchange.getProperty("productStatusGroupData", List.class);
        List<List<Map<String, Object>>> productGroupData = exchange.getProperty("productGroupData", List.class);
        List<ProductStatusGroup> productStatusGroupList = new ArrayList<>();
        for (List<Map<String, Object>> currentListElement : productStatusGroupData) {
            ProductStatusGroup psg = null;
            for(Map<String, Object> currentMapElement : currentListElement){
                if(psg == null){
                    psg = new ProductStatusGroup((int)currentMapElement.get("PRODUCT_ID"), /*currentMapElement.get("USER_GROUP_ID").toString(), */null);
                }
                if(currentMapElement.containsKey("DESIGN_STAT_CD")){
                    String statCode = currentMapElement.get("DESIGN_STAT_CD").toString();
                    //log.error("statCode = " + statCode);
                    psg.DESIGN_STAT_CD.add(statCode);
                }
            }
            productStatusGroupList.add(psg);
            //log.error("psg = " + psg.toString());
        }
        for(List<Map<String, Object>> currentListElement: productGroupData){
            addProductGroupPermissions(productStatusGroupList, currentListElement);
        }
        
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("UserGroup", exchange.getProperty("userGroupId", String.class));
        boolean canAccessConfigTab = (exchange.getProperty("configPermissionQueryResult", List.class).size() > 0);
        returnMap.put("accessConfigTab", canAccessConfigTab);
        returnMap.put("products", productStatusGroupList);
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory());
        mapper.setAnnotationIntrospector(introspector);
                    
        String json = mapper.writeValueAsString(returnMap);
        exchange.getIn().setBody(json, String.class);
        //exchange.getOut().setBody(productStatusGroupList);
    }
    
    protected void addProductGroupPermissions(List<ProductStatusGroup> lst, List<Map<String, Object>> permData) throws Exception{
        for(ProductStatusGroup psg: lst){
            if(permData.size() > 0){    //if permissions were found for this product
                //log.error("permData = " + permData.toString());
                //log.error("PRODUCT_ID = " + psg.PRODUCT_ID);
                //log.error("PRODUCT_ID = " + permData.get(0).get("PRODUCT_ID"));
                if(psg.PRODUCT_ID == (int) (permData.get(0).get("PRODUCT_ID"))){ //there should be only one map in the list, so check against the first one
                    for(Map<String, Object> permDataMap: permData){
                        //log.error("ADMIN_IND = " + permDataMap.get("ADMIN_IND").toString());
                        psg.ADMIN_IND = permDataMap.get("ADMIN_IND").toString().equals("Y");
                        //log.error("ADMIN_READONLY_IND = " + permDataMap.get("ADMIN_READONLY_IND").toString());
                        psg.ADMIN_READONLY_IND = permDataMap.get("ADMIN_READONLY_IND").toString().equals("Y");
                        //log.error("BUILD_IND = " + permDataMap.get("BUILD_IND").toString());
                        psg.BUILD_IND = permDataMap.get("BUILD_IND").toString().equals("Y");
                    }
                }
            }
            else{   //if CO_PROD_GROUP 
                log.error("No permissions found for " + psg.PRODUCT_ID + "! You may need to update the Product Rights in Configure One for this product in order to create its database entry.");
                throw new Exception("Failed to get permissions for product " + psg.PRODUCT_ID + "! You may need to update the Product Rights in Configure One for this product in order to create its database entry.");
                /*psg.ADMIN_IND = false;
                psg.ADMIN_READONLY_IND = false;
                psg.BUILD_IND = false;*/
            }
        }
    }
    
    protected boolean isProductInList(List<Map<String, Object>> list, int prodId){
        for(Map<String, Object> currentElement: list){
            if(currentElement.get("PRODUCT_ID").equals(prodId)){
                return true;
            }
        }
        return false;
    }
    
    protected void getProdAndCategories(Exchange exchange){
        ProductsAndCategoriesRequest p = exchange.getIn().getBody(ProductsAndCategoriesRequest.class);
        exchange.setProperty("productIds", p.getProductIds());
        //exchange.setProperty("categoryIds", p.getCategoryIds());
        exchange.setProperty("userGroupId", p.getUserGroupId());
    }
    
    protected Exchange productStatusPermissionAggregator(Exchange oldex, Exchange newex){
        List<Map<String,Object>> sqlresult = newex.getIn().getBody(List.class);
        if(sqlresult.isEmpty()){
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("PRODUCT_ID", newex.getProperty("PRODUCT_ID", Integer.class));
            sqlresult.add(map);
        }
        List<List<Map<String,Object>>> newexbody = (oldex == null ? new ArrayList() : oldex.getIn().getBody(List.class));
        newexbody.add(sqlresult);
        newex.getOut().setBody(newexbody);
        return newex;
    }
    
    protected Exchange productGroupPermissionsAggregator(Exchange oldex, Exchange newex){
        List<Map<String,Object>> sqlresult = newex.getIn().getBody(List.class);
        List<List<Map<String,Object>>> newexbody = (oldex == null ? new ArrayList() : oldex.getIn().getBody(List.class));
        newexbody.add(sqlresult);
        newex.getOut().setBody(newexbody);
        return newex;
    }

}

class ProductStatusGroup{
    public int PRODUCT_ID;
    //public String USER_GROUP_ID;
    public List<String> DESIGN_STAT_CD;
    public boolean ADMIN_IND;
    public boolean BUILD_IND;
    public boolean ADMIN_READONLY_IND;
    
    public ProductStatusGroup(int productId, /*String userGroupId, */List<String> statusCodes){
        this.PRODUCT_ID = productId;
        //this.USER_GROUP_ID = userGroupId;
        if(statusCodes == null){
            this.DESIGN_STAT_CD = new ArrayList<>();
        }
        else{
            this.DESIGN_STAT_CD = statusCodes;
        }
    }
    
    @Override
    public String toString(){
        return "PRODUCT_ID = " + this.PRODUCT_ID + ", DESIGN_STAT_CD = " + String.join(", ", this.DESIGN_STAT_CD) + ", ADMIN_IND = " + this.ADMIN_IND + ", BUILD_IND" + this.BUILD_IND + ", ADMIN_READONLY_IND = " + this.ADMIN_READONLY_IND;
    }
    
}
    
class ProductsAndCategoriesRequest{
    public String userGroupId;
    public String productIds[];
        
    public ProductsAndCategoriesRequest(){
        this.userGroupId = "";
        this.productIds = new String[0];
    }
        
    public String getUserGroupId(){
        return this.userGroupId;
    }
        
    public void setUserGroupId(String u){
        this.userGroupId = u;
    }
        
    public String[] getProductIds(){
        return this.productIds;
    }
        
    public void setProductIds(String[] p){
        this.productIds = p;
    }
        
}
