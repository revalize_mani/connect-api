/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.Quote;
import com.configureone.ws.GetQuoteResponse;
import org.apache.logging.log4j.Level;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cloud
 */
public class GetQuoteResponseToQuoteTransformer {

    private static final Logger LOGGER =
            LogManager.getLogger(GetQuoteResponseToQuoteTransformer.class);

    public Quote transform(GetQuoteResponse getQuoteResponse) {
        return new Quote(getQuoteResponse.getGetQuoteReturn());
        }
        
    }
