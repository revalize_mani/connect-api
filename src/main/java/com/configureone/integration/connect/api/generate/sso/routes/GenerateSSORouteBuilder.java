/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.generate.sso.routes;


import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.model.SSOTokenRequest;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.integration.connect.api.transform.GenerateSSOTransformer;
import com.configureone.ws.GenerateSSO;
import com.configureone.ws.GenerateSSOResponse;

import javax.xml.namespace.QName;
import org.apache.camel.BeanInject;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.soap.name.QNameStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author ravent
 */
public class GenerateSSORouteBuilder extends RouteBuilder{
      
        /**
     * Logger for the class.
     */
    protected final Logger logger = LogManager.getLogger(getClass());
    
    SoapJaxbDataFormat soapGenerateSSO;
    SoapJaxbDataFormat soapGenerateSSOResponse;
    
    @Override
    public void configure() throws Exception {
        errorHandler(noErrorHandler());
        onException(Exception.class)
            .process(new OnExceptionProcessor());
        
        soapGenerateSSO = new SoapJaxbDataFormat(GenerateSSO.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","generateSSO")));
        soapGenerateSSOResponse = new SoapJaxbDataFormat(GenerateSSOResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","generateSSOResponse")));
        
        // Enter route definition here
        generateSSO();
    }

    private void generateSSO() {
        from("direct-vm:generateSSO").id("generateSSO").routeId("generateSSODirectVM")
                .streamCaching()
                .log(LoggingLevel.INFO, "generateSSO: start of route - ${body}")
                .setProperty("JsonRequest", simple("${body}"))
                .bean(GenerateSSOTransformer.class, "requestValidation")
                .log(LoggingLevel.INFO, "generateSSO: validation passed - ${body}")
                .choice()
                    .when(body().contains("missing"))
                            .removeHeaders("*")
                            .setProperty("fault", simple("${body}"))
                            .process(this:: invalidMessage)
                            .endChoice()
                    .otherwise()
                        .setBody(exchangeProperty("JsonRequest"))
                        .convertBodyTo(SSOTokenRequest.class)
                        .bean(GenerateSSOTransformer.class, "generateSSORequest") //transform header propertys passed into generateSSO SOAP message
                        .log(LoggingLevel.INFO, "generateSSO: marshal to soapGenerateSSO format")
                        .marshal(soapGenerateSSO)
                        .log(LoggingLevel.INFO, "generateSSO: call Configure One: - ${body}")
                        .removeHeaders("*")
                        .to("direct-vm:callConfigureOneDirectVM")
                        //.to(connectCoreProperties.getCallConfigureOneSyncRoute().getConfiguredURI())
                        .log(LoggingLevel.INFO, "this was the response from c1 - ${body}")
                        // verify that the response is not fault
                        .setHeader("count", simple(""))
                        .setHeader("count", xpath("count(//faultstring)", String.class))
                        .choice()
                            .when(header("count").isNotEqualTo("1"))
                                .log(LoggingLevel.INFO, "generateSSO: unmarshal to soapGenerateSSOResponse format")
                                .unmarshal(soapGenerateSSOResponse)
                                .log(LoggingLevel.INFO, "generateSSO: run transform")
                                .bean(GenerateSSOTransformer.class, "transform")
                                .removeHeaders("*")
                                .endChoice()
                            .otherwise()
                                .setProperty("fault",xpath("//faultstring", String.class))
                                .removeHeaders("*")
                                .process(this:: invalidUser)
                                .endChoice()
                    .end();  
    }
  
    protected void invalidUser(Exchange exchange) {
        String message = (String) exchange.getProperty("fault");
        String faultMessage ="";
        int code =0;
        if (message.contains("XML document structures must start and end within the same entity.") || message.contains("Premature end of file")) {
            code = 500;
            faultMessage = "Invalid XML document structure: - "+message; 
        }
        else if(message.contains("UserId does not exist.")){
            code = 401;
            faultMessage = "Invalid user: - "+message;
        }
          
        RestErrorResponse rer = new RestErrorResponse(code, faultMessage);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }
    
    protected void invalidMessage(Exchange exchange) {
        String message = (String) exchange.getProperty("fault");       
        RestErrorResponse rer = new RestErrorResponse(400, message);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }
    
    
}
