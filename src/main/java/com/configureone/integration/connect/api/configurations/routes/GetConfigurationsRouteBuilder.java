/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.configurations.routes;

import com.configureone.integration.connect.api.transform.GetConfigurationResponseToConfigurationTransformer;
import com.configureone.integration.connect.core.camel.processors.FailedOriginalMessageProcessor;
import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.processors.SetInToOutProcessor;
import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.model.Configuration;
import com.configureone.ws.GetConfiguration;
import com.configureone.ws.GetConfigurationResponse;
import javax.xml.namespace.QName;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.camel.LoggingLevel;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.soap.SoapJaxbDataFormat;
import org.apache.camel.dataformat.soap.name.QNameStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world! route provided with the archetype.  This is used as a
 * placeholder for the routes package.  It is also used in some example unit
 * tests.
 */
public class GetConfigurationsRouteBuilder extends RouteBuilder {
    
    /**
     * The logger for the class.
     */
    protected Logger logger = LogManager.getLogger(getClass());
    
    
    public static final String DESIGN_ID_EXCHANGE_HEADER = "di";
    public static final String SMARTPART_NUM_EXCHANGE_HEADER = "spn";
    private final ConnectExchangeProperty<Configuration> parentConfiguration = new ConnectExchangeProperty("parentConfiguration", Configuration.class);
    private final ConnectExchangeProperty<List<Map<String, Object>>> queryResult = new ConnectExchangeProperty("sqlConfigurationQueryResult", List.class);
    /**
     * Overridden RouteBuilder configure() method.  Used to define a simple
     * route that sets it's body to "Hello World!"
     * 
     * @throws  Exception   Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel(CoreConnectRoutes.DEAD_LETTER_QUEUE_ROUTE.getConfiguredURI())
                .useOriginalMessage()
                .onExceptionOccurred(new OnExceptionProcessor())
                .onPrepareFailure(new FailedOriginalMessageProcessor())
        );

        // Enter route definition here
        getConfigurations();
        enrichConfigurationWithProductId();
    }

    public void getConfigurations() throws Exception{
        SoapJaxbDataFormat soapGetConfiguration = new SoapJaxbDataFormat(GetConfiguration.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getConfiguration")));
        SoapJaxbDataFormat soapGetConfigurationResponse = new SoapJaxbDataFormat(GetConfigurationResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getConfigurationResponse")));
        
        from("direct-vm:getConfiguration")
            .streamCaching()
            .process(new Processor(){
                @Override
                public void process(Exchange exchange){
                    GetConfiguration getConfiguration = new GetConfiguration();
                    String designId = exchange.getIn().getHeader("designId", String.class);
                    getConfiguration.setDesignId(designId);
                    //getItem.setSmartPartNumber(smartPartNum);
                    exchange.getIn().setBody(getConfiguration);
                }
            })
            .marshal(soapGetConfiguration)
            .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
            .unmarshal(soapGetConfigurationResponse)
            .bean(GetConfigurationResponseToConfigurationTransformer.class, "transform(${body})")
            .setProperty("configurationClass",simple("${body}"))
            //.log(LoggingLevel.ERROR, "body = " + simple("${body}"))
            .setBody(simple("${exchangeProperty.configurationClass}",Configuration.class))
            .process(new Processor(){
                @Override
                public void process(Exchange exchange) throws Exception{
                    Configuration configObj = exchange.getIn().getBody(Configuration.class);

                    // Store the quote object 
                    parentConfiguration.setPropertyData(exchange, configObj);
                }
            })
            //add productId to main configuration
            .to("direct-vm:enrichConfigurationWithProductId")
            
            // Split out QuoteDetail objects to enrich with the productid
            /*.split(simple("${body.getChildren}"), this::ConfigurationAggregator).parallelProcessing().shareUnitOfWork()
                .to("direct-vm:enrichConfigurationWithProductId")
            .end()
            .process(this::setConfigurationChildOnConfiguration)*/
            .removeHeaders("*") // TODO: Figure out what we need to remove
            .process(new SetInToOutProcessor());
    }
    
    protected Exchange ConfigurationAggregator (Exchange oldex, Exchange newex) {
        List<Configuration> newexbody = (oldex == null ? new ArrayList() : oldex.getIn().getBody(List.class));
        newexbody.add(newex.getIn().getBody(Configuration.class));
        newex.getIn().setBody(newexbody);
        return newex;
    }
    
    protected Exchange setProductIdAggregator(Exchange original, Exchange resource) {
        List<Map<String,Object>> sqlresult = resource.getIn().getBody(List.class);
        // if (sqlresult.size() == 0 || sqlresult.size() > 1) {
        //     throw new Exception("Could not retrieve additional quote detail information from C1 database. [" + sqlresult.size() + "] rows received from database, but expected 1.");
        // }
        Configuration cc = original.getIn().getBody(Configuration.class);
        if (sqlresult.size() >= 1) {
            cc.setProductId(((Integer)sqlresult.get(0).get("PRODUCT_ID")).longValue());
        }
        original.getIn().setBody(cc);
        return original;
    }
    
    /**
     * 
     * Route to enrich the quote details with a 
     * @throws java.lang.Exception
     */
    protected void enrichConfigurationWithProductId() throws Exception {
        from("direct-vm:enrichConfigurationWithProductId").routeId("enrichConfigurationWithProductId")
            .streamCaching()
            .process(this::setDesignIdAndSmartPartNumProcessor)
            .enrich("sql:SELECT PRODUCT_ID FROM CO_DES WHERE DESIGN_ID = :#${header." + DESIGN_ID_EXCHANGE_HEADER + "}?dataSource=c1SQLDataSource", this::setProductIdAggregator)
            .choice()
                .when(simple("${body.getChildren.size} > 0"))
                    // Split out QuoteDetail objects to enrich with the productid
                    .split(simple("${body.getChildren}"), this::ConfigurationAggregator).parallelProcessing().shareUnitOfWork()
                        .to("direct-vm:enrichConfigurationWithProductId")
                    .end()
                .process(this::setConfigurationChildOnConfiguration)
            .end();
    }
   
    protected Exchange setConfigurationChildOnConfiguration(Exchange exchange) {
        Configuration c = parentConfiguration.getPropertyData(exchange);
        try{
            List<Configuration> childList = exchange.getIn().getBody(List.class);
            c.setChildren(childList);
        }
        catch(Exception e){
            logger.warn("Failed to set configuration children on parent configuration");
        }
        exchange.getIn().setBody(c);
        return exchange; 
    }
    
    protected Exchange setDesignIdAndSmartPartNumProcessor(Exchange exchange) {
        Configuration con = exchange.getIn().getBody(Configuration.class);
        long designid = Long.parseLong(con.getId());
        //String smartpartnum = con.getSmartpartnum();
        exchange.getIn().setHeader(DESIGN_ID_EXCHANGE_HEADER, designid);
        //exchange.getIn().setHeader(SMARTPART_NUM_EXCHANGE_HEADER, smartpartnum);
        return exchange;
    }

}