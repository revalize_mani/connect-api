/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.quotes.routes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.processors.SetInToOutProcessor;
import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Quote;
import com.configureone.integration.connect.core.model.QuoteDetail;
import com.configureone.integration.connect.core.model.QuoteStatusRequest;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.model.StatusResponse;
import com.configureone.integration.connect.api.transform.GetQuoteResponseToQuoteTransformer;
import com.configureone.ws.GetQuote;
import com.configureone.ws.GetQuoteResponse;
import com.configureone.ws.Input;
import com.configureone.ws.ObjectFactory;
import com.configureone.ws.ProcessQuote;
import com.configureone.ws.ProcessQuoteResponse;
import com.configureone.ws.Response;
import com.configureone.ws.SimpleInput;
import com.configureone.ws.UpdateQuote;
import com.configureone.ws.UpdateQuoteResponse;
import com.configureone.ws.model.quote.QuoteRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static java.lang.reflect.Modifier.TRANSIENT;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;
import org.apache.camel.dataformat.soap.name.QNameStrategy;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.http.HttpException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;

/**
 * Route builder for accessing configure one quotes.
 * 
 */
public class QuotesRouteBuilder extends RouteBuilder {

    /**
     * Logger for the class.
     */
        protected Logger logger = LogManager.getLogger(getClass());


       
    SoapJaxbDataFormat processQuoteSoapDataFormat;
    SoapJaxbDataFormat processQuoteResponseSoapDataFormat;
    SoapJaxbDataFormat soapGetQuote;
    SoapJaxbDataFormat soapGetQuoteResponse;
    SoapJaxbDataFormat soapUpdateQuote;
    SoapJaxbDataFormat soapUpdateQuoteResponse;



    public static final String SERIAL_NUM_EXCHANGE_HEADER = "sn";
    public static final String DESIGN_ID_EXCHANGE_HEADER = "di";
    public static final String SMARTPART_NUM_EXCHANGE_HEADER = "spn";
    private final ConnectExchangeProperty<List<Map<String, Object>>> queryResult = new ConnectExchangeProperty("sqlQuoteQueryResult", List.class);
    private final ConnectExchangeProperty<Quote> parentQuote = new ConnectExchangeProperty("parentQuote", Quote.class);

    /**
     * Configuration of the connect-quotes route.
     * @throws  Exception   Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(noErrorHandler());
        onException(Exception.class)
            .process(new OnExceptionProcessor());
                
        processQuoteSoapDataFormat = new SoapJaxbDataFormat(ProcessQuote.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "processQuote")));
        processQuoteResponseSoapDataFormat = new SoapJaxbDataFormat(ProcessQuoteResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "processQuoteResponse")));
        soapGetQuote = new SoapJaxbDataFormat(GetQuote.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getQuote")));
        soapGetQuoteResponse = new SoapJaxbDataFormat(GetQuoteResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getQuoteResponse")));
        soapUpdateQuote = new SoapJaxbDataFormat(UpdateQuote.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "updateQuote")));
        soapUpdateQuoteResponse = new SoapJaxbDataFormat(UpdateQuoteResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "updateQuoteResponse")));

        processQuoteRoute();
        getQuoteBySerialNumber();
        enrichQuoteDetailWithProductId();
        updateQuoteStatus();
    }    
    
    /**
     * Creates a quote (using the Configure One process quote endpoint). Requires a 
     * {@link com.configureone.integration.connect.core.model.Quote} object in the message body.
     * Returns the {@link com.configureone.integration.connect.core.model.Quote} object returned by 
     * the {@link #getQuoteBySerialNumber()} route.
     * @throws java.lang.Exception
     */
    protected void processQuoteRoute() throws Exception {
        from("direct-vm:processQuote").routeId("processQuoteDirectVM")
            .log("Received process quote request.")
            .process(this::createProcessQuoteSOAPRequestProcessor)
            // Format into a SOAP message using the standard Soap DataFormat
            .marshal(processQuoteSoapDataFormat)
            .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
            .process(this::checkForHttpProcessResponseErrors)
            .removeHeaders("CamelHttp*")
            .convertBodyTo(String.class)
            .unmarshal(processQuoteResponseSoapDataFormat)
            .removeHeader(Exchange.CONTENT_TYPE)
            .process(this::checkProcessQuoteResponseForErrorsProcessor)
            .setHeader(SERIAL_NUM_EXCHANGE_HEADER, body())
            .setBody(constant(null))
            //Now that we've created the quote, retrieve it via the standard getQuote route.
            .to("direct-vm:getQuoteBySerialNumber")
            .removeHeader(SERIAL_NUM_EXCHANGE_HEADER);
    }
    
    
        protected void updateQuoteStatus() throws Exception {
        from("direct-vm:updateQuoteStatus").routeId("updateQuoteStatusDirectVM")
            .removeHeaders("*")
            .log("Received updateQuoteStatus request.")
            .process(this::createUpdateQuoteStatusSOAPRequestProcessor)
                
            // Format into a SOAP message using the standard Soap DataFormat
            .marshal(soapUpdateQuote)
            .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
            .process(this::checkForHttpProcessResponseErrors)
            .removeHeaders("*")
            .unmarshal(soapUpdateQuoteResponse)
            .process(this::setStatusResponse);
    }
    
    
    
    
    


    /**
     * Route to retrieve the quote from ConfigureOne by its Serial Number.
     * @throws java.lang.Exception
     */
    
        GsonDataFormat gsonDataFormat = new GsonDataFormat(Quote.class);

    protected void getQuoteBySerialNumber() throws Exception {
        from("direct-vm:getQuoteBySerialNumber").routeId("getQuoteBySerialNumberDirectVM")
                .streamCaching()
                .process(this::getQuoteSOAPProcessor)
                // Format into a SOAP message using the standard Soap DataFormat
                // provided with camel
                .marshal(soapGetQuote)
                .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
                .convertBodyTo(String.class)
                .process(this::checkForHttpResponseErrors)
                .removeHeaders("CamelHttp*")
                .choice()
                    .when(simple("${body} == '' || ${body} == '[]'"))
                        .log(LoggingLevel.INFO, "getQuote response was empty")
                        .setProperty("errorCode", simple("404"))
                        .setProperty("errorMessage", simple("Quote not found"))
                        .process(this:: nullResponse)
                    .endChoice()
                    .otherwise()
                        .log(LoggingLevel.INFO, "getQuoteBySerialNumber: quote message contains data")
                        .process(this::pullGetQuoteResponse)
                        .unmarshal(soapGetQuoteResponse)
                        //Turn the GetQuoteResponse into our standard quote object.
                        .log(LoggingLevel.INFO, "getQuoteBySerialNumber: calling bean - GetQuoteResponseToQuoteTransformer")
                        .bean(GetQuoteResponseToQuoteTransformer.class, "transform(${body})")
                        .setProperty("quoteClass",simple("${body}"))
                        .log(LoggingLevel.INFO, "getQuoteBySerialNumber: finished processor")
                        //.marshal(gsonDataFormat)
                        //.process(this::jsonbody)
                        //.to("file:C:/cca-4.0.11-temp/connect-advanced-4.0.11-SNAPSHOT/bin?fileName=quoted.txt")
                        .setBody(simple("${exchangeProperty.quoteClass}",Quote.class))
                        //.log(LoggingLevel.INFO, "getQuoteBySerialNumber: call enritch")

                        .enrich("sql:SELECT QUOTE_VALID FROM CO_DOC WHERE SERIAL_NUMBER = :#${header." + SERIAL_NUM_EXCHANGE_HEADER + "}?dataSource=c1SQLDataSource", this::setSqlResultToPropertyAggregator)
                        //.log(LoggingLevel.INFO, "getQuoteBySerialNumber: enritch finish")

                        .process(this::appendSqlToObjProcessor)
                        // Split out QuoteDetail objects to enrich with the productid
                        .split(simple("${body.getDetails}"), this::quoteDetailAggregator).parallelProcessing().shareUnitOfWork()
                            .to("direct-vm:enrichQuoteDetailWithProductId")
                        .end()
                        .process(this::setQuoteDetailOnQuote)
                        .removeHeaders("*") // TODO: Figure out what we need to remove
                        .process(new SetInToOutProcessor())
                    .endChoice()
                .end()
                ;  
    }
    
    
            protected void jsonbody(Exchange exchange) throws Exception {
            logger.log(Level.INFO, "jsonbody -  processor called");
            
                        Quote quote = exchange.getIn().getBody(Quote.class);        
                        Gson gson = new GsonBuilder()
                        .create();
                        String json = gson.toJson(quote, Quote.class);
                       logger.log(Level.INFO, json);

       exchange.getIn().setBody(json, String.class);

        }
            
    


    protected Exchange pullGetQuoteResponse (Exchange exchange) {
        String newbody = "";
        try {
            String msgbody = exchange.getIn().getBody(String.class);
            newbody = msgbody.replaceAll("http://model.ws.configureone.com","http://ws.configureone.com");
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        exchange.getIn().setBody(newbody);

        return exchange;
    }
    
    /**
     * 
     * Route to enrich the quote details with a 
     * @throws java.lang.Exception
     */
    protected void enrichQuoteDetailWithProductId() throws Exception {
        from("direct-vm:enrichQuoteDetailWithProductId").routeId("enrichQuoteDetailWithProductId")
            .choice()
                .when(simple("${body.getType} == 'C'"))
                    .process(this::setDesignIdAndSmartPartNumProcessor)
                    .enrich("sql:SELECT PRODUCT_ID FROM CO_DES WHERE DESIGN_ID = :#${header." + DESIGN_ID_EXCHANGE_HEADER + "}?dataSource=c1SQLDataSource", this::setProductIdAggregator);
    }
   
    protected Exchange setQuoteDetailOnQuote(Exchange exchange) {
        Quote q = parentQuote.getPropertyData(exchange);
        List<QuoteDetail> qdlist = exchange.getIn().getBody(List.class);
        q.setDetails(qdlist);
        exchange.getIn().setBody(q);
        return exchange; 
    }
    
    protected Exchange setDesignIdAndSmartPartNumProcessor(Exchange exchange) {
        QuoteDetail qd = exchange.getIn().getBody(QuoteDetail.class);
        long designid = Long.parseLong(qd.getId());
        String smartpartnum = qd.getSmartpartnum();
        exchange.getIn().setHeader(DESIGN_ID_EXCHANGE_HEADER, designid);
        exchange.getIn().setHeader(SMARTPART_NUM_EXCHANGE_HEADER, smartpartnum);
        return exchange;
    }

    protected Exchange setProductIdAggregator(Exchange original, Exchange resource) {
        List<Map<String,Object>> sqlresult = resource.getIn().getBody(List.class);
        // if (sqlresult.size() == 0 || sqlresult.size() > 1) {
        //     throw new Exception("Could not retrieve additional quote detail information from C1 database. [" + sqlresult.size() + "] rows received from database, but expected 1.");
        // }
        QuoteDetail qd = original.getIn().getBody(QuoteDetail.class);
        if (sqlresult.size() >= 1) {
            qd.setProductid(((Integer)sqlresult.get(0).get("PRODUCT_ID")).longValue());
        }
        original.getIn().setBody(qd);
        return original;
    }

    protected Exchange quoteDetailAggregator (Exchange oldex, Exchange newex) {
        List<QuoteDetail> newexbody = (oldex == null ? new ArrayList() : oldex.getIn().getBody(List.class));
        newexbody.add(newex.getIn().getBody(QuoteDetail.class));
        newex.getIn().setBody(newexbody);
        return newex;
    }
    
    protected Exchange setSqlResultToPropertyAggregator(Exchange original, Exchange resource) {
        queryResult.setPropertyData(original, resource.getIn().getBody(List.class));
        return original;
    }
    
    protected Exchange appendSqlToObjProcessor(Exchange exchange) throws Exception {
        List<Map<String, Object>> sqlResult = queryResult.getPropertyData(exchange);
        if (sqlResult == null) {
            throw new Exception("Could not retrieve additional quote information from C1 database. SQL result was not present.");
        }
        if (sqlResult.size() != 1) {
            throw new Exception("Could not retrieve additional quote information from C1 database. [" + sqlResult.size() + "] rows received from database, but expected 1.");
        }
        boolean quoteValid = (boolean) sqlResult.get(0).get("QUOTE_VALID");
        
        Quote quoteObj = exchange.getIn().getBody(Quote.class);
        quoteObj.setValid(quoteValid);

        // Store the quote object 
        parentQuote.setPropertyData(exchange, quoteObj);

        return exchange;
    }

    /**
     * Processor to see if the ProcessQuoteResponse contains an error or a success.
     * @param exchange The exchange being processed.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException If an error is found.
     */
    protected void checkProcessQuoteResponseForErrorsProcessor(Exchange exchange) throws ConnectBusinessException {
        ProcessQuoteResponse pqr = exchange.getIn().getBody(ProcessQuoteResponse.class);
        Response processQuoteReturn = pqr.getProcessQuoteReturn();
        boolean success = processQuoteReturn.isSuccess();
        String message = processQuoteReturn.getMessage().getValue();
        if (success) {
            logger.info("Success.");
            exchange.getIn().setBody(message);
        } else {
            ConnectBusinessException cbe = new ConnectBusinessException("Error from Configure One while attempting to process request. See log for details.", message);
            logger.error(cbe.toString());
            throw cbe;
        }
    }
    
    /**
     * Checks an exchange containing an HTTP response for errors (response code not in the 200s or body is null/empty).
     * If there are any present, it is logged and the exchange is set to a 500 error with a json error response body.
     * @param exchange The exchange to check.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException
     * @throws org.apache.http.HttpException
     */
    protected void checkForHttpResponseErrors(Exchange exchange) throws ConnectBusinessException, HttpException {
        int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
        String body = exchange.getIn().getBody(String.class);

        if (responseCode >= 300) {
            exchange.getIn().removeHeaders("CamelHttp*");
            exchange.getIn().setBody(null);
            throw new HttpException(body);
        }
    }
    
    /**
     * Checks an exchange containing an HTTP response for errors (response code not in the 200s or body is null/empty).
     * If there are any present, it is logged and the exchange is set to a 500 error with a json error response body.
     * @param exchange The exchange to check.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException
     * @throws org.apache.http.HttpException
     */
    protected void checkForHttpProcessResponseErrors(Exchange exchange) throws ConnectBusinessException, HttpException {
        int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
        String body = exchange.getIn().getBody(String.class);

        if (responseCode >= 300 || StringUtils.isEmpty(body)) {
            exchange.getIn().removeHeaders("CamelHttp*");
            exchange.getIn().setBody(null);
            throw new HttpException(body);
        }
    }
    
    /**
     * Processor to create the SOAP request for Configure One based on the incoming Json.
     * @param exchange The exchange to process.
     * @throws java.lang.Exception If the process quote request cannot be created with the data provided.
     */
    protected void createProcessQuoteSOAPRequestProcessor(Exchange exchange) throws Exception {
        Quote quote = exchange.getIn().getBody(Quote.class);

        QuoteRequest quoteRequest = new QuoteRequest();
        
        ObjectFactory objectFactory = new ObjectFactory();
        ProcessQuote processQuoteRequest = objectFactory.createProcessQuote();
        
        quoteRequest.setSTATUS(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "STATUS"), String.class, quote.getStatus()));   
        
        quoteRequest.setSERIALNUMBER(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "SERIAL_NUMBER"), String.class, quote.getSerialNumber()));
        
        if (getBillToAddressRefNumType(quote) != null) {
            quoteRequest.setBILLTOADDRESSREFERENCENUMBER(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "BILL_TO_ADDRESS_REFERENCE_NUMBER"), String.class, getBillToAddressRefNum(quote)));
            quoteRequest.setBILLTOADDRESSREFERENCENUMBERTYPE(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "BILL_TO_ADDRESS_REFERENCE_NUMBER_TYPE"), String.class, getBillToAddressRefNumType(quote).name()));
        }
        
        if (getCustomerRefNumType(quote) != null) {
            quoteRequest.setCUSTOMERREFERENCENUMBER(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "CUSTOMER_REFERENCE_NUMBER"), String.class, getCustomerRefNum(quote)));
            quoteRequest.setCUSTOMERREFERENCENUMBERTYPE(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "CUSTOMER_REFERENCE_NUMBER_TYPE"), String.class, getCustomerRefNumType(quote).name()));
        }
        quoteRequest.setPRICEBOOKNAME(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "PRICE_BOOK_NAME"), String.class, quote.getPricebookName()));
        
        if (getShipToAddressRefNumType(quote) != null) {
            quoteRequest.setSHIPTOADDRESSREFERENCENUMBER(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "SHIP_TO_ADDRESS_REFERENCE_NUMBER"), String.class, getShipToAddressRefNum(quote)));
            quoteRequest.setSHIPTOADDRESSREFERENCENUMBERTYPE(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "SHIP_TO_ADDRESS_REFERENCE_NUMBER_TYPE"), String.class, getShipToAddressRefNumType(quote).name()));
        }
        
        
        quoteRequest.setTEMPLATENAME(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "TEMPLATE_NAME"), String.class, quote.getTemplateName()));
        quoteRequest.setQUOTENAME(new JAXBElement<>(new QName("http://quote.model.ws.configureone.com", "QUOTE_NAME"), String.class, quote.getName()));
        
        if (!CollectionUtils.isEmpty(quote.getInputs())) {
            Collection<SimpleInput> simpleInputs = new ArrayList<>(quote.getInputs().size());
            for (Input input : quote.getInputs()) {
                if (input != null) {
                    SimpleInput si = new SimpleInput();
                    if (input.getValue() != null && input.getValue().size() != 1) {
                        throw new Exception("Ambiguous match found for input value in request. "
                                + "Only one value must be present, but was " + input.getValue().size()
                                + ". [" + new Gson().toJson(input.getValue()) + "]");
                    }

                    si.setNAME(input.getName());
                    String value = input.getValue().get(0).getName();
                    si.setVALUE(new JAXBElement<>(new QName("http://ws.configureone.com", "VALUE"), String.class, value));
                    simpleInputs.add(si);
                }
            }
            quoteRequest.getINPUTS().addAll(simpleInputs);
        }
        
        quoteRequest.setMAINTAINBUILD(quote.getMaintainBuild());
        quoteRequest.setUSERID(quote.getUserId());

        processQuoteRequest.setQUOTEREQUEST(quoteRequest);
        
        exchange.getIn().setBody(processQuoteRequest);
    }    

    protected void createUpdateQuoteStatusSOAPRequestProcessor(Exchange exchange) throws Exception {
        QuoteStatusRequest statusReq = exchange.getIn().getBody(QuoteStatusRequest.class);
        UpdateQuote updateQuote = new UpdateQuote();
        
        updateQuote.setSERIALNUMBER(statusReq.getSerialNumber());
        updateQuote.setSTATUS(statusReq.getStatus());

        exchange.getIn().setBody(updateQuote);
    }   
    
        protected void setStatusResponse(Exchange exchange) throws Exception {
        UpdateQuoteResponse quoteResponse = exchange.getIn().getBody(UpdateQuoteResponse.class);
        
        StatusResponse statusRes = new StatusResponse();
        statusRes.setMessage(quoteResponse.getUpdateQuoteReturn().getMessage().getValue());
        statusRes.setSuccess(String.valueOf(quoteResponse.getUpdateQuoteReturn().isSuccess()));
        
        exchange.getIn().setBody(statusRes);
    } 
    
    
    /**
     * Processor to Create a GetQuote object from the message header.
     * @param exchange
     * @throws Exception
     */
    protected void getQuoteSOAPProcessor(Exchange exchange) throws Exception {
        String serialNumber =
                exchange.getIn().getHeader(SERIAL_NUM_EXCHANGE_HEADER,
                        String.class);

        GetQuote getQuote = new GetQuote();
        getQuote.setSerialNumber(serialNumber);
        Message inMsg = exchange.getIn();
        inMsg.setBody(getQuote);
    }
    
    protected void nullResponse(Exchange exchange) {
        String errorCodeString = (String) exchange.getProperty("errorCode");
        int errorCode = Integer.parseInt(errorCodeString);
        String errorMessage = (String) exchange.getProperty("errorMessage");

        RestErrorResponse rer = new RestErrorResponse(errorCode, errorMessage);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }
    
    /**
     * For use in process quote request; determines the supplied shipTo contact information.
     * @param quote The quote to check for the shipTo reference.
     * @return The found reference number, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and contact ref number cannot be determined.
     */
    public String getShipToAddressRefNum(Quote quote) throws Exception {
        if (getShipToAddressRefNumType(quote) == null) {
            return null;
        }
        switch (getShipToAddressRefNumType(quote)) {
            case CRM:
                return quote.getShipToCRMContactRefNum();
            case ERP:
                return quote.getShipToERPContactRefNum();
            case REFERENCE:
                return quote.getShipToRefNum();
            default:
                return null;
        }
    }

    /**
     * For use in process quote request; determines the {@link com.configureone.integration.connect.core.model.Quote.ReferenceType} 
     * of the supplied shipTo contact information.
     * @param quote The quote to check for the shipTo reference.
     * @return The found reference type, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and cannot be determined.
     */
    public Quote.ReferenceType getShipToAddressRefNumType(Quote quote) throws Exception{
        //Create a map of the potential references
        EnumMap<Quote.ReferenceType, String> shipToRefNumsMap = new EnumMap<>(Quote.ReferenceType.class);
        shipToRefNumsMap.put(Quote.ReferenceType.REFERENCE, quote.getShipToRefNum());
        shipToRefNumsMap.put(Quote.ReferenceType.CRM, quote.getShipToCRMContactRefNum());
        shipToRefNumsMap.put(Quote.ReferenceType.ERP, quote.getShipToERPContactRefNum());
        
        return getSingleNonEmptyEnumTypeFromMap(shipToRefNumsMap);
    }
    
    /**
     * For use in process quote request; determines the supplied billTo contact information.
     * @param quote The quote to check for the billTo reference.
     * @return The found reference number, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and contact ref number cannot be determined.
     */
    public String getBillToAddressRefNum(Quote quote) throws Exception {
        if (getBillToAddressRefNumType(quote) == null) {
            return null;
        }
        switch (getBillToAddressRefNumType(quote)) {
            case CRM:
                return quote.getBillToCRMContactRefNum();
            case ERP:
                return quote.getBillToERPContactRefNum();
            case REFERENCE:
                return quote.getBillToRefNum();
            default:
                return null;
        }
    }

    /**
     * For use in process quote request; determines the {@link com.configureone.integration.connect.core.model.Quote.ReferenceType} 
     * of the supplied billTo contact information.
     * @param quote The quote to check for the billTo reference.
     * @return The found reference type, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and cannot be determined.
     */
    public Quote.ReferenceType getBillToAddressRefNumType(Quote quote) throws Exception {
        //Create a map of the potential references
        EnumMap<Quote.ReferenceType, String> billToRefNumsMap = new EnumMap<>(Quote.ReferenceType.class);
        billToRefNumsMap.put(Quote.ReferenceType.REFERENCE, quote.getBillToRefNum());
        billToRefNumsMap.put(Quote.ReferenceType.CRM, quote.getBillToCRMContactRefNum());
        billToRefNumsMap.put(Quote.ReferenceType.ERP, quote.getBillToERPContactRefNum());
        
        return getSingleNonEmptyEnumTypeFromMap(billToRefNumsMap);
    }
    
    /**
     * For use in process quote request; determines the supplied customer contact information.
     * @param quote The quote to check for the customer reference.
     * @return The found reference number, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and contact ref number cannot be determined.
     */
    public String getCustomerRefNum(Quote quote) throws Exception {
        if (getCustomerRefNumType(quote) == null) {
            return null;
        }
        switch (getCustomerRefNumType(quote)) {
            case CRM:
                return quote.getCrmReferenceNum();
            case ERP:
                return quote.getErpReferenceNum();
            case REFERENCE:
                return quote.getCustRefNum();
            default:
                return null;
        }
    }

    /**
     * For use in process quote request; determines the {@link com.configureone.integration.connect.core.model.Quote.ReferenceType} 
     * of the supplied customer contact information.
     * @param quote The quote to check for the customer reference.
     * @return The found reference type, or null if no contacts are referenced.
     * @throws java.lang.Exception When more than one reference type is found and cannot be determined.
     */
    public Quote.ReferenceType getCustomerRefNumType(Quote quote) throws Exception {
        //Create a map of the potential references
        EnumMap<Quote.ReferenceType, String> customerRefNumsMap = new EnumMap<>(Quote.ReferenceType.class);
        customerRefNumsMap.put(Quote.ReferenceType.REFERENCE, quote.getCustRefNum());
        customerRefNumsMap.put(Quote.ReferenceType.CRM, quote.getCrmReferenceNum());
        customerRefNumsMap.put(Quote.ReferenceType.ERP, quote.getErpReferenceNum());
        
        return getSingleNonEmptyEnumTypeFromMap(customerRefNumsMap);
    }
    
    /**
     *
     * @param <K>
     * @param enumMap
     * @return 
     * @throws java.lang.Exception @return
     */
    public <K extends Enum<K>> K getSingleNonEmptyEnumTypeFromMap(EnumMap<K, String> enumMap) throws Exception {
        //Remove any null or empty references from the map
        for (Map.Entry<K, String> entry : enumMap.entrySet()) {
            if (StringUtils.isEmpty(entry.getValue()))
            enumMap.remove(entry.getKey());
        }
        
        //Validate that the map contains only one item
        switch (enumMap.size()) {
            case 0:
                return null;
            case 1:
                for (K refType : enumMap.keySet()) {
                    return refType;
                }
            default:
                throw new Exception("Ambiguous match found for reference type. "
                        + "Only one reference type must be present, but was " + enumMap.size()
                        + ". [" + enumMap.toString() + "]");
        }
    }
}
