/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.items.routes;

import com.configureone.integration.connect.core.camel.processors.FailedOriginalMessageProcessor;
import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.ws.GetItem;
import com.configureone.ws.GetItemResponse;
import com.configureone.ws.ItemMaster;
import com.configureone.ws.ItemPrice;
import com.configureone.ws.ProcessItem;
import com.configureone.ws.ProcessItemPrice;
import com.configureone.ws.ProcessItemPriceResponse;
import com.configureone.ws.ProcessItemResponse;
import com.configureone.ws.Response;
import com.configureone.integration.connect.core.model.ItemList;
import com.configureone.ws.ItemMaster;
import com.configureone.integration.connect.core.model.ItemBatchProcessingStrategy;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.model.ItemResponse;
import com.configureone.integration.connect.core.model.BatchResponse;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import javax.xml.namespace.QName;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.dataformat.soap.SoapJaxbDataFormat;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.camel.LoggingLevel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import org.apache.camel.component.gson.GsonDataFormat;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.soap.name.QNameStrategy;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world! route provided with the archetype.  This is used as a
 * placeholder for the routes package.  It is also used in some example unit
 * tests.
 */
public class AppRouteBuilder extends RouteBuilder {
    
    String sqlGetPriceBookId ="select * from CO_PRICE_BOOK where NAME = '${exchangeProperty.bookName}'";
        protected Logger logger = LogManager.getLogger(getClass());

    /**
     * Overridden RouteBuilder configure() method.  Used to define a simple
     * route that sets it's body to "Hello World!"
     * 
     * @throws  Exception   Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel(CoreConnectRoutes.DEAD_LETTER_QUEUE_ROUTE.getConfiguredURI())
                .useOriginalMessage()
                .onExceptionOccurred(new OnExceptionProcessor())
                .onPrepareFailure(new FailedOriginalMessageProcessor())
        );

        // Enter route definition here
        processItems();
        getItems();
        processItemPrice();
    }

    public void getItems() throws Exception{
        SoapJaxbDataFormat soapGetItem = new SoapJaxbDataFormat(GetItem.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getItem")));
        SoapJaxbDataFormat soapGetItemResponse = new SoapJaxbDataFormat(GetItemResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com", "getItemResponse")));
        
        from("direct-vm:getItem")
            .streamCaching()
            .process(new Processor(){
                @Override
                public void process(Exchange exchange){
                    GetItem getItem = new GetItem();
                    String smartPartNum = exchange.getIn().getHeader("itemRefNum", String.class);
                    getItem.setSmartPartNumber(smartPartNum);
                    exchange.getIn().setBody(getItem);
                }
            })
            .marshal(soapGetItem)
            .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
            .unmarshal(soapGetItemResponse)
            .process(new Processor(){
                @Override
                public void process(Exchange exchange) throws Exception{
                    GetItemResponse itemResponse = exchange.getIn().getBody(GetItemResponse.class);
                    /*Gson gson = new Gson();
                    String json = gson.toJson(itemResponse, GetItemResponse.class);
                    exchange.getIn().setBody(json);*/
                    ItemMaster item = itemResponse.getGetItemReturn();
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.enable(SerializationFeature.INDENT_OUTPUT);
                    AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory());
                    mapper.setAnnotationIntrospector(introspector);
                    
                    String json = mapper.writeValueAsString(item);
                    exchange.getIn().setBody(json, String.class);
                }
            });
    }
    
    /**
     * Method that receives item create/update requests in JSON
     * form and converts them to SOAP requests and forwards them
     * to the C1 processItem endpoint
     * 
     * @throws java.lang.Exception
     */
    public void processItems() throws Exception {
        SoapJaxbDataFormat soapProcessItem = new SoapJaxbDataFormat(ProcessItem.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","processItem")));
        SoapJaxbDataFormat soapProcessItemResponse = new SoapJaxbDataFormat(ProcessItemResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","processItemResponse")));
        
        from("direct-vm:processItem").routeId("process-item-route")
            .streamCaching()
            .process(new Processor(){
                @Override
                public void process(Exchange exchange) throws IOException{
                    String body = exchange.getIn().getBody(String.class);
                    Gson gson = new Gson();
                    JsonParser parser = new JsonParser();
                    ItemMaster item;

                    try {
                        JsonObject object = (JsonObject) parser.parse(body);
                        item = gson.fromJson(object, ItemMaster.class);
                    }
                    catch (Exception exception) {
                        item = exchange.getIn().getBody(ItemMaster.class);
                    }

                    ProcessItem processItemObj = new ProcessItem();
                    processItemObj.setIn0(item);
                    
                    exchange.getIn().setBody(processItemObj, ProcessItem.class);
                    /*ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    JsonNode jsonNode = objectMapper.readTree(body);
                    ItemMaster item = new ItemMaster();*/
                }
            })
            //.marshal().json(JsonLibrary.Gson)
            //.marshal(soapProcessItem)
            .marshal(soapProcessItem)
			.removeHeaders("*")
			//.setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.POST))
            //.setHeader("SOAPAction", constant("processItem"))
			//.setHeader(Exchange.HTTP_URI, simple("{{c1.conceptaccess.url}}"))
            //.setHeader("Authorization", simple(Base64.getEncoder().encodeToString("${c1.WSUser}:${c1.WSPassword}".getBytes(StandardCharsets.UTF_8))))
            //.toD("http4://processItemPlaceholderURI?throwExceptionOnFailure=false&authMethod=Basic&authUsername={{c1.WSUser}}&authPassword={{c1.WSPassword}}&authenticationPreemptive=true");
            .doTry()
                .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
                // .log("Body is ${in.body}")
                .unmarshal(soapProcessItemResponse)
                .process(new Processor(){
                    @Override
                    public void process(Exchange exchange){
                        ProcessItemResponse itemResponse = exchange.getIn().getBody(ProcessItemResponse.class);
                        String key = itemResponse.getProcessItemReturn().getKey().getValue();
                        String[] keyData = key.split("\\|\\|");
                        exchange.getIn().setHeader("itemRefNum", keyData[0]);

                        String message = itemResponse.getProcessItemReturn().getMessage().getValue();
                        exchange.setProperty("respMessage", message);
                    }
                })
                .log("Headers are ${in.headers}")
                .to("direct-vm:getItem")
            .doCatch(Exception.class)
                .setProperty("Created", constant("False"))
                .setBody(constant(null))
                .log(LoggingLevel.ERROR, "An error occurred in Configure One")
            .end();

        // GsonDataFormat gdf = new GsonDataFormat();
        // gdf.setUnmarshalType(Optional.class);
        // ItemList custResp = new ItemList();
        from("direct-vm:processItemList").routeId("processItemListDirect")
            .streamCaching()
            // Splits body message into individual items using getItems method from ItemList class (from C1RestRoutesBuilder)
            .split(body().method("getItems"), new ItemBatchProcessingStrategy())
                // process to create item
                .process(this::checkSmartpartnum)
                .setProperty("Created", constant("True"))
                .doTry()
                    .to("direct-vm:processItem")
                .doCatch(Exception.class)
                    .setProperty("Created", constant("False"))
                .end()
            .end()
            .process(this::finalizeResponse);
    }
    
    protected void checkSmartpartnum(Exchange ex) {
        Gson gson = new Gson();
        ItemMaster body = ex.getIn().getBody(ItemMaster.class);
        if ((body.getSMARTPARTNUM() != null) && (body.getSMARTPARTNUM().length() != 0)) {
            ex.setProperty("smartpartnum", body.getSMARTPARTNUM());
        }
    }
    
    protected void formatBody(Exchange ex) {
        ItemMaster item = ex.getIn().getBody(ItemMaster.class);
        String str = item.toString();
        ex.getIn().setBody(str);
    }
    
     /**
     * Reformats body to list of customers; sets content_type header to "application/json"
     */
    protected void finalizeResponse(Exchange ex) {
        Gson gson = new Gson();
        String oldStr = ex.getIn().getBody(String.class);
        List<ItemResponse> itemList = gson.fromJson(oldStr, List.class);

        ex.getIn().setBody(itemList);
        ex.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
    }
    
    protected void setBodyFromOptional(Exchange exchange) {
        Optional optional = exchange.getIn().getBody(Optional.class);
        if (optional != null && optional.isPresent()) {            
            exchange.getIn().setBody(optional.get());
            exchange.getIn().removeHeader(Exchange.CONTENT_TYPE);
        }
        else if (optional != null && !optional.isPresent()) {
            RestErrorResponse rest404Error = new RestErrorResponse(404, "Entity Not Found.");
            rest404Error.setOnExchange(exchange);
        }
        else {
            RestErrorResponse rest500Error = new RestErrorResponse(500, "Invalid response from endpoint. " + exchange.getIn().getBody(String.class));
            rest500Error.setOnExchange(exchange);
        }
    }
    
    public void processItemPrice() throws Exception {
        SoapJaxbDataFormat soapProcessItemPrice = new SoapJaxbDataFormat(ProcessItemPrice.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","processItemPrice")));
        SoapJaxbDataFormat soapProcessItemPriceResponse = new SoapJaxbDataFormat(ProcessItemPriceResponse.class.getPackage().getName(), new QNameStrategy(new QName("http://ws.configureone.com","processItemPriceResponse")));
        
        from("direct-vm:ProcessItemPriceRoute").routeId("process-item-price-route")
            .streamCaching()
            .log(LoggingLevel.INFO, "process-item-price-route: route called")
                .setProperty("message",simple("${body}"))
            .process(new Processor(){
                @Override
                public void process(Exchange exchange) throws IOException{
                    com.configureone.integration.connect.core.model.ProcessItemPrice body = exchange.getIn().getBody(com.configureone.integration.connect.core.model.ProcessItemPrice.class);
                                
                    exchange.setProperty("idOrName", "");
                    
                    if(body.getPRICEBOOK_ID().isEmpty()){
                        
                        if(!body.getPRICEBOOK_NAME().isEmpty()){
                               String priceBookName = body.getPRICEBOOK_NAME();
                                exchange.setProperty("idOrName", "name");
                                exchange.setProperty("bookName", priceBookName);
                        }
                    }else{
                        exchange.setProperty("idOrName", "id");
                        exchange.setProperty("pricebookId", body.getPRICEBOOK_ID());
                        exchange.setProperty("noPriceBook", "1");
                     
                    }
                }
            })
            .log(LoggingLevel.INFO, "process-item-price-route: exchangeProperty idOrName = ${exchangeProperty.idOrName}")
            .choice()
                .when(simple("${exchangeProperty.idOrName} == 'name' "))
                    .log(LoggingLevel.INFO, "process-item-price-route: get pricebook id from c1")
                         .to("direct-vm:GetPriceBookSQL")            
                         .end()
                .choice()
                .when(simple("${exchangeProperty.noPriceBook} == '1' "))  
                .log(LoggingLevel.INFO, "process-item-price-route: idRoute pricebook id = ${exchangeProperty.pricebookId} ")
                                .process(new Processor(){
                                        @Override
                                        public void process(Exchange exchange) throws IOException{
                                            com.configureone.integration.connect.core.model.ProcessItemPrice body = exchange.getProperty("message", com.configureone.integration.connect.core.model.ProcessItemPrice.class);

                                            ProcessItemPrice c1ProcessItemPrice = new ProcessItemPrice();
                                            
                                            ItemPrice ip = new ItemPrice();
                                            
                                            ip.setSMARTPARTNUM(body.getSMARTPART_NUM());
                                            ip.setPRICE(body.getPRICE());
                                            ip.setPRICEBOOKID(exchange.getProperty("pricebookId", String.class));
                                            
                                            c1ProcessItemPrice.setIn0(ip);
                                           
                                             exchange.getIn().setBody(c1ProcessItemPrice,ProcessItemPrice.class);
                                        }
                                    })
                    .marshal(soapProcessItemPrice)
                    .removeHeaders("*")
                    .log(LoggingLevel.INFO, "process-item-price-route: call c1 ")

                    .to(CoreConnectRoutes.CALL_CONFIGURE_ONE_EXTERNAL_SYNC_ROUTE.getConfiguredURI())
                    .unmarshal(soapProcessItemPriceResponse)
                    .process(this::parseItemPriceResponse)
                    .removeHeaders("*")

                
                    .end();


          from("direct-vm:GetPriceBookSQL").routeId("get-price-book-sql")
                  .streamCaching()
                  .toD(String.format("sql:%s ?dataSource=c1SQLDataSource",sqlGetPriceBookId))
                        .log(LoggingLevel.INFO, "process-item-price-route: pricebooks - ${body}")
                        .process(this::parseSqlResponse)
                        .choice()
                            .when(simple("${exchangeProperty.noPriceBook} == 'multi' ||  ${exchangeProperty.noPriceBook} == '0' "))
                                .log(LoggingLevel.INFO, "process-item-price-route: multiple or no price books found error....")
                                 .process(this::parseItemPriceResponse)
                                 .removeHeaders("*")
                                .endChoice()
                            .otherwise()
                                .log(LoggingLevel.INFO, "process-item-price-route: NameRoute pricebook id = ${exchangeProperty.pricebookId} ")
                                 .endChoice();     
 
        
    } 
        
   protected void parseItemPriceResponse(Exchange exchange) {
       
            logger.log(Level.INFO, "parseItemPriceResponse - processor called");
            
                String key ="";
                String message ="";
                String success ="";

            if(exchange.getProperty("noPriceBook", String.class).equals("1")){
                ProcessItemPriceResponse itemResponse = exchange.getIn().getBody(ProcessItemPriceResponse.class);
                Response response = itemResponse.getProcessItemPriceReturn();

                key = response.getKey().getValue();
                message = response.getMessage().getValue();
                success = Boolean.toString(response.isSuccess());
            }else if(exchange.getProperty("noPriceBook", String.class).equals("0")){
                key = "";
                message = "Invalid PriceBook Name - " + exchange.getProperty("bookName", String.class);
                success ="false";
            }else if(exchange.getProperty("noPriceBook", String.class).equals("multi")){
                List<String> ids = new ArrayList<>();
                ids = (List<String>) exchange.getProperty("bookList");
                key = ids.toString();
                message = "Multiple Price Books identified";
                success ="false";
            
            }
            
            com.configureone.integration.connect.core.model.ProcessItemPriceResponse jsonResponse = new com.configureone.integration.connect.core.model.ProcessItemPriceResponse();
            
           jsonResponse.setKey(key);
           jsonResponse.setMessage(message);
           jsonResponse.setSuccess(success);
            
           exchange.getIn().setBody(jsonResponse, com.configureone.integration.connect.core.model.ProcessItemPriceResponse.class);

        

    }
        
        
        
        
        
        
        
        
        
        
    protected void parseSqlResponse(Exchange exchange) {
            logger.log(Level.INFO, "parseSqlResponse -  processor called");
            
            List<String> ids = new ArrayList<>();

            List<Map<String, Object>> sql = (List<Map<String, Object>>) exchange.getIn().getBody();

            for (Map<String, Object> map : sql) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                            
                            String value ="";
                            if(entry.getValue() != null){
                                
                                Object valueOb = entry.getValue();
                                value = valueOb.toString();
                            }
                    
                    if(key.equals("PRICE_BOOK_ID")){
                    ids.add(value);
                    
                    }

            }


        }
                logger.log(Level.INFO, "parseSqlResponse - size - " + ids.size());
                if(ids.size() == 1){
                    exchange.setProperty("pricebookId", ids.get(0));
                    exchange.setProperty("noPriceBook", "1");
                    logger.log(Level.INFO, "parseSqlResponse - one id found - " + ids.get(0));

                }else if(ids.size() > 1){
                    exchange.setProperty("noPriceBook", "multi");
                    exchange.setProperty("bookList", ids);
                    logger.log(Level.INFO, "parseSqlResponse - multiple ids found - " + ids.toString());

                 }else if(ids.isEmpty()){
                    exchange.setProperty("noPriceBook", "0");
                    logger.log(Level.INFO, "parseSqlResponse - no  id found - " );

                 }

    }

    private void elseif(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

