/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.search.routes;

import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.model.AdvancedSearch;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Params;
import com.configureone.integration.connect.core.model.QueryResult;
import com.configureone.integration.connect.core.model.QueryResult.OrderBy;
import com.configureone.integration.connect.core.model.QueryResultRecord;
import com.configureone.integration.connect.core.model.SearchTableFields;
import com.configureone.integration.connect.core.model.TableJoins;
import com.configureone.integration.connect.core.model.WhereStatement;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.orderBy;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * Hello world! route provided with the archetype. This is used as a placeholder
 * for the routes package. It is also used in some example unit tests.
 */
public class SearchRouteBuilder extends RouteBuilder {

    /**
     * Logger for the class.
     */
    protected final Logger logger = LogManager.getLogger(getClass());

    public static final String entityTypeHeaderName = "recordType";
    public static final String searchStringHeaderName = "q";
    public static final String orderByHeaderName = "orderBy";
    public static final String pageSizeHeaderName = "pageSize";
    public static final String pageNumberHeaderName = "pageNumber";

    protected static final ConnectExchangeProperty<String> recordType = new ConnectExchangeProperty("recordType", String.class);
    protected static final ConnectExchangeProperty<String> searchString = new ConnectExchangeProperty("searchString", String.class);
    protected static final ConnectExchangeProperty<QueryResult.OrderBy> orderBy = new ConnectExchangeProperty("orderBy", QueryResult.OrderBy.class);
    protected static final ConnectExchangeProperty<String> tableName = new ConnectExchangeProperty("tableName", String.class);
    protected static final ConnectExchangeProperty<String> idField = new ConnectExchangeProperty("idField", String.class);
    protected static final ConnectExchangeProperty<String> nameField = new ConnectExchangeProperty("nameField", String.class);
    protected static final ConnectExchangeProperty<String> descriptionField = new ConnectExchangeProperty("descriptionField", String.class);
    protected static final ConnectExchangeProperty<String> restIDField = new ConnectExchangeProperty("restIDField", String.class);
    protected static final ConnectExchangeProperty<String> restURLBasePath = new ConnectExchangeProperty("restURLBasePathField", String.class);
    protected static final ConnectExchangeProperty<List<String>> fieldsToSearch = new ConnectExchangeProperty("fieldsToSearchCSV", List.class);
    protected static final ConnectExchangeProperty<Integer> pageSize = new ConnectExchangeProperty("pageSize", Integer.class);
    protected static final ConnectExchangeProperty<Integer> pageNumber = new ConnectExchangeProperty("pageNumber", Integer.class);
    protected static final ConnectExchangeProperty<String> countQueryProperty = new ConnectExchangeProperty("countQuery", String.class);
    protected static final ConnectExchangeProperty<String> resultQueryProperty = new ConnectExchangeProperty("resultQuery", String.class);
    protected static final ConnectExchangeProperty<Integer> totalResults = new ConnectExchangeProperty("totalResults", Integer.class);
    protected static final ConnectExchangeProperty<String> filterField = new ConnectExchangeProperty("filterField", String.class);
    protected static final ConnectExchangeProperty<String> filterFieldMatches = new ConnectExchangeProperty("filterFieldMatches", String.class);
    protected static final ConnectExchangeProperty<String> filterFieldNotMatches = new ConnectExchangeProperty("filterFieldNotMatches", String.class);
    protected static final ConnectExchangeProperty<String> filterFieldIn = new ConnectExchangeProperty("filterFieldIn", String.class);
    protected static final ConnectExchangeProperty<Integer> noSearchFields = new ConnectExchangeProperty("noSearchFields", Integer.class);


    /**
     * Overridden RouteBuilder configure() method. Used to define a simple route
     * that sets it's body to "Hello World!"
     *
     * @throws Exception Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(noErrorHandler());

        searchByRecordType();
        advancedSearch();
    }

    /**
     *
     */
    public void advancedSearch() {
        from("direct-vm:AdvancedSearch").routeId("directVMAdvancedSearch")
                .streamCaching()
                .choice()
                    .when((header(searchStringHeaderName).isNull()))
                        .throwException(new ConnectBusinessException("directVMAdvancedSearch - No search value was passed."))
                        .endChoice()
                     .when((header(entityTypeHeaderName).isNull()))
                        .throwException(new ConnectBusinessException("directVMAdvancedSearch - No record type was passed."))
                        .endChoice()
                    .otherwise()
                        .process(this::massageValidation)
                        .choice()
                            .when(exchangeProperty("tableName").isNull())
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No Table value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("tableName").isEqualTo(""))
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No Table value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("idField").isNull())
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No idField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("idField").isEqualTo(""))
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No idField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("nameField").isNull())
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No NameField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("nameField").isEqualTo(""))
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No NameField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("descriptionField").isNull())
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No DescriptionField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("descriptionField").isEqualTo(""))
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No DescriptionField value was passed in message body."))
                                    .endChoice()
                            .when(exchangeProperty("noSearchFields").isEqualTo(0))
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No SearchTableFields were provided in message body."))
                                    .endChoice()
                            .when(exchangeProperty("noSearchFields").isNull())
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - No SearchTableFields were provided in message body."))
                                    .endChoice()
                            .when(simple("${exchangeProperty.recordType} != 'quotes' && ${exchangeProperty.recordType} != 'orders' && ${exchangeProperty.recordType} != 'configurations'"))  
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - Incorrect record type, please specfy a value of configurations, quotes, or orders"))
                                    .endChoice()
                            .when(simple("${exchangeProperty.recordType} == 'quotes' && ${exchangeProperty.tableName} != 'CO_DOC'"))  
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - Incorrect Table, you have chosen a recordType of "+ recordType + " please use CO_DOC as the Table" ))
                                    .endChoice()
                            .when(simple("${exchangeProperty.recordType} == 'configurations' && ${exchangeProperty.tableName} != 'CO_DES'"))  
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - Incorrect Table, you have chosen a recordType of "+ recordType + " please use CO_DES as the Table" ))
                                    .endChoice()
                            .when(simple("${exchangeProperty.recordType} == 'orders' && ${exchangeProperty.tableName} != 'CO_DOC'"))  
                                    .throwException(new ConnectBusinessException("directVMAdvancedSearch - Incorrect Table, you have chosen a recordType of "+ recordType + " please use CO_DOC as the Table" ))
                                    .endChoice()
                           .otherwise()
                                .process(this::createQuery)
                                .setBody(countQueryProperty::getPropertyData)
                                .to("sql:placeholderSQL?useMessageBodyForSql=true&dataSource=c1SQLDataSource")
                                .process(this::parseCountQueryResult)
                                .setBody(resultQueryProperty::getPropertyData)
                                .to("sql:placeholderSQL?useMessageBodyForSql=true&dataSource=c1SQLDataSource")
                                .log(LoggingLevel.INFO, "sql body -  ${body}")
                                .endChoice()

                    .endChoice()
                .end()
                .process(this::generateSearchResultProcessor);

    }
    
     protected void massageValidation(Exchange exchange) throws ConnectBusinessException, Exception {
        logger.log(Level.INFO, "massageValidation - message validation called");
        
        // get  the body of the search message 
        AdvancedSearch as = exchange.getIn().getBody(AdvancedSearch.class);
        
        // define the fields used in the response payload
        String sqlTableName;
        String idFieldname; 
        String nameFieldname;
        String descriptionFieldname;
        String restIDFieldname;
        int numberSearchFields = 0;
        

             if(as.getTable() != null){
                sqlTableName = as.getTable().toUpperCase();
            }else{
                throw new ConnectBusinessException("massageValidation - No Table was provided in message body, please specify a Table to look at");
            }
            if(as.getIdField() != null){
                idFieldname = as.getIdField();
            }else{
                throw new ConnectBusinessException("massageValidation - No IdField was provided in message body, please specify the IdField of the Table");
            }           
            if(as.getNameField() != null){
                nameFieldname = as.getNameField();
            }else{
                throw new ConnectBusinessException("massageValidation - No NameField was provided in message body, please specify a NameField that will be used populate the query response");
            }
            if(as.getDescriptionField() != null){
                descriptionFieldname = as.getDescriptionField();
            }else{
                throw new ConnectBusinessException("massageValidation - No DescriptionField was provided in message body, please specify a DescriptionField that will be used to populate the query response");
            }
            if(as.getRestIdField() != null){
               restIDFieldname = as.getRestIdField();
            }else{
                throw new ConnectBusinessException("massageValidation - No RestIdField was provided in message body, please specify a RestIdField that will be used to populate the query response");
            }
            if(restIDFieldname.isEmpty()){
                restIDFieldname = as.getRestIdField();
            }else{
                restIDFieldname = "";
            }
            if(as.getSearchTableFields() != null){
               numberSearchFields = as.getSearchTableFields().length;
            }else{
               throw new ConnectBusinessException("massageValidation - No SearchTableFields were provided in message body");
            }    
            if (StringUtils.isEmpty(restIDFieldname)) {
               restIDFieldname = descriptionFieldname;
            }
            
        try{
            tableName.setPropertyData(exchange, sqlTableName);
            idField.setPropertyData(exchange, idFieldname);
            nameField.setPropertyData(exchange, nameFieldname);
            descriptionField.setPropertyData(exchange, descriptionFieldname);
            restIDField.setPropertyData(exchange, restIDFieldname);
            noSearchFields.setPropertyData(exchange,numberSearchFields);
            String recordTypeValue = exchange.getIn().getHeader(entityTypeHeaderName, String.class);
           recordType.setPropertyData(exchange, recordTypeValue.toLowerCase());
            //logger.log(Level.INFO, "massageValidation -  recordType = " + recordType.getPropertyData(exchange));
            restURLBasePath.setPropertyData(exchange, "/rest/"+ recordTypeValue.toLowerCase()+"/");
        }catch (Exception e) {
            throw new ConnectBusinessException("massageValidation - Could preform massageValidation - failed to set exchange properties");
        }
        
     }

    protected void validateTableJoinFields(TableJoins join) throws ConnectBusinessException, Exception {
            logger.log(Level.INFO, "validateTableJoinFields -  processor called");
            if(join.getType() == null){
                throw new ConnectBusinessException("validateTableJoinFields - Incorrect TableJoins definition, TableJoins must contain Type field - data recieved = " + join.toString());
            }else{
            
                if(!join.getType().equalsIgnoreCase("LEFT") && !join.getType().equalsIgnoreCase("INNER") && !join.getType().equalsIgnoreCase("RIGHT") && !join.getType().equalsIgnoreCase("FULL") && !join.getType().equalsIgnoreCase("FULL OUTER") ){
                    throw new ConnectBusinessException("validateTableJoinFields - Incorrect TableJoins definition, inorrect value passed for Type field. Type must equal one of the following: 'LEFT', 'RIGHT', 'INNER', 'FULL', or 'FULL OUTER' - data recieved = " + join.toString());
                }
            }
            if(join.getTable() == null){
                throw new ConnectBusinessException("validateTableJoinFields - Incorrect TableJoins definition, TableJoins must contain Table field - data recieved = " + join.toString());
            }
            if(join.getJoinField1() == null){
                throw new ConnectBusinessException("validateTableJoinFields - Incorrect TableJoins definition, TableJoins must contain JoinField1 field - data recieved = " + join.toString());
            }
            if(join.getJoinField2() == null){
                throw new ConnectBusinessException("validateTableJoinFields - Incorrect TableJoins definition, TableJoins must contain JoinField2 field - data recieved = " + join.toString());
            }
    }
     
    protected void createQuery(Exchange exchange) throws ConnectBusinessException, Exception {
        logger.log(Level.INFO, "createQuery -  processor called");
        
        // define the fields used in the response payload
        String idFieldname = exchange.getProperty("idField", String.class);
        String nameFieldname = exchange.getProperty("nameField", String.class);
        String descriptionFieldname = exchange.getProperty("descriptionField", String.class);
        String restIDFieldname = exchange.getProperty("restIDField", String.class);

        // get  the body of the search message 
        AdvancedSearch as = exchange.getIn().getBody(AdvancedSearch.class);

        // define the string used to construct the table joins
        String joins = "";
        
        // define the string used in the WHERE that contains the fields we look in with the search string
        String SearchString = "";

        // define the table to look at
        String sqlTableName = exchange.getProperty("tableName", String.class);
        String recordTypeValue = exchange.getIn().getHeader(entityTypeHeaderName, String.class);
        String searchStringValue = exchange.getIn().getHeader(searchStringHeaderName, String.class);
        String orderByStringValue = exchange.getIn().getHeader(orderByHeaderName, String.class);
        String pageSizeStringValue = exchange.getIn().getHeader(pageSizeHeaderName, String.class);
        String pageNumberStringValue = exchange.getIn().getHeader(pageNumberHeaderName, String.class);

        // Set the search string (this is the value we look for in the fields provided)    
        String likeSearchString = "'%" + searchStringValue + "%'";
        
        recordType.setPropertyData(exchange, recordTypeValue);
        searchString.setPropertyData(exchange, searchStringValue);
        
        if (StringUtils.isEmpty(pageSizeStringValue)) {
            pageSize.setPropertyData(exchange, 10);
        } else {
            if (pageSizeStringValue.matches("^\\d{1,3}$")) {
                pageSize.setPropertyData(exchange, Integer.parseInt(pageSizeStringValue));
            } else {
                pageSize.setPropertyData(exchange, 10);
            }
        }
        if (StringUtils.isEmpty(pageNumberStringValue)) {
            pageNumber.setPropertyData(exchange, 1);
        } else {
            if (pageNumberStringValue.matches("^\\d{1,3}$")) {
                pageNumber.setPropertyData(exchange, Integer.parseInt(pageNumberStringValue));
            } else {
                pageNumber.setPropertyData(exchange, 1);
            }
        }
        OrderBy orderByEnumValue;
        if (!StringUtils.isEmpty(orderByStringValue)) {
            orderByEnumValue = QueryResult.OrderBy.valueOf(orderByStringValue);
        } else {
            orderByEnumValue = OrderBy.NAME;
        }
        orderBy.setPropertyData(exchange, orderByEnumValue);
        
        if(as.getTableJoins() != null){
            // create table joins section
            TableJoins[] tj = as.getTableJoins();
                if (tj.length > 0) {

                    //logger.log(Level.INFO, "createQuery - query has table joins = " + tj.length);
                    int i;
                    for (i = 0; i < tj.length; i++) {
                        // validate all fields are populated as required
                        validateTableJoinFields(tj[i]);

                        String joinType = tj[i].getType().toUpperCase() + " JOIN";
                        String joinTable = tj[i].getTable();
                        String joinFiled1 = tj[i].getJoinField1();
                        String joinFiled2 = tj[i].getJoinField2();

                        if (!joinType.isEmpty() && !joinTable.isEmpty() && !joinFiled1.isEmpty() && !joinFiled2.isEmpty()) {

                            String join = joinType + " " + joinTable + " ON " + joinFiled1 + " = " + joinFiled2 + " ";

                            joins = joins + join;
                        }
                    }
                }
                //logger.log(Level.INFO, "createQuery - table joins  = " + joins);
        }


        
        // get search fields Array from the message
        SearchTableFields[] searchfields = as.getSearchTableFields();
        
        
        
        // create searchfields section
        if (searchfields.length > 0) {
           //logger.log(Level.INFO, "createQuery - number of search fields = " + searchfields.length);
            int i;
            for (i = 0; i < searchfields.length; i++) {
                String field = searchfields[i].getField();
                String search = field + " LIKE " + likeSearchString;
                if (i == 0) {
                    SearchString = "(" + search;
                } else {
                    SearchString = SearchString + " OR " + search;
                }

                if (i == searchfields.length - 1) {
                    SearchString = SearchString + ")";
                }
            }
        }
        
         String WhereStatments = "";
         
        //get user defined where statments from message
        if(as.getWhereStatement() != null){
            WhereStatement[] whereStatments = as.getWhereStatement();
            //logger.log(Level.INFO, "createQuery - number of whereStatments = " + whereStatments.length);
            //<editor-fold defaultstate="collapsed" desc="process were statments">
            if (whereStatments.length > 0) {

                int i;
                for (i = 0; i < whereStatments.length; i++) {
                    String response = "";

                    //logger.log(Level.INFO, "processing whereStatment  = " + i);

                    if(whereStatments[i].getType() == null || whereStatments[i].getParams() == null){
                                throw new ConnectBusinessException("createQuery - Incorrect definition for WhereStatement, WhereStatement must contain the following paramaters:  Type, Params[] - data recieved = " + whereStatments[i].toString());
                    }

                    String type = whereStatments[i].getType();

                    // check if type is Eval, if so only process first param set.
                    Params[] param = whereStatments[i].getParams();
                    //logger.log(Level.INFO, "whereStatments param length = " + param.length);

                    if(type.equalsIgnoreCase("Eval") || type.equalsIgnoreCase("Or") || type.equalsIgnoreCase("AND")){

                        if (type.equalsIgnoreCase("Eval")) {
                            logger.log(Level.INFO, "createQuery - processing EVAL statment ");
                            response = processEval(param[0]);
                        }

                        if(type.equalsIgnoreCase("Or") || type.equalsIgnoreCase("AND")) {
                            logger.log(Level.INFO, "createQuery - processing OR / AND statment ");
                            String orStatment = "";
                            String resp = "";
                            resp = processAnd(param, type);
                            response = resp;
                            //logger.log(Level.INFO, "whereStatment OR condition =  " + orStatment);
                        }
                    }else{
                        throw new ConnectBusinessException("createQuery - Incorrect value passed for Type, please specify a string of Or, Eval, or And  - data recieved = " + whereStatments[i].toString());
                    }
                    if (i == 0) {
                        WhereStatments = response;
                    } else {
                        WhereStatments = WhereStatments + " AND " + response;
                    }
                }

               // logger.log(Level.INFO, "WhereStatments string = " + WhereStatments);
                WhereStatments = "(" + WhereStatments  + ")";
            }
            //</editor-fold>
            
        }

        //logger.log(Level.INFO, "createQuery - search string = " + SearchString);

        String query = "select * from " + sqlTableName;

        pageSize.setPropertyData(exchange, 10);
        pageNumber.setPropertyData(exchange, 1);

        int requestedPageSize = pageSize.getPropertyData(exchange);
        int requestedPageNumber = pageNumber.getPropertyData(exchange);
        int requestedFirstRecord = (requestedPageSize * (requestedPageNumber - 1)) + 1;
        int requestedLastRecord = requestedFirstRecord + requestedPageSize - 1;
        
        String whereClauseSQL = SearchString;
        
        if(!WhereStatments.isEmpty()){
            //logger.log(Level.INFO, "createQuery - WhereStatments is NOT empty");

            if(recordType.getPropertyData(exchange).equalsIgnoreCase("orders")){
                    WhereStatments = WhereStatments + " AND CO_DOC.orderNumber IS NOT NULL";
            }else if(recordType.getPropertyData(exchange).equalsIgnoreCase("quotes")){
                    WhereStatments = WhereStatments + " AND CO_DOC.orderNumber IS NULL";
            }
            
            whereClauseSQL =  " where " + whereClauseSQL + " And " + WhereStatments;

        } else {
            //logger.log(Level.INFO, "createQuery - WhereStatments IS empty");

            if(recordType.getPropertyData(exchange).equalsIgnoreCase("orders")){
                    WhereStatments = WhereStatments + " CO_DOC.orderNumber IS NOT NULL";
            }else if(recordType.getPropertyData(exchange).equalsIgnoreCase("quotes")){
                    WhereStatments = WhereStatments + " CO_DOC.orderNumber IS NULL";
            }
            whereClauseSQL =  " where " + whereClauseSQL + " AND " + WhereStatments;
        }

        

        String countQuery = "select COUNT(" + idFieldname + ") as ResultCount from " + sqlTableName + " " + joins + whereClauseSQL;
        countQueryProperty.setPropertyData(exchange, countQuery);

        String querySQL = "select "
                + "idField" + ", "
                + "nameField" + ", "
                + "descriptionField" + ", "
                + "restIDField"
                + " from "
                + "( select "
                + "ROW_NUMBER() OVER (ORDER BY " + nameFieldname + ") AS RowNum, "
                + idFieldname + " as " + "idField" + ", "
                + nameFieldname + " as " + "nameField" + ", "
                + descriptionFieldname + " as " + "descriptionField" + ", "
                + restIDFieldname + " as " + "restIDField"
                + " FROM "
                + sqlTableName + " "
                + joins
                + whereClauseSQL.toString()
                + ") AS RowConstrainedResult"
                + " WHERE "
                + "RowNum >= " + requestedFirstRecord + " AND RowNum <= " + requestedLastRecord
                + " ORDER BY RowNum";

        logger.log(Level.INFO, "Query = " + querySQL);

        resultQueryProperty.setPropertyData(exchange, querySQL);

    }
    
    public void checkParams(Params param, String type) throws ConnectBusinessException, Exception {
          logger.log(Level.INFO, "checkParams  - processor called");
        
        if(type.equalsIgnoreCase("And") || type.equalsIgnoreCase("Or")){
            if(param.getType() == null || param.getParams() == null){
                throw new ConnectBusinessException("checkParams - Incorrect Params definition in "+ type +" param block, Params must contain the following paramaters:  Type, Params[] - data recieved = " + param.toString());
            }else if(!param.getType().equalsIgnoreCase("Eval") && !param.getType().equalsIgnoreCase("And") && !param.getType().equalsIgnoreCase("Or")){
                    throw new ConnectBusinessException("checkParams - Incorrect value passed for Type, please specify a string of Or, Eval, or And - data recieved = " +  param.toString());
            }else if(param.getType().equalsIgnoreCase("And") || param.getType().equalsIgnoreCase("Or")){
                if(param.getArg1() != null || param.getArg1Type() != null || param.getArg2() != null || param.getArg2Type() != null || param.getOperation() !=null){
                    throw new ConnectBusinessException("checkParams - Incorrect Params definition in "+ type +" param block, Params must NOT contain the following paramaters:  Arg1Type, Arg1, Operation, Arg2Type, Arg2 - data recieved = " + param.toString());
                }
            }
        }else if(type.equalsIgnoreCase("Eval")){
            if(param.getArg1Type() == null || param.getArg1() == null || param.getArg2Type() == null || param.getArg2() == null || param.getOperation() == null){
                throw new ConnectBusinessException("checkParams - Incorrect definition of Eval statment, Eval statment must contain the following paramaters: Arg1Type, Arg1, Operation, Arg2Type, Arg2 - data recieved = " + param.toString());
            }else if(param.getType() != null){
                throw new ConnectBusinessException("checkParams - Incorrect definition of Eval statment, Type should not be specified in paramaters  - data recieved = " + param.toString());
            }else if(param.getParams() != null){
                throw new ConnectBusinessException("checkParams - Incorrect definition of Eval statment, Params should not be specified in paramaters  - data recieved = " + param.toString());
            }
            if(param.getOperation() != null){
                String operation = param.getOperation();
                //logger.log(Level.INFO, "checkParams  - operation = " + operation);
                if(!operation.equals("=") && !operation.equals("<") && !operation.equals(">") && !operation.equals(">=") && !operation.equals("<=") && !operation.equals("<>")){
                    throw new ConnectBusinessException("checkParams - Incorrect definition of Eval statment, operation is invalid. operation must equal one of the following: '=', '<', '>', '>=', '<=', '<>' - data recieved = " + param.toString());
                }
            }
        }
    }
    
    public String processAnd(Params[] params, String andOr) throws ConnectBusinessException, Exception {
        logger.log(Level.INFO, "processAnd - processor called");
        
        checkParams(params[0], andOr);
        
        String Statment = "";
        int p;
        for (p = 0; p < params.length; p++) {
           // logger.log(Level.INFO, "processAnd - current param statment = " + p);
            
            checkParams(params[p], andOr);

            String Type = params[p].getType();
            String resp = "";
            
            if(Type.equalsIgnoreCase("Eval") || Type.equalsIgnoreCase("And") || Type.equalsIgnoreCase("Or")){

                Params[] evalParam = params[p].getParams();
                
                if (Type.equalsIgnoreCase("Eval")) {
                    
                    resp = processEval(evalParam[0]);
                }
                if (Type.equalsIgnoreCase("And")) {
                    resp = processAnd(evalParam, "AND");
                }
                if (Type.equalsIgnoreCase("Or")) {
                    
                    resp = processAnd(evalParam, "Or");
                }
            }


            if (p == 0) {
                Statment = resp;
            } else {

                Statment = Statment + " " + andOr + " " + resp;

            }

        }

        Statment = "(" + Statment + ")";

        //logger.log(Level.INFO, "And statment = " + Statment);

        return Statment;
    }
    
    public String processEval(Params param) throws ConnectBusinessException, Exception {
        logger.log(Level.INFO, "processEval - processor called");
        String eval;
        
        checkParams(param, "Eval");

        String arg1Type = param.getArg1Type();
        String arg1 = "";
        String op = param.getOperation();
        String arg2Type = param.getArg2Type();
        String arg2= "";
        
        /*        if(param.getArg1Type() == null || param.getArg1() == null || param.getArg2Type() == null || param.getArg2() == null || param.getOperation() == null){
        throw new ConnectBusinessException("Incorrect definition of Eval statment, Eval statment must contain the following paramaters: Arg1Type, Arg1, Operation, Arg2Type, Arg2 - data recieved = " + param.toString());
        }else if(param.getType() != null){
        throw new ConnectBusinessException("Incorrect definition of Eval statment, Type should not be specified in paramaters  - data recieved = " + param.toString());
        }else if(param.getParams() != null){
        throw new ConnectBusinessException("Incorrect definition of Eval statment, Params should not be specified in paramaters  - data recieved = " + param.toString());
        }*/
        
        if(arg1Type.equalsIgnoreCase("Field") || arg1Type.equalsIgnoreCase("Value")){
            if(arg1Type.equalsIgnoreCase("Field")){
                arg1 = param.getArg1();
            }else{
                arg1 = "'" +param.getArg1() + "'";
            }
        }else {
            throw new ConnectBusinessException("processEval - Incorrect value passed for arg1Type, please provide a String of Field or Value  - data recieved = " + param.toString());
        }
        
        if(arg2Type.equalsIgnoreCase("Field") || arg2Type.equalsIgnoreCase("Value")){
            if(arg2Type.equalsIgnoreCase("Field")){
                arg2 = param.getArg2();
            }else{
                arg2 = "'" +param.getArg2() + "'";
            }
        }else {
           throw new ConnectBusinessException("processEval - Incorrect value passed for arg2Type, please provide a String of Field or Value  - data recieved = " + param.toString());
        }
        
        if(arg1Type.isEmpty() || arg2Type.isEmpty() ){
            throw new ConnectBusinessException("processEval - Incorrect definition of Eval statment, please provide values for arg1Type and arg2Type  - data recieved = " + param.toString());
        }
        
        if(arg1.isEmpty() && arg2.isEmpty() ){
            throw new ConnectBusinessException("processEval - Incorrect definition of Eval statment, please provide values for arg1 and/ or arg2  - data recieved = " + param.toString());
        }
        
        eval = arg1 + " " + op + " " +  arg2;
        //logger.log(Level.INFO, "Eval statment = " + eval);
         return eval;
    }

    public void searchByRecordType() {
        from("direct-vm:searchByRecordType").routeId("directVMSearchByRecordType")
                .choice()
                    .when((header(searchStringHeaderName).isNull()))
                    .throwException(new ConnectBusinessException("No search value was passed."))
                    .endChoice()
                .when((header(entityTypeHeaderName).isNull()))
                    .throwException(new ConnectBusinessException("No record type was passed."))
                    .endChoice()
                .otherwise()
                    .process(this::setSearchExchangePropertiesProcessor)
                    .process(this::createSQLForRecordTypeAndSearchText)
                    .setBody(countQueryProperty::getPropertyData)
                    .to("sql:placeholderSQL?useMessageBodyForSql=true&dataSource=c1SQLDataSource")
                    .process(this::parseCountQueryResult)
                    .setBody(resultQueryProperty::getPropertyData)
                    .to("sql:placeholderSQL?useMessageBodyForSql=true&dataSource=c1SQLDataSource")
                .endChoice()
                .end()
                .process(this::generateSearchResultProcessor);
    }
      
    /**
     *
     * @param exchange
     * @throws ConnectBusinessException
     */
    protected void setSearchExchangePropertiesProcessor(Exchange exchange) throws ConnectBusinessException {
        String recordTypeValue = exchange.getIn().getHeader(entityTypeHeaderName, String.class);
        String searchStringValue = exchange.getIn().getHeader(searchStringHeaderName, String.class);
        String orderByStringValue = exchange.getIn().getHeader(orderByHeaderName, String.class);
        String pageSizeStringValue = exchange.getIn().getHeader(pageSizeHeaderName, String.class);
        String pageNumberStringValue = exchange.getIn().getHeader(pageNumberHeaderName, String.class);

        recordType.setPropertyData(exchange, recordTypeValue);
        searchString.setPropertyData(exchange, searchStringValue);
        if (StringUtils.isEmpty(pageSizeStringValue)) {
            pageSize.setPropertyData(exchange, 10);
        } else {
            if (pageSizeStringValue.matches("^\\d{1,3}$")) {
                pageSize.setPropertyData(exchange, Integer.parseInt(pageSizeStringValue));
            } else {
                pageSize.setPropertyData(exchange, 10);
            }
        }
        if (StringUtils.isEmpty(pageNumberStringValue)) {
            pageNumber.setPropertyData(exchange, 1);
        } else {
            if (pageNumberStringValue.matches("^\\d{1,3}$")) {
                pageNumber.setPropertyData(exchange, Integer.parseInt(pageNumberStringValue));
            } else {
                pageNumber.setPropertyData(exchange, 1);
            }
        }
        OrderBy orderByEnumValue;
        if (!StringUtils.isEmpty(orderByStringValue)) {
            orderByEnumValue = QueryResult.OrderBy.valueOf(orderByStringValue);
        } else {
            orderByEnumValue = OrderBy.NAME;
        }
        orderBy.setPropertyData(exchange, orderByEnumValue);

        CamelContext exchangeContext;
        try {
            exchangeContext = exchange.getContext();
            String sqlTableName = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".tableName}}");
            String idFieldname = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".idField}}");
            String nameFieldname = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".nameField}}");
            String descriptionFieldname = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".descriptionField}}");
            String restIDFieldname = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".restIDField}}");
            if (StringUtils.isEmpty(restIDFieldname)) {
                restIDFieldname = idFieldname;
            }
            String restURLBasePathConfig = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".restURLBasePath}}");
            String fieldsToSearchCSV = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".fieldsToSearch}}");

            tableName.setPropertyData(exchange, sqlTableName);
            idField.setPropertyData(exchange, idFieldname);
            nameField.setPropertyData(exchange, nameFieldname);
            descriptionField.setPropertyData(exchange, descriptionFieldname);
            restIDField.setPropertyData(exchange, restIDFieldname);
            restURLBasePath.setPropertyData(exchange, restURLBasePathConfig);

            List<String> fieldnamesToSearch = Arrays.asList(fieldsToSearchCSV.split("\\s*,\\s*"));
            fieldsToSearch.setPropertyData(exchange, fieldnamesToSearch);
        } catch (Exception e) {
            throw new ConnectBusinessException("Could not retrieve query configuration(s) for requested object. "
                    + "Expected: \n"
                    + "{{textSearch." + recordTypeValue + ".tableName}}, "
                    + "{{textSearch." + recordTypeValue + ".idField}}, "
                    + "{{textSearch." + recordTypeValue + ".nameField}}, "
                    + "{{textSearch." + recordTypeValue + ".descriptionField}}, "
                    + "{{textSearch." + recordTypeValue + ".restIDField}} (optional), "
                    + "{{textSearch." + recordTypeValue + ".restURLBasePath}}, "
                    + "{{textSearch." + recordTypeValue + ".fieldsToSearch}}"
                    + "{{textSearch." + recordTypeValue + ".filterField}}"
                    + "{{textSearch." + recordTypeValue + ".filterFieldMatches}}"
                    + "{{textSearch." + recordTypeValue + ".filterFieldNotMatches}}",
                     e);
        }

        try {
            String filterFieldname = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".filterField}}");
            filterField.setPropertyData(exchange, filterFieldname);
            try {
                String filterFieldMatchesValue = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".filterFieldMatches}}");
                filterFieldMatches.setPropertyData(exchange, filterFieldMatchesValue);
            } catch (Exception e) {

            }
            try {
                String filterFieldNotMatchesValue = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".filterFieldNotMatches}}");
                filterFieldNotMatches.setPropertyData(exchange, filterFieldNotMatchesValue);
            } catch (Exception e) {

            }
            try {
                String filterFieldInValue = exchangeContext.resolvePropertyPlaceholders("{{textSearch." + recordTypeValue + ".filterFieldIn}}");
                filterFieldIn.setPropertyData(exchange, filterFieldInValue);
            } catch (Exception e) {

            }
        } catch (Exception e) {
            logger.info("Filter fields not retrieved for search record of type " + recordTypeValue + ". " + e.getMessage());
        }
    }

    /**
     *
     * @param exchange
     * @throws ConnectBusinessException
     */
    protected void generateSearchResultProcessor(Exchange exchange) throws ConnectBusinessException, Exception {
        logger.log(Level.INFO, "generateSearchResultProcessor - processor called");
        List<Map> queryResultsMap = exchange.getIn().getBody(List.class);
        String recordTypeValue = recordType.getPropertyData(exchange);
        OrderBy orderByEnumValue = orderBy.getPropertyData(exchange);
        String restURLBasePathConfig = restURLBasePath.getPropertyData(exchange);
        QueryResult queryResult = new QueryResult();
        queryResult.setType(recordTypeValue);
        List<QueryResultRecord> queryResultRecords = new ArrayList<>();
        exchange.getIn().removeHeader("CamelSqlRowCount");
        int resultsMapRowCount = 0;
        if (queryResultsMap != null) {
            resultsMapRowCount = queryResultsMap.size();
            for (Map row : queryResultsMap) {
                QueryResultRecord resultRecord = new QueryResultRecord();
                resultRecord.setId(String.valueOf(row.get(idField.getPropertyName())));
                resultRecord.setName(String.valueOf(row.get(nameField.getPropertyName())));
                resultRecord.setDescription(String.valueOf(row.get(descriptionField.getPropertyName())));
                if (!StringUtils.isEmpty(restURLBasePathConfig)) {
                    resultRecord.setUrl(restURLBasePathConfig + String.valueOf(row.get(restIDField.getPropertyName())));
                }
                queryResultRecords.add(resultRecord);
            }
        }
        queryResult.setRecordReferences(queryResultRecords);

        queryResult.setResultsSize(resultsMapRowCount);
        queryResult.setTotalResultSize(totalResults.getPropertyData(exchange));
        queryResult.setPageSize(pageSize.getPropertyData(exchange));
        queryResult.setPageNumber(pageNumber.getPropertyData(exchange));
        queryResult.setOrderedBy(orderByEnumValue);

        logger.log(Level.INFO, new Gson().toJson(queryResult));
        exchange.getIn().setBody(queryResult);
    }

    /**
     *
     * @param exchange
     * @throws ConnectBusinessException When configurations cannot be retrieved
     * for the requested object.
     */
    protected void createSQLForRecordTypeAndSearchText(Exchange exchange) throws ConnectBusinessException {
        //Generate WHERE condition for search
        StringBuilder whereClauseSQL = new StringBuilder();
        OrderBy orderByEnumValue = orderBy.getPropertyData(exchange);
        String orderByFieldName;
        String orderByField;
        switch (orderByEnumValue) {
            case ID:
                orderByFieldName = idField.getPropertyName();
                orderByField = idField.getPropertyData(exchange);
                break;
            case NAME:
                orderByFieldName = nameField.getPropertyName();
                orderByField = nameField.getPropertyData(exchange);
                break;
            case DESCRIPTION:
                orderByFieldName = descriptionField.getPropertyName();
                orderByField = descriptionField.getPropertyData(exchange);
                break;
            default:
                orderByFieldName = nameField.getPropertyName();
                orderByField = nameField.getPropertyData(exchange);
                break;
        }

        String origSearchString = searchString.getPropertyData(exchange);
        String likeSearchString = "%" + origSearchString + "%";
        whereClauseSQL.append("(");
        int fieldcnt = 0;
        for (String fieldToSearch : fieldsToSearch.getPropertyData(exchange)) {
            whereClauseSQL.append(fieldToSearch + " like :?searchfield" + String.valueOf(fieldcnt));
            whereClauseSQL.append(" or ");
            exchange.getIn().setHeader("searchfield" + String.valueOf(fieldcnt), likeSearchString);
            fieldcnt++;
        }
        whereClauseSQL.delete(whereClauseSQL.length() - 4, whereClauseSQL.length());
        whereClauseSQL.append(")");

        String filterFieldname = filterField.getPropertyData(exchange);
        String filterFieldMatchesValue = filterFieldMatches.getPropertyData(exchange);
        String filterFieldNotMatchesValue = filterFieldNotMatches.getPropertyData(exchange);
        String filterFieldInValue = filterFieldIn.getPropertyData(exchange);

        if (!StringUtils.isEmpty(filterFieldname) && !StringUtils.isEmpty(filterFieldMatchesValue)) {
            whereClauseSQL.append(" and (");
            whereClauseSQL.append(filterFieldname);
            if (filterFieldMatchesValue.equals("null")) {
                whereClauseSQL.append(" is ");
            } else {
                whereClauseSQL.append(" = ");
            }
            whereClauseSQL.append(filterFieldMatchesValue);
            whereClauseSQL.append(")");
        }

        if (!StringUtils.isEmpty(filterFieldname) && !StringUtils.isEmpty(filterFieldNotMatchesValue)) {
            whereClauseSQL.append(" and (");
            whereClauseSQL.append(filterFieldname);
            if (filterFieldNotMatchesValue.equals("null")) {
                whereClauseSQL.append(" is not ");
            } else {
                whereClauseSQL.append(" != ");
            }
            whereClauseSQL.append(filterFieldNotMatchesValue);
            whereClauseSQL.append(")");
        }

        if (!StringUtils.isEmpty(filterFieldname) && !StringUtils.isEmpty(filterFieldInValue)) {
            whereClauseSQL.append(" and (");
            whereClauseSQL.append(filterFieldname);
            whereClauseSQL.append(" in (");
            whereClauseSQL.append(filterFieldInValue);
            whereClauseSQL.append("))");
        }

        int requestedPageSize = pageSize.getPropertyData(exchange);
        int requestedPageNumber = pageNumber.getPropertyData(exchange);
        int requestedFirstRecord = (requestedPageSize * (requestedPageNumber - 1)) + 1;
        int requestedLastRecord = requestedFirstRecord + requestedPageSize - 1;

        String countQuery = "select COUNT(" + idField.getPropertyData(exchange) + ") as ResultCount from " + tableName.getPropertyData(exchange) + " where " + whereClauseSQL;
        countQueryProperty.setPropertyData(exchange, countQuery);

        String querySQL = "select "
                + idField.getPropertyName() + ", "
                + nameField.getPropertyName() + ", "
                + descriptionField.getPropertyName() + ", "
                + restIDField.getPropertyName()
                + " from "
                + "( select "
                + "ROW_NUMBER() OVER (ORDER BY " + orderByField + ") AS RowNum, "
                + idField.getPropertyData(exchange) + " as " + idField.getPropertyName() + ", "
                + nameField.getPropertyData(exchange) + " as " + nameField.getPropertyName() + ", "
                + descriptionField.getPropertyData(exchange) + " as " + descriptionField.getPropertyName() + ", "
                + restIDField.getPropertyData(exchange) + " as " + restIDField.getPropertyName()
                + " FROM "
                + tableName.getPropertyData(exchange)
                + " where "
                + whereClauseSQL.toString()
                + ") AS RowConstrainedResult"
                + " WHERE "
                + "RowNum >= " + requestedFirstRecord + " AND RowNum <= " + requestedLastRecord
                + " ORDER BY RowNum";

        logger.info("Query SQL: \n" + querySQL);
        resultQueryProperty.setPropertyData(exchange, querySQL);
    }

    protected void parseCountQueryResult(Exchange exchange) throws Exception {
        List<Map> queryResultsMap = exchange.getIn().getBody(List.class);
        exchange.getIn().removeHeader("CamelSqlRowCount");
        int totalResultCount;
        if (queryResultsMap != null && queryResultsMap.size() == 1) {
            for (Map row : queryResultsMap) {
                totalResultCount = Integer.parseInt(String.valueOf(row.get("ResultCount")));
                totalResults.setPropertyData(exchange, totalResultCount);
            }
        } else {
            throw new Exception("Results Count Query failed.");
        }
    }

}
