/**
 * <B>The routes package contains RouteBuilder and Activator objects</B>.
 * <p>
 * <ul>
 *  <li>RouteBuilder objects define camel routes used by this bundle</li>
 *  <li>Typically one route defined in a RouteBuilder object, though there can be more</li>
 *  <li>The Activator object (ProjectSpecificActivator) sets up an event listener to detect changing connect-core properties</li>
 * </ul> 
 */
package com.configureone.integration.connect.api.customers.routes;
