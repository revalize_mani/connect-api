/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.items.routes;

import com.configureone.integration.connect.core.camel.routes.definitions.groups.CoreConnectRoutes;
import com.configureone.integration.connect.core.properties.ConnectRoutesGroupActivator;
import com.configureone.integration.connect.core.properties.OSGIConnectRouteConfigurationManager;


/**
 *  Bundle activator used to register this bundle with karaf so when the core
 *  routes are updated, the bundle will be restarted with the new
 *  config values.
 */
public class ProjectSpecificActivator extends ConnectRoutesGroupActivator {

    /**
     * Default constructor used to register an event listener for the core
     * route configurations.
     */
    public ProjectSpecificActivator () {
        super(null, new OSGIConnectRouteConfigurationManager("com.configureone.integration.connect.items.routes"), CoreConnectRoutes.class);
    }
}
