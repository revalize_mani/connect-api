/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import java.io.UnsupportedEncodingException;

import com.configureone.ws.GenerateSSO;
import com.configureone.ws.GenerateSSOResponse;
import com.configureone.integration.connect.core.model.SSOTokenRequest;
import com.configureone.integration.connect.core.model.SSOToken;

import org.apache.camel.Body;

/**
 *
 * @author ravent
 */
public class GenerateSSOTransformer {


    public GenerateSSO generateSSORequest(@Body SSOTokenRequest jsonRequest) throws UnsupportedEncodingException {

        GenerateSSO request = new GenerateSSO();
        request.setUserId(jsonRequest.getUserId());
        request.setSecretKey(jsonRequest.getSecretKey());

        return request;
    }
    
    public String requestValidation(@Body SSOTokenRequest jsonRequest) throws UnsupportedEncodingException {
        String message = "Data passed";
        
        if(jsonRequest.getSecretKey() == null){
            message = "SecretKey key/value pair is missing";
        }
        if(jsonRequest.getUserId() == null){
            message = "UserId key/value pair is missing";
        }
        
        if(jsonRequest.getSecretKey() == null && jsonRequest.getUserId() == null){
            message = "UserId key/value and SecretKey key/value pair are missing";
        }
        return message;
    }

    public  SSOToken transform(@Body GenerateSSOResponse response) {
        String key = response.getGenerateSSOReturn();
        SSOToken ssoKey = new SSOToken();
        ssoKey.setSSOToken(key);

        return ssoKey;

    }  

    public SSOToken transformFailure() {
        String key = "invalidUser";
        SSOToken ssoKey = new SSOToken();
        ssoKey.setSSOToken(key);

        return ssoKey;
    }

}