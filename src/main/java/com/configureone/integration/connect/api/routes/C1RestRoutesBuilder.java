package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.model.Product;
import com.configureone.integration.connect.core.model.User;
import com.configureone.integration.connect.core.model.SSOToken;
import com.configureone.integration.connect.core.model.Quote;
import com.configureone.integration.connect.core.model.OAuthToken;
import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.model.SSOTokenRequest;
import com.configureone.integration.connect.core.model.serialization.GsonStrictEnumDeserializer;
import com.configureone.integration.connect.core.model.Configuration;
import com.configureone.integration.connect.core.model.Customer;
import com.configureone.integration.connect.core.model.CustomerList;
import com.configureone.integration.connect.core.model.QueryResult;
import com.configureone.integration.connect.core.model.Document;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.integration.connect.core.model.Contact;
import com.configureone.integration.connect.core.model.ItemList;
import com.configureone.integration.connect.core.model.BatchResponse;
import static com.configureone.integration.connect.api.constants.GeneratedConstants.API_VERSION;
import static com.configureone.integration.connect.core.camel.routes.AsyncCustomerQueuePublisherRouteBuilder.orgId;
import static com.configureone.integration.connect.core.camel.routes.AsyncCustomerQueuePublisherRouteBuilder.reference;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.entityTypeHeaderName;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.searchStringHeaderName;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.orderByHeaderName;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.pageSizeHeaderName;
import static com.configureone.integration.connect.api.search.routes.SearchRouteBuilder.pageNumberHeaderName;
import com.configureone.integration.connect.core.camel.routes.definitions.EventAPIPublishEventRoute;
import com.configureone.integration.connect.core.camel.routes.definitions.GetSingleDocumentInternalRoute;
import com.configureone.integration.connect.core.model.AdvancedSearch;
import com.configureone.integration.connect.core.model.Order;
import com.configureone.integration.connect.core.model.OrderStatusRequest;
import com.configureone.integration.connect.core.model.OrderStatusResponse;
import com.configureone.integration.connect.core.model.ProcessItemPrice;
import com.configureone.integration.connect.core.model.ProcessItemPriceResponse;
import com.configureone.integration.connect.core.model.QuoteStatusRequest;
import com.configureone.integration.connect.core.model.StatusResponse;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;


import java.util.Optional;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author rmoutray
 */
public class C1RestRoutesBuilder extends RouteBuilder {

    /**
     * Logger for the class.
     */
    protected Logger logger = LogManager.getLogger(getClass());

    public static final String GET_CUSTOMER_BY_REFNUM_URI = "direct-vm:getCustomerByRefNum";
    public static final String PROCESS_CUSTOMER_URI = "direct-vm:processCustomer";
    public static final String UPDATE_CUSTOMER_URI = "direct-vm:updateCustomer";
    public static final String UPDATE_CONTACT_URI = "direct-vm:updateContact";
    public static final String PROCESS_ITEM_URI = "direct-vm:processItem";
    public static final String GET_ITEM_URI = "direct-vm:getItem";
    public static final String ASYNC_CUSTOMER_QUEUE_URI = "direct-vm:processCustomerAsync";

    // GetUsersRouteBuilder userTest = new GetUsersRouteBuilder();
    
    // public static final String direct_vm_getUsers SF_DOCUMENT_REQUEST_ROUTE = new GetUsersRouteBuilder(GROUP);


    /**
     * Connect-core shared properties.
     */
    @BeanInject("connectCoreSharedProperties")
    public ConnectCorePropertiesService connectCoreProperties;

    /**
     * Configuration of the Configure One Rest Routes.
     * @throws Exception DESCRIPTION
     */
    @Override
    public void configure() throws Exception {
        onException(ConnectBusinessException.class)
            .handled(true)
            .log(LoggingLevel.TRACE, "TODO: Remove this log line") //Log here enables the successive processor to actually work.
            .process(this::processBusinessExceptions)
            .process(new OnExceptionProcessor());
        onException(JsonParseException.class, JsonSyntaxException.class, MalformedJsonException.class)
            .handled(true)
            .log(LoggingLevel.TRACE, "TODO: Remove this log line") //Log here enables the successive processor to actually work.
            .process(this::processJsonExceptions)
            .process(new OnExceptionProcessor());
        onException(Exception.class)
            .handled(true)
            .log(LoggingLevel.TRACE, "TODO: Remove this log line") //Log here enables the successive processor to actually work.
            .process(this::processExceptions)
            .process(new OnExceptionProcessor());

        restConfiguration()
                .component("jetty")
                .port("{{c1.rest.port}}") // Defined in blueprint properties
                .contextPath("c1")
                .endpointProperty("handlers", "accessLog,securityHandler")  // Defined in blueprint.xml
                .bindingMode(RestBindingMode.auto)
                .jsonDataFormat("json-gson") // Allows output types, e.g., Quote to be serialized to JSON
                //.jsonDataFormat("json-jackson")
                .dataFormatProperty("prettyPrint", "true")
                .apiContextPath("/api-docs")
                .apiProperty("api.title", "Connect api REST API (connect-api)")
                .apiProperty("api.version", API_VERSION)
                .apiProperty("api.description", "Swagger documentation for the Configure One Connect API Gateway")
                .apiProperty("api.contact.name", "The Configure One Integration Dev Team")
                .apiContextListing(true)
                .enableCORS(true)
                .corsAllowCredentials(true)
                .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization")
                .setApiContextRouteId("apiDocs");

        ping();
        getUsers(User.class);
        getProducts(Product.class);
        token(SSOToken.class);
        quoteRoutes(Quote.class);
        configurationRoutes(Configuration.class);
        customerRoutes(Customer.class, CustomerList.class, BatchResponse.class, Contact.class);
        myCustomerRoutes();
        documentRoutes(Document.class);
        search();
        events();
        itemPost();
        itemGet();
        authToken(OAuthToken.class);
        authCode();
        itemPrice(ProcessItemPrice.class);
        quoteStatus(QuoteStatusRequest.class);
        orderRoutes(Order.class);
        updateOrder(OrderStatusRequest.class);
        // userTest.configure();
    }

    public void myCustomerRoutes(){
        rest("/rest/user/{userid}/myCustomers")
            .produces("application/json")
            .bindingMode(RestBindingMode.json)
            .get()
                .param()
                    .name("userid")
                    .description("The ID of the user whose 'My Customer records to get'")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("getMyCustomers")
                    .to("direct-vm:getMyCustomers");
    }

    /**
     * Method used to process items
     */
    public void itemPost(){
        rest("/rest/items")
            .bindingMode(RestBindingMode.off)
            .post()
                .route().routeId("postItem")
                    .to(PROCESS_ITEM_URI)
                    .getRestDefinition()

            .post("/batch").tag("items").id("postItemsListEndpoint").description("Create a list of items")
                .consumes("application/json")
                .type(ItemList.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(ItemList.class)
                .route().routeId("postItemList")
                    .log("JJJAPI HERE1")
                    .to("direct-vm:processItemList")
                .getRestDefinition();
	}

    public void itemGet(){
        rest("/rest/items")
                        .bindingMode(RestBindingMode.off)
            .get("/{itemRefNum}")
                        .param()
                                .name("itemRefNum")
                                .description("The item reference number")
                                .type(RestParamType.path)
                        .endParam()
                .to(GET_ITEM_URI);
    }

    /**
     * Method to process business logic exceptions.
     * @param exchange The exchange to process.
     */
    protected void processBusinessExceptions(Exchange exchange) {
        ConnectBusinessException cbe = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, ConnectBusinessException.class);
        RestErrorResponse rer = new RestErrorResponse(cbe);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }

    /**
     * Method to process client json exceptions.
     * @param exchange The exchange to process.
     */
    protected void processJsonExceptions(Exchange exchange) {
        Exception e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
        RestErrorResponse rer = new RestErrorResponse(400, e);
        logger.error(rer);
        rer.setOnExchange(exchange);
    }

    /**
     * Method to process all other exceptions.
     * @param exchange The exchange to process.
     */
    protected void processExceptions(Exchange exchange) {
        Exception e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
        RestErrorResponse rer = new RestErrorResponse(500, e);
        logger.error(rer);
        rer.setOnExchange(exchange);
        logger.error("REST ERROR PROCESSED: " + exchange.getIn().getBody(String.class));
    }

    /**
     * Route to tell if Configure One Rest is up or not.
     */
    public void ping() {
        rest("/echo").id("echo").tag("echo")
                .consumes("application/text")
                .produces("application/text")
                .description("Echo rest service")
                .get("/ping").tag("echo")
                    .outType(String.class)
                    .description("A ping service")
                    .route().routeId("echo")
                        .transform()
                        .constant("pong");
    }

    /**
     * GET request Route to get Configure One users.
     * Usage: Send an HTTP GET request to the configured URL.
     * Responds with: ??? Currently dynamic json array of users based on Configure One db.
     * @param userClass DESCRIPTION
     */
    public void getUsers(Class<? extends User> userClass) {
	rest("/rest/users").id("users").tag("users")
	    .produces("application/json")
	    .get().id("getUsers").tag("users").description("Get all users")
            .bindingMode(RestBindingMode.json)
            .outType(userClass)
	    .route().routeId("getUsers")
	    .to("direct-vm:getUsers");
    }


    /**
     * GET request Route to get Configure One products.
     * Usage: Send an HTTP GET request to the configured URL.
     * Responds with: Dynamic json array of products based on Configure One db
     * @param productClass DESCRIPTION
     */
    public void getProducts(Class<? extends Product> productClass) {
        rest("/rest/products").tag("products")
            .produces("application/json")
                .get().id("getProducts").tag("products").description("Get all products")
                    .bindingMode(RestBindingMode.json)
                    .outType(productClass)
                    .route().routeId("getProducts")
                        .to("direct-vm:getProducts")
                .getRestDefinition()
                    .get("/{id}").id("getProductById").tag("products").description("Get product by ID")
                        .bindingMode(RestBindingMode.json)
                        .outType(productClass)
                        .param()
                            .name("id")
                            .description("The ID of the requested product")
                            .type(RestParamType.path)
                            .endParam()
                        .route().routeId("getProductById")
                            .to("direct-vm:getProductById");
        rest("/rest/productPermissions").tag("productPermissions")
            .post().id("getProductPermissions").tag("products").description("Get the permissions of the specified products for the specified user group")
                .bindingMode(RestBindingMode.off)
                .consumes("application/json")
                .produces("application/json")
                .route().routeId("getProductPermissionsRoute")
                    .streamCaching()
                    .to("direct-vm:getProductPermissions");
    }


    public void token (Class<? extends SSOToken> ssoTokenClass) {
        rest("/rest/token").id("token").tag("token").description("Generate SSO token")
                .post().tag("token").description("Generate single sign-on token")
                .consumes("application/json")
                .bindingMode(RestBindingMode.json)
                .type(SSOTokenRequest.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(ssoTokenClass)
                .route().routeId("generateSSO")
                .to("direct-vm:generateSSO");
    }

    public void itemPrice (Class<? extends ProcessItemPrice> processItemPriceClass) {
        rest("/rest/itemPrice").id("itemPrice").tag("itemPrice")
                .post().id("itemPrice").tag("itemPrice").description("update Price List Item (processItemPrice)")
                .consumes("application/json")
                .bindingMode(RestBindingMode.json)
                .type(ProcessItemPrice.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(ProcessItemPriceResponse.class)
                .route().routeId("ProcessItemPriceRoute")
                .to("direct-vm:ProcessItemPriceRoute");
    }

    public void configurationRoutes(Class<? extends Configuration> configurationClass) {
        rest("/rest/configurations").id("configurations").tag("configurations")
                .get("/{designId}").tag("configurations").description("Get configuration by Design ID")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(configurationClass)
                .param()
                .name("designId")
                .description("The Design ID of the requested configuration")
                .type(RestParamType.path)
                .endParam()
                .route().routeId("getConfigurationByDesignId")
                .to("direct-vm:getConfiguration");
    }

    public void search() {
        rest("/search").id("search)").tag("search")
            .get("/{" + entityTypeHeaderName +"}").tag("search").description("The Configure One Connect Search Service API")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(QueryResult.class)
                .param()
                    .name("recordType")
                    .description("The Record Type for which to search")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The search string to search for data by")
                    .dataType("string")
                    .name(searchStringHeaderName)
                    .required(true)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The field to order data by (ID, NAME, or DESCRIPTION")
                    .dataType("string")
                    .name(orderByHeaderName)
                    .required(false)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The number of results to return per page")
                    .dataType("string")
                    .name(pageSizeHeaderName)
                    .required(false)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The number of results to return per page")
                    .dataType("string")
                    .name(pageNumberHeaderName)
                    .required(false)
                .endParam()
                .route().routeId("searchByRecordType")
                    .to("direct-vm:searchByRecordType")
                .getRestDefinition()

                 .post("/{" + entityTypeHeaderName +"}").id("postSearch").description("advanced search")
                .consumes("application/json")
                .bindingMode(RestBindingMode.json)
                .type(AdvancedSearch.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(QueryResult.class)
                .param()
                    .name("recordType")
                    .description("The Record Type for which to search")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The search string to search for data by")
                    .dataType("string")
                    .name(searchStringHeaderName)
                    .required(true)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The field to order data by (ID, NAME, or DESCRIPTION")
                    .dataType("string")
                    .name(orderByHeaderName)
                    .required(false)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The number of results to return per page")
                    .dataType("string")
                    .name(pageSizeHeaderName)
                    .required(false)
                .endParam()
                .param()
                    .type(RestParamType.query)
                    .description("The number of results to return per page")
                    .dataType("string")
                    .name(pageNumberHeaderName)
                    .required(false)
                .endParam()
                .route().routeId("AdvancedSearch")
                    .to("direct-vm:AdvancedSearch");
    }

    public void events() {
        rest("/events").id("events").tag("events")
            .post("/{" + EventAPIPublishEventRoute.EVENT_TYPE.getHeaderName() + "}").description("The Configure One Connect Event Bus API")
                .bindingMode(RestBindingMode.json)
                .consumes("application/json")
                .param()
                    .name(EventAPIPublishEventRoute.EVENT_TYPE.getHeaderName())
                    .description("The event name for the event getting fired.")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .name("body")
                    .type(RestParamType.body)
                    .description("The event data.")
                .endParam()
            .route().routeId("eventBusRoute")
                .to("direct-vm:eventBusRoute");
    }

    /**
     * Rest API for Configure One quote service.
     * @param quoteClass DESCRIPTION
     */
    public void quoteRoutes(Class<? extends Quote> quoteClass) {
        rest("/rest/quotes").tag("quotes")
            .get("/{sn}").id("getQuoteBySerialNumber").description("Get quote by serial number")
            .produces("application/json")
            .bindingMode(RestBindingMode.json)
            .outType(quoteClass)
            .param()
                .name("sn")
                .description("The Serial Number of the requested quote")
                .type(RestParamType.path)
                .endParam()
            .route().routeId("getQuoteBySerialNumber")
                .to("direct-vm:getQuoteBySerialNumber")
            .getRestDefinition()

            .post().id("postQuote").description("Create/Update a Configure One quote")
                .consumes("application/json")
                .bindingMode(RestBindingMode.off)  // This ensures the quotes in the JSON sent to us are not removed.
                .type(JsonElement.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(quoteClass)
                .route().routeId("postQuote")
                    .process(this::parseQuoteJson)
                    .to("direct-vm:processQuote");
    }


            /**
     * POST request Route to get Configure One update Quote Statue.
     * Usage: Send an HTTP POST request to the configured URL.
     * Responds with: response class
     * @param quoteStatusRequestClass
     */
    public void quoteStatus(Class<? extends QuoteStatusRequest> quoteStatusRequestClass) {
	rest("/rest/quoteStatus")
	    .post().id("quoteStatus").tag("quoteStatus").description("Update the Configure One Quote Status")
            .consumes("application/json")
            .bindingMode(RestBindingMode.json)
            .type(QuoteStatusRequest.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(StatusResponse.class)
                .route().routeId("updateQuoteStatusRest")
                .to("direct-vm:updateQuoteStatus");
    }


     /**
     * Rest API for Configure One order service.
     * @param orderClass DESCRIPTION
     */
    public void orderRoutes(Class<? extends Order> orderClass) {
        rest("/rest/orders").tag("orders")
            .get("/{on}").id("getOrderByOrderNumber").description("Get Configure One Order by order number")
            .produces("application/json")
            .bindingMode(RestBindingMode.json)
            .outType(orderClass)
            .param()
                .name("on")
                .description("The Order Number of the requested order")
                .type(RestParamType.path)
                .endParam()
            .route().routeId("getOrderByOrderNumber")
                .to("direct-vm:getOrderByOrderNumber")
            .getRestDefinition();
    }

    /**
     * POST request Route to get Configure One update Order service.
     * Usage: Send an HTTP POST request to the configured URL.
     * Responds with: response class
     * @param orderStatusRequestClass
     */
    public void updateOrder(Class<? extends OrderStatusRequest> orderStatusRequestClass) {
        rest("/rest/orderStatus")
                .post().id("orderStatus").tag("orderStatus").description("Update Status of Configure one Order")
                    .consumes("application/json")
                    .bindingMode(RestBindingMode.json)
                    .type(OrderStatusRequest.class)
                    .produces("application/json")
                    .bindingMode(RestBindingMode.json)
                    .outType(OrderStatusResponse.class)
                    .route().routeId("updateOrderStatusRest")
                    .to("direct-vm:updateOrderStatus");
    }


    /**
     * Rest API for Configure One customers service.
     * @param customerClass DESCRIPTION
     * @param contactClass DESCRIPTION
     */
    public void customerRoutes(Class<? extends Customer> customerClass, Class<? extends CustomerList> customerListClass, Class<? extends BatchResponse> BatchResponseClass, Class<? extends Contact> contactClass) {
        rest("/rest/externalCustomerAsync").tag("customersAsyncEndpoint")
            .post().id("customerAsyncRest").tag("customerAsyncRest").description("post customer to AMQP Queue (Async processCustomer)")
                .consumes("application/json")
                    .bindingMode(RestBindingMode.off)  // This ensures any quotes in the JSON sent to us are not removed.
                    .type(JsonElement.class).description("A JSON representation of the Customer schema")
                    .bindingMode(RestBindingMode.json)
                .produces("application/json")
                    .bindingMode(RestBindingMode.json)
                    .outType(StatusResponse.class)
                .param()
                    .name(orgId)
                    .description("The Orig Id to use in lookup / the Customer Master Source")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .name(reference)
                    .description("The referance number Type to use in lookup / the Customer Master Source (erp / crm)")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("callAsyncCustomerQueue")
                    .process(this::parseCustomerJson)
                    .to(ASYNC_CUSTOMER_QUEUE_URI)
                    .process(this::setBodyFromstatus)
                    .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                    //.setHeader(Exchange.HTTP_RESPONSE_TEXT, simple("${body}"))
                ;


        rest("/rest/customers").tag("customers")
            .get("/{custRefNum}").id("getCustomerByRefNum").tag("customers").description("Get customer by reference number")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(customerClass)
                .param()
                    .name("custRefNum")
                    .description("The reference number of the requested customer")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("getCustomerByRefNum")
                    .to(GET_CUSTOMER_BY_REFNUM_URI)
                    .log("Received Body.")
                    .process(this::setBodyFromOptional)
                    .log("Optional Processed.")
                .getRestDefinition()

            .get("/{custRefNum}/contacts/{contactRefNum}").id("getContactByAddressRefNum").tag("contacts").description("Get customer contact by customer reference number and address reference number")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(contactClass)
                .param()
                    .name("custRefNum")
                    .description("The reference number of the requested customer")
                    .type(RestParamType.path)
                    .endParam()
                .param()
                    .name("contactRefNum")
                    .description("The address ID of the requested contact")
                    .type(RestParamType.path)
                    .endParam()
                .route().routeId("getContactByAddressRefNum")
                    .to("direct-vm:getContactByAddressRefNum")
                    .log("Received Body.")
                    .process(this::setBodyFromOptional)
                    .log("Optional Processed.")
                .getRestDefinition()

            .post().tag("customers").id("postCustomer").tag("customers").description("Create a customer")
                .consumes("application/json")
                .produces("application/json")

                .bindingMode(RestBindingMode.json)

                .bindingMode(RestBindingMode.off)  // This ensures the quotes in the JSON sent to us are not removed.
                .type(JsonElement.class).description("A JSON representation of the Customer schema")
                .bindingMode(RestBindingMode.json)

                .outType(customerClass)

                .route().routeId("postCustomer")
                    .process(this::parseCustomerJson)
                    .to(PROCESS_CUSTOMER_URI)
                    .process(this::setBodyFromOptional)
                .getRestDefinition()

            .patch("/{custRefNum}").id("updateCustomer").tag("customers").description("Update a customer")
                .consumes("application/json")
                .produces("application/json")
                .bindingMode(RestBindingMode.off)  // This ensures the quotes in the JSON sent to us are not removed.
                .type(JsonElement.class).description("A JSON representation of the Customer schema")
                .bindingMode(RestBindingMode.json)
                .outType(customerClass)
                .param()
                    .name("custRefNum")
                    .description("The reference number of the requested customer")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("updateCustomer")
                    .process(this::parseCustomerJson)
                    .to(UPDATE_CUSTOMER_URI)
                    .process(this::setBodyFromOptional)
                .getRestDefinition()

            .post("/batch").tag("customers").id("postCustomerListEndpoint").description("Create a list of customers")
                .consumes("application/json")
                .type(CustomerList.class)
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(customerClass)
                .route().routeId("postCustomerList")
                    .to("direct-vm:processCustomerList")
                .getRestDefinition()

            //
            // Contact integration points.
            //

            .post("/{custRefNum}/contacts").id("postCustomerContact").tag("customers").description("Create/Update a customer contact")
                .consumes("application/json")
                .produces("application/json")

                .bindingMode(RestBindingMode.off)  // This ensures the quotes in the JSON sent to us are not removed.
                .type(JsonElement.class).description("A JSON representation of the Contact schema")
                .bindingMode(RestBindingMode.json)

                .outType(contactClass)

                .param()
                    .name("custRefNum")
                    .description("The reference number of the requested customer")
                    .type(RestParamType.path)
                    .endParam()
                .route().routeId("postCustomerContact")
                    .process(this::parseContactJson)
                    .to("direct-vm:processContact")
                    .process(this::setBodyFromOptional)
                .getRestDefinition()

            .patch("/{custRefNum}/contacts/{contactRefNum}").id("updateCustomerContact").description("Update a customer contact")
                .consumes("application/json")
                .produces("application/json")

                //.type(contactClass)
                //.bindingMode(RestBindingMode.json)

                .bindingMode(RestBindingMode.off)  // This ensures the quotes in the JSON sent to us are not removed.
                .type(JsonElement.class).description("A JSON representation of the Contact schema")
                .bindingMode(RestBindingMode.json)

                .outType(contactClass)

                .param()
                    .name("custRefNum")
                    .description("The reference number of the requested customer")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .name("contactRefNum")
                    .description("The reference number of the requested customer contact")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("patchCustomerContact")
                    .process(this::parseContactJson)
                    .to(UPDATE_CONTACT_URI)
                    .process(this::setBodyFromOptional)
                .getRestDefinition();
    }

    /**
     * REST API form Configure One document service
     *
     * @param body type class
     */
    public void documentRoutes (Class<? extends Document> body) {
        rest("/rest/document").tag("document")
            .get("/{document_type}/{document_id}/{document_file_name}").id("getDocumentByFileName").tag("document").description("Get a document for specific entity by file name.")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .outType(body)
                .param()
                    .name("document_type")
                    .description("The type of entity that the document resides on.  Can be one of Quote, Design, or Order.")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .name("document_id")
                    .description("The ID of the entity that the document resides on.  FOr example, if document type is Quote, then the document ID would be the quote serial number.")
                    .type(RestParamType.path)
                .endParam()
                .param()
                    .name("document_file_name")
                    .description("The file name of the document to be retrieved.")
                    .type(RestParamType.path)
                .endParam()
                .route().routeId("getDocumentByFileName")
                    .process(this::setDocumentProperties)
                    .to("direct-vm:getSingleDocumentInternal")
                    .log("Received Body")
                    .process(this::setBodyAsDocument)
                    .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                    .log("After putting a document in the body.")
                .getRestDefinition();
    }

    /**
     * REST API form Configure One document service
     *
     * @param authTokenClass type class
     */
    public void authToken(Class<? extends OAuthToken> authTokenClass) {
        rest("/rest/authToken").id("authToken").tag("authToken")
            .consumes("application/json")
            .post().tag("authToken").description("Endpoint for storing OAuth Tokens")
                .produces("text/plain")
                .type(JsonElement.class)
                .route().routeId("Store OAuth Tokens")
                    .streamCaching()
                    .to("direct-vm:OAuthTokenRoute");
    }

    /**
     * REST API for Configure One document service
     *
     *
     */
    public void authCode() {
        rest("/rest/authCode").id("authCode").tag("authCode")
            .get().tag("authCode").description("Endpoint as callback URL for receiving OAuth authorization code")
                .produces("text/plain")
                .type(JsonElement.class)
                .route().routeId("receiveOAuthCode")
                    .streamCaching()
                    .to("direct-vm:OAuthCodeRoute");
    }

    protected void setDocumentProperties (Exchange exchange) {
        String doctype = exchange.getIn().getHeader("document_type", String.class);
        String docid = exchange.getIn().getHeader("document_id", String.class);
        String docfilename = exchange.getIn().getHeader("document_file_name", String.class);

        if (doctype != null && !doctype.trim().isEmpty() && docid != null && !docid.trim().isEmpty() && docfilename != null && !docfilename.trim().isEmpty()) {
            exchange.setProperty(GetSingleDocumentInternalRoute.DOCUMENT_TYPE.getPropertyName(), doctype);
            exchange.setProperty(GetSingleDocumentInternalRoute.DOCUMENT_FILENAME.getPropertyName(), docfilename);
            exchange.setProperty("document_serial_num", docid);
            exchange.getIn().setBody(docid);
        }
        else {
            RestErrorResponse rest400error = new RestErrorResponse(400, "Invalid request.");
            rest400error.setOnExchange(exchange);
        }
    }

    protected void setBodyAsDocument(Exchange exchange) {
        Document newbody = new Document(GetSingleDocumentInternalRoute.DOCUMENT_TYPE.getPropertyData(exchange), exchange.getProperty("document_serial_num",String.class), GetSingleDocumentInternalRoute.DOCUMENT_FILENAME.getPropertyData(exchange), exchange.getIn().getBody(String.class));
        //String newbody = "{\"type\": \"" + GetSingleDocumentInternalRoute.DOCUMENT_TYPE.getPropertyData(exchange) + "\", \"serialNumber\": \"" + exchange.getProperty("document_id",String.class) + "\",";
        //newbody += "\"files\": [ { \"fileName\": \"" + GetSingleDocumentInternalRoute.DOCUMENT_FILENAME.getPropertyData(exchange) + "\", \"content\": \"" + exchange.getIn().getBody(String.class) + "\"";
        //newbody += " } ] }";
        exchange.getIn().setBody(newbody);
    }

    protected void setBodyFromstatus(Exchange exchange) {
        StatusResponse resp = exchange.getIn().getBody(StatusResponse.class);
        Integer code = 400;

        if(resp.getSuccess().equals("true")){
            code = 200;
        }

        //exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE,code);
        logger.info("setBodyFromstatus log - " + resp.toString());
        exchange.getIn().setBody(resp);
    }


    protected void setBodyFromOptional(Exchange exchange) {
        logger.info("setBodyFromOptional log");
        Optional optional = exchange.getIn().getBody(Optional.class);
        if (optional != null && optional.isPresent()) {
            exchange.getIn().setBody(optional.get());
            exchange.getIn().removeHeader(Exchange.CONTENT_TYPE);
        }

        else if (optional != null && !optional.isPresent()) {
            RestErrorResponse rest404Error = new RestErrorResponse(404, "Entity Not Found.");
            rest404Error.setOnExchange(exchange);
        }
        else {
            RestErrorResponse rest500Error = new RestErrorResponse(500, "Invalid response from endpoint. " + exchange.getIn().getBody(String.class));
            rest500Error.setOnExchange(exchange);
        }
    }

    /**
     * Processor to parse the incoming Json into a Quote object and store it back on the exchange's message body.
     * @param exchange The exchange to process.
     */
    protected void parseQuoteJson(Exchange exchange) {
        //Creating a type adapter for the Gson json parser so that invalid Enums will throw exceptions.
        GsonStrictEnumDeserializer<Quote.ReferenceType> gsed = new GsonStrictEnumDeserializer<>(Quote.ReferenceType.class);
        Gson gson = new GsonBuilder().registerTypeAdapter(Quote.ReferenceType.class, gsed).create();
        Quote quote = gson.fromJson(exchange.getIn().getBody(JsonElement.class), Quote.class);
        exchange.getIn().setBody(quote, Quote.class);
    }

    /**
     * Processor to parse the incoming Json into a Contact object and store it back on the exchange's message body.
     * @param exchange The exchange to process.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException DESCRIPTION
     */
    protected void parseContactJson(Exchange exchange) throws ConnectBusinessException {
        //Creating a type adapter for the Gson json parser so that invalid Enums will throw exceptions.
        GsonStrictEnumDeserializer<Contact.TypeCode> gsed = new GsonStrictEnumDeserializer<>(Contact.TypeCode.class);
        Gson gson = new GsonBuilder().registerTypeAdapter(Contact.TypeCode.class, gsed).create();
        Contact contact = gson.fromJson(exchange.getIn().getBody(JsonElement.class), Contact.class);
        if (contact == null) {
            throw new ConnectBusinessException("Entity Not Provided.");
        }
        exchange.getIn().setBody(contact, Contact.class);
    }

    /**
     * Processor to parse the incoming Json into a Customer object and store it back on the exchange's message body.
     * @param exchange The exchange to process.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException DESCRIPTION
     */
    protected void parseCustomerJson(Exchange exchange) throws ConnectBusinessException {
        //Creating a type adapter for the Gson json parser so that invalid Enums will throw exceptions.
        logger.info("parseCustomerJson log");
        GsonStrictEnumDeserializer<Contact.TypeCode> gsed = new GsonStrictEnumDeserializer<>(Contact.TypeCode.class);
        Gson gson = new GsonBuilder().registerTypeAdapter(Contact.TypeCode.class, gsed).create();
        if (exchange.getIn().getBody(JsonElement.class).isJsonObject()) {
            Customer customer = gson.fromJson(exchange.getIn().getBody(JsonElement.class), Customer.class);
            if (customer == null) {
                throw new ConnectBusinessException("Entity Not Provided.");
            }
            exchange.getIn().setBody(customer, Customer.class);
        }
        //TODO REMOVE BELOW
        else {
            Customer[] customer = gson.fromJson(exchange.getIn().getBody(JsonElement.class), Customer[].class);
            if (customer == null) {
                throw new ConnectBusinessException("Entity Not Provided.");
            }
            exchange.getIn().setBody(customer, Customer[].class);
        }
    }
}
