/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.routes.groups;


import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectExchangeProperty;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRouteDefinition;
import com.configureone.integration.connect.core.camel.routes.definitions.model.ConnectRoutesGroupConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Configuration of routes for the mapping of a quote to an opportunity (retrieving SF data to pre-populate 
 * quotes being created. The route to generate a quote request from a Salesforce Opportunity ID.
 */
public class GetUsersRoute extends ConnectRouteDefinition {
    
    private Logger logger = LogManager.getLogger(this.getClass());
        
    /**
     * Instantiates an instance of the class.
     * @param registrationGroup
     */
    public GetUsersRoute(ConnectRoutesGroupConfiguration registrationGroup) {
        super("direct-vm:getUsers", Object.class, Object.class, registrationGroup);
    }   

}
