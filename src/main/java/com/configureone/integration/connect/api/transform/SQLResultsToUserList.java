/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.api.transform.UserMapper;
import com.configureone.integration.connect.core.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Handler;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author rolson
 */
public class SQLResultsToUserList {

    @Handler
    public List<User> transform(List<Map> body) throws Exception {
        List<User> userlist = new ArrayList<>();
        if (!CollectionUtils.isEmpty(body)) {
            body.forEach((row) -> {
                userlist.add(UserMapper.mapRowToUser(row));
            });
        }
        return userlist;
    }
}