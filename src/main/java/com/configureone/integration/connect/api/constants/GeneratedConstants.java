package com.configureone.integration.connect.api.constants;

public class GeneratedConstants {

    // This string is updated via the com.google.code.maven-replacer-plugin
    // in pom.xml during the generate-sources phase.

    public static final String API_VERSION = "0.1.4-SNAPSHOT";


}

