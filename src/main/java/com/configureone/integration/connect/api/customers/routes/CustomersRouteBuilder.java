/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.customers.routes;

import com.configureone.integration.connect.core.camel.processors.OnExceptionProcessor;
import com.configureone.integration.connect.core.camel.routes.builders.helpers.C1SOAPEntityRepositoryRouteBuilder;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Contact;
import com.configureone.integration.connect.core.model.Customer;
import com.configureone.integration.connect.core.model.CustomerList;
import com.configureone.integration.connect.core.model.BatchResponse;
import com.configureone.integration.connect.core.model.BatchProcessingStrategy;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.integration.connect.core.services.ConnectRoute;
import com.configureone.ws.GetCustomer;
import com.configureone.ws.GetCustomerResponse;
import com.configureone.ws.ObjectFactory;
import com.configureone.ws.ProcessCustomer;
import com.configureone.ws.ProcessCustomerResponse;
import com.configureone.ws.Response;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.camel.component.gson.GsonDataFormat;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;


/**
 * Hello world! route provided with the archetype.  This is used as a
 * placeholder for the routes package.  It is also used in some example unit
 * tests.
 */
public class CustomersRouteBuilder extends RouteBuilder {

    /**
     * Logger for the class.
     */
    protected final Logger logger = LogManager.getLogger(getClass());
    
    /**
     * Connect-core shared properties.
     */
    @BeanInject("connectCoreSharedProperties")
    public ConnectCorePropertiesService connectCoreProperties;
    
    /**
     * The header name to use to retrieve a customer from Configure One.
     */
    public static final String CUST_REF_NUM_EXCHANGE_HEADER = "custRefNum";
    /**
     * The ConnectRoute used to retrieve a customer from Configure One.
     */
    public static final ConnectRoute GET_CUSTOMER_ROUTE = new ConnectRoute("direct-vm:getCustomerByRefNum", "direct-vm:getCustomerByRefNum", "getCustomerByRefNumDirectVM");
    /**
     * The ConnectRoute used to add a customer to Configure One.
     */
    public static final ConnectRoute ADD_CUSTOMER_ROUTE = new ConnectRoute("direct-vm:processCustomer", "direct-vm:processCustomer", "processCustomerDirectVM");
    /**
     * The ConnectRoute used to update an existing customer in Configure One. This route first checks if a customer 
     * exists, before calling the internal route to process the update.
     */
    public static final ConnectRoute UPDATE_CUSTOMER_ROUTE = new ConnectRoute("direct-vm:updateCustomer", "direct-vm:updateCustomer", "updateCustomerDirectVM");
    /**
     * The route that merely performs the process customer action for an update, without checking first if the 
     * customer actually exists. If the customer does not exist, it will be created.
     */
    public static final ConnectRoute PROCESS_UPDATE_CUSTOMER_ROUTE_INTERNAL = new ConnectRoute("direct-vm:processCustomerUpdateInternal", "direct-vm:processCustomerUpdateInternal", "processCustomerUpdateInternalDirect");

    /**
     * Configuration of the connect-customers routes.
     * @throws  Exception   Any exceptions during execution will be thrown.
     */
    @Override
    public void configure() throws Exception {
        errorHandler(noErrorHandler());
        onException(Exception.class)
            .process(new OnExceptionProcessor());
                
        QName getCustRequestQName = new QName("http://ws.configureone.com", "getCustomer");
        QName getCustResponseQName = new QName("http://ws.configureone.com", "getCustomerResponse");
        QName addCustRequestQName = new QName("http://ws.configureone.com", "processCustomer");
        QName addCustResponseQName = new QName("http://ws.configureone.com", "processCustomerResponse");
        
        C1SOAPEntityRepositoryRouteBuilder c1SOAPEntityRepositoryRouteBuilder = new C1SOAPEntityRepositoryRouteBuilder(this);
        
        //Use the C1SOAPEntityRepositoryRouteBuilder helper to build an add customer route
        c1SOAPEntityRepositoryRouteBuilder.addC1SOAPEntityGetByIDOnHeader(ADD_CUSTOMER_ROUTE, GET_CUSTOMER_ROUTE, 
                CUST_REF_NUM_EXCHANGE_HEADER, Customer.class, ProcessCustomer.class, ProcessCustomerResponse.class, 
                addCustRequestQName, addCustResponseQName, this::createProcessCustomerSOAPRequest, 
                this::getIDFromProcessCustomerResponse);
        
        //Use the C1SOAPEntityRepositoryRouteBuilder helper to build a get customer route
        c1SOAPEntityRepositoryRouteBuilder.getC1SOAPEntityByID(GET_CUSTOMER_ROUTE, Customer.class, GetCustomer.class, 
                GetCustomerResponse.class, getCustRequestQName, getCustResponseQName, 
                CustomersRouteBuilder::generateGetCustomerSOAPObjFromHeaderRefNum, this::getCustomerFromSoapResponse);
        
        from(UPDATE_CUSTOMER_ROUTE.getDefaultURI()).routeId(UPDATE_CUSTOMER_ROUTE.getRouteID())
            .enrich("direct-vm:verifyCustomerExists", (original, resource) -> original)
            .log(LoggingLevel.INFO, "Customer Verified to Exist.")
            .to(PROCESS_UPDATE_CUSTOMER_ROUTE_INTERNAL.getConfiguredURI())
            .log(LoggingLevel.INFO, "Customer Updated.")
            .end();
        
        from("direct-vm:verifyCustomerExists").routeId("verifyCustomerExistsDirect")
            .to(GET_CUSTOMER_ROUTE.getConfiguredURI())
            .process(this::verifyCustomerResultExists);

        GsonDataFormat gdf = new GsonDataFormat();
        gdf.setUnmarshalType(Optional.class);
        CustomerList custResp = new CustomerList();
        from("direct-vm:processCustomerList").routeId("processCustomerListDirect")
            .streamCaching()
            // Splits body message into individual customers using getCustomers method from CustomerList class (from C1RestRoutesBuilder)
            .split(body().method("getCustomers"), new BatchProcessingStrategy())
                .setProperty("Created", constant("True"))
                // if custrefnum is not here, then create, if it is, then patch (updated) 
                // MAKE SURE TO CHECK RESPONSE WITH CUSTOMERS
                .process(this::checkCustrefnum)
                .log(LoggingLevel.INFO, "PROPERTY OF PROCESS: ${property.Process}")
                .doTry()
                    .choice()
                        .when(simple("${property.Process} == 'Create'"))
                            .log(LoggingLevel.INFO, "CREATE CUSTOMER")
                            .to("direct-vm:processCustomer")
                        // .endChoice()
                        .when().simple("${property.Process} == 'Update'")
                            .log(LoggingLevel.INFO, "UDPATE CUSTOMER")
                            .to("direct-vm:updateCustomer")
                        // .endChoice()
                    .end()   
                    .log(LoggingLevel.INFO, "NEWTEST: ${body}")
                    .process(this::setBodyFromOptional)
                .endDoTry()
                .doCatch(Exception.class)
                    .setProperty("Created", constant("False"))
                .end()
            .end()
            .process(this::finalizeResponse);
        
        // Use the C1SOAPEntityRepositoryRouteBuilder helper to build an update customer route
        c1SOAPEntityRepositoryRouteBuilder.addC1SOAPEntityGetByIDOnHeader(PROCESS_UPDATE_CUSTOMER_ROUTE_INTERNAL, GET_CUSTOMER_ROUTE, 
                CUST_REF_NUM_EXCHANGE_HEADER, Customer.class, ProcessCustomer.class, ProcessCustomerResponse.class, 
                addCustRequestQName, addCustResponseQName, this::createUpdateCustomerSOAPRequest, 
                this::getIDFromProcessCustomerResponse);
    }
    
    protected void checkCustrefnum(Exchange ex) {
        Gson gson = new Gson();
        String oldStr = ex.getIn().getBody(String.class);
        Customer newCust = ex.getIn().getBody(Customer.class);
        logger.info("CHECKCUSTREFNUM: " + newCust.getCustrefnum());
        ex.setProperty("crmrefnum", newCust.getCrmreferencenum());
        if ((newCust.getCustrefnum() == null) || (newCust.getCustrefnum().length() == 0)) {
            ex.setProperty("Process", "Create");
        }
        else {
            ex.setProperty("Process", "Update");
            ex.getIn().setHeader("custRefNum", newCust.getCustrefnum());
            ex.setProperty("custrefnum", newCust.getCustrefnum());
            //TODO CHECK IF THIS GETS RESET AFTER EVERY SPLIT; SHOULDN'T MATTER BUT JUST CLEANER EXCHANGES
        }
        logger.info("CHECKING IF CUSTREFNUM HEADER WAS SET: " + ex.getIn().getHeader("custRefNum", String.class));
    }
    
    /**
     * Reformats body to list of customers; sets content_type header to "application/json"
     */
    protected void finalizeResponse(Exchange ex) {
        Gson gson = new Gson();
        String oldStr = ex.getIn().getBody(String.class);
        logger.info("custResp from oldExchange: " + oldStr);
        List<Customer> custList = gson.fromJson(oldStr, List.class);

        ex.getIn().setBody(custList);
        ex.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
    }
    
    protected void setBodyFromOptional(Exchange exchange) {
        logger.info("setBodyFromOptional log: " + exchange.getIn().getBody(String.class));
        Optional optional = exchange.getIn().getBody(Optional.class);
        if (optional != null && optional.isPresent()) {            
            exchange.getIn().setBody(optional.get());
            exchange.getIn().removeHeader(Exchange.CONTENT_TYPE);
        }
        else if (optional != null && !optional.isPresent()) {
            RestErrorResponse rest404Error = new RestErrorResponse(404, "Entity Not Found.");
            rest404Error.setOnExchange(exchange);
        }
        else {
            RestErrorResponse rest500Error = new RestErrorResponse(500, "Invalid response from endpoint. " + exchange.getIn().getBody(String.class));
            rest500Error.setOnExchange(exchange);
        }
    }

    private void verifyCustomerResultExists(Exchange exchange) throws Exception {
        if (exchange.getIn().getBody() == null) {
            throw new Exception("Error attempting to retrive existing object. Message body was null.");
        }
        
        Optional<Customer> opt = exchange.getIn().getBody(Optional.class);
        
        if (opt == null) {
            throw new Exception("Error attempting to retrive existing object. Message body was not of the expected type "
                + "(Optional).");
        }
        
        if (!opt.isPresent()) {
            throw new ConnectBusinessException(Customer.class.getSimpleName() + " does not exist, and so cannot be updated.");
        }
        
        if (opt.isPresent() && !Customer.class.isAssignableFrom(opt.get().getClass())) {
            throw new Exception("Object was retrieved but an Optional of an unexpected type ["
                + opt.get().getClass() + "]. It must be assignable to [" + Customer.class + "]");
        }
        
        
    }
    
    /**
     * Determines if the GetCustomerResponse from the SOAP endpoint contains a customer, and if so, returns it.
     * Otherwise, return null.
     * @param getCustomerResponse The GetCustomerResponse object.
     * @return The Customer object, if available.
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException If the response 
     * from Configure One is empty.
     */
    protected Customer getCustomerFromSoapResponse (GetCustomerResponse getCustomerResponse) throws ConnectBusinessException {
        
        Customer cust;
        if (getCustomerResponse == null) {
            throw new ConnectBusinessException("The response from Configure One was empty.");
        }
        if (getCustomerResponse.getGetCustomerReturn() != null 
                && !StringUtils.isEmpty(getCustomerResponse.getGetCustomerReturn().getCUSTREFNUM())) {
            cust = new Customer(getCustomerResponse.getGetCustomerReturn());
            return cust;
        }
        else {
            return null;
        }
    }
    
    /**
     * Processes the customer response returned by configure one and returns the ID of the new object.
     * @param pcr The ProcessCustomerResponse object to be checked for a successful object creation.
     * @return The ID of the created Customer if successful.
     * @throws ConnectBusinessException If the ProcessCustomerResponse indicates an error.
     */
    protected String getIDFromProcessCustomerResponse(ProcessCustomerResponse pcr) throws ConnectBusinessException {
        Response processCustomerReturn = pcr.getProcessCustomerReturn();
        boolean success = processCustomerReturn.isSuccess();
        String message = processCustomerReturn.getMessage().getValue();
        String key = processCustomerReturn.getKey().getValue();
        //The returned Key from Configure One is a list of IDs separated by the '||' characters. So we parse that into a list.
        List<String> customerReturnRefIDs = Arrays.asList(key.split("\\|\\|"));
        //In that list, the 2nd element represents the customer ref num, so we get that. Otherwise throw an exception.
        if (success && customerReturnRefIDs != null && customerReturnRefIDs.size() >= 2 && customerReturnRefIDs.get(1) != null && !"null".equals(customerReturnRefIDs.get(1))) {
            logger.info("Success.");
            String createdRefNum = customerReturnRefIDs.get(1);
            return createdRefNum;
        } else {
            ConnectBusinessException cbe = new ConnectBusinessException("Error from Configure One while attempting to process request. See log for details.", "MESSAGE: " + message + "KEY: " + key);
            logger.error(cbe.toString());
            throw cbe;
        }
    }
    
    /**
     * Transforms a {@link com.configureone.integration.connect.core.model.Customer} into a 
     * Configure One SOAP ProcessCustomer request object.
     * @param customer The customer to be transformed
     * @param headers The headers contained in the request (optional)
     * @return The ProcessCustomer object
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException If the Customer object 
     * does not satisfy minimum requirements for creation.
     */
    protected com.configureone.ws.ProcessCustomer createUpdateCustomerSOAPRequest(Customer customer, Map<String, Object> headers) throws ConnectBusinessException {
        
        String custRefNumHeader = (String) headers.get(CUST_REF_NUM_EXCHANGE_HEADER);
        if (custRefNumHeader == null || StringUtils.isEmpty(custRefNumHeader)) {
            throw new ConnectBusinessException("Customer Reference Number must be provided in Header [" + CUST_REF_NUM_EXCHANGE_HEADER + "].");
        }
        
        //Check if a cust ref num is provided by the supplied customer object that it is consistent with the supplied header
        if (customer != null && customer.getCustrefnum() != null && !StringUtils.isEmpty(customer.getCustrefnum()) && !StringUtils.equals(customer.getCustrefnum(), custRefNumHeader)) {
            throw new ConnectBusinessException("Field [custrefnum] provided in customer ([" + customer.getCustrefnum() + "]) must match the one "
                    + "provided in header ([" + custRefNumHeader + "]), or be blank.");
        }
                
        ObjectFactory objectFactory = new ObjectFactory();
        com.configureone.ws.Customer createCustomer = objectFactory.createCustomer();
        
        //Set the request object's custrefnum to the header value supplied
        createCustomer.setCUSTREFNUM(custRefNumHeader);

        if (customer != null) {
            createCustomer.setACCOUNTBALANCE(customer.getAccountbalance());
            createCustomer.setACCOUNTNUM(customer.getAccountnum());
            createCustomer.setCREDITLIMIT(customer.getCreditlimit());
            createCustomer.setCRMREFERENCENUM(customer.getCrmreferencenum());
            createCustomer.setCUSTLOGO(customer.getCustlogo());
            createCustomer.setCUSTNAME(customer.getCustname());
            createCustomer.setCUSTOMERDISCOUNTGROUP(customer.getCustomerdiscountgroup());
            createCustomer.setDISCOUNTAMOUNT(customer.getDiscountamount());
            createCustomer.setEMAILADDRESS(customer.getEmailaddress());
            createCustomer.setENFORCECREDITLIMITIND(customer.getEnforcecreditlimitind());
            createCustomer.setERPREFERENCENUM(customer.getErpreferencenum());
            createCustomer.setPAYMENTTERMS(customer.getPaymentterms());
            createCustomer.setPRICEBOOK(customer.getPricebook());
            createCustomer.setSHIPPINGTERMS(customer.getShippingterms());
            createCustomer.setSHIPVIA(customer.getShipvia());
            createCustomer.setTAXEXEMPT(customer.getTaxexempt());
            createCustomer.setTAXREFERENCENUM(customer.getTaxreferencenum());
            createCustomer.setUDF1(customer.getUdf1());
            createCustomer.setUDF2(customer.getUdf2());
            createCustomer.setUDF3(customer.getUdf3());
            createCustomer.setUDF4(customer.getUdf4());
            createCustomer.setUDF5(customer.getUdf5());
            createCustomer.setUDF6(customer.getUdf6());
            createCustomer.setUDF7(customer.getUdf7());
            createCustomer.setUDF8(customer.getUdf8());
            createCustomer.setUDF9(customer.getUdf9());
            createCustomer.setUDF10(customer.getUdf10());
            createCustomer.setWEBSITE(customer.getWebsite());
            if (customer.getAddresses() != null) {
                for (Contact contact : customer.getAddresses()) {
                    if (contact != null && !StringUtils.isEmpty(contact.getCustrefnum()) && !StringUtils.equals(contact.getCustrefnum(), custRefNumHeader)) {
                        throw new ConnectBusinessException("Field [custrefnum] provided in customer contact ([" + contact.getCustrefnum() + "]) must match the one "
                                + "provided in header ([" + custRefNumHeader + "]), or be blank.");
                    }
                    if (contact != null) {
                        createCustomer.getAddress().add(contact.toAddress());
                    }
                }
            }
        }
        ProcessCustomer processCustomerRequest = objectFactory.createProcessCustomer();
        processCustomerRequest.setIn0(createCustomer);

        return processCustomerRequest;
    }
    
    /**
     * Transforms a {@link com.configureone.integration.connect.core.model.Customer} into a 
     * Configure One SOAP ProcessCustomer request object.
     * @param customer The customer to be transformed
     * @param headers The headers contained in the request (optional)
     * @return The ProcessCustomer object
     * @throws com.configureone.integration.connect.core.model.ConnectBusinessException If the Customer object 
     * does not satisfy minimum requirements for creation.
     */
    protected com.configureone.ws.ProcessCustomer createProcessCustomerSOAPRequest(Customer customer, Map<String, Object> headers) throws ConnectBusinessException {
        
        if (customer != null && !StringUtils.isEmpty(customer.getCustrefnum())) {
            throw new ConnectBusinessException("Field [custrefnum] must be blank, as it will be generated automatically "
                + "as a unique ID.");
        }
        
        ObjectFactory objectFactory = new ObjectFactory();
        com.configureone.ws.Customer createCustomer = objectFactory.createCustomer();
        
        if (StringUtils.isEmpty(createCustomer.getCUSTREFNUM())) {
            createCustomer.setCUSTREFNUM(UUID.randomUUID().toString());
        }
        if (customer != null) {
            createCustomer.setACCOUNTBALANCE(customer.getAccountbalance());
            createCustomer.setACCOUNTNUM(customer.getAccountnum());
            createCustomer.setCREDITLIMIT(customer.getCreditlimit());
            createCustomer.setCRMREFERENCENUM(customer.getCrmreferencenum());
            createCustomer.setCUSTLOGO(customer.getCustlogo());
            createCustomer.setCUSTNAME(customer.getCustname());
            createCustomer.setCUSTOMERDISCOUNTGROUP(customer.getCustomerdiscountgroup());
            createCustomer.setDISCOUNTAMOUNT(customer.getDiscountamount());
            createCustomer.setEMAILADDRESS(customer.getEmailaddress());
            createCustomer.setENFORCECREDITLIMITIND(customer.getEnforcecreditlimitind());
            createCustomer.setERPREFERENCENUM(customer.getErpreferencenum());
            createCustomer.setPAYMENTTERMS(customer.getPaymentterms());
            createCustomer.setPRICEBOOK(customer.getPricebook());
            createCustomer.setSHIPPINGTERMS(customer.getShippingterms());
            createCustomer.setSHIPVIA(customer.getShipvia());
            createCustomer.setTAXEXEMPT(customer.getTaxexempt());
            createCustomer.setTAXREFERENCENUM(customer.getTaxreferencenum());
            createCustomer.setUDF1(customer.getUdf1());
            createCustomer.setUDF2(customer.getUdf2());
            createCustomer.setUDF3(customer.getUdf3());
            createCustomer.setUDF4(customer.getUdf4());
            createCustomer.setUDF5(customer.getUdf5());
            createCustomer.setUDF6(customer.getUdf6());
            createCustomer.setUDF7(customer.getUdf7());
            createCustomer.setUDF8(customer.getUdf8());
            createCustomer.setUDF9(customer.getUdf9());
            createCustomer.setUDF10(customer.getUdf10());
            createCustomer.setWEBSITE(customer.getWebsite());
            if (customer.getAddresses() != null) {
                for (Contact contact : customer.getAddresses()) {
                    if (contact != null && !StringUtils.isEmpty(contact.getCustrefnum())) {
                        throw new ConnectBusinessException("Field [custrefnum] must be blank, as it will be automatically generated.");
                    }

                    if (contact != null && !StringUtils.isEmpty(contact.getAddrrefnum())) {
                        throw new ConnectBusinessException("Field [addrrefnum] must be blank, as it will be generated automatically.");
                    }
                    if (contact != null && contact.getTypecd() == null) {
                        throw new ConnectBusinessException("Field [typecd] is required and must be provided.");
                    }
                    if (contact != null) {
                        createCustomer.getAddress().add(contact.toAddress());
                    } else {
                        logger.warn("Null customer address found in customer being processed for customer [" + createCustomer.getCUSTREFNUM() + "].");
                    }
                }
            }
        }
        ProcessCustomer processCustomerRequest = objectFactory.createProcessCustomer();
        processCustomerRequest.setIn0(createCustomer);
        
        return processCustomerRequest;
    }

    /**
     * Processor used to create the GetCustomer body for sending to the soap endpoint, based on the 
     * Customer Reference Number value in the {@link #CUST_REF_NUM_EXCHANGE_HEADER} header.
     * @param message The message containing the Customer Reference Number header.
     * @return The GetCustomer request.
     */
    protected static GetCustomer generateGetCustomerSOAPObjFromHeaderRefNum(Message message) {
        String customerRefNum =
                message.getHeader(CUST_REF_NUM_EXCHANGE_HEADER,
                        String.class);

        GetCustomer getCustomer = new GetCustomer();
        getCustomer.setCustomerRefNum(customerRefNum);
        return getCustomer;
    }

}