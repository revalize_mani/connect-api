/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.configureone.integration.connect.api.transform;

import com.configureone.integration.connect.core.model.User;
import java.util.Map;

/**
 *
 * @author rolson
 */
public class UserMapper {
        
    /**
     * Mapping for the results from the SQL query to a User object. 
     * @param row A Map object from a List&lt;Map&gt; result set from the SQL component
     * @return The mapped user object
     */
    public static User mapRowToUser (Map row) {
        User user = new User();
        
        user.setUSER_ID((String)row.get("USER_ID"));
        user.setUSER_FIRST_NAME((String)row.get("USER_FIRST_NAME"));
        user.setUSER_EMAIL_ADDR((String)row.get("USER_EMAIL_ADDR"));
        user.setUSER_LAST_NAME((String)row.get("USER_LAST_NAME"));
        user.setCOMPANY_NAME((String)row.get("COMPANY_NAME"));
        user.setSECURITY_LEVEL_NUM(((Short)row.get("SECURITY_LEVEL_NUM")).toString());
        user.setUSER_STATUS_CD((String)row.get("USER_STATUS_CD"));
        user.setUSER_TYPE_CD((String)row.get("USER_TYPE_CD"));
        user.setUSER_NAME((String)row.get("USER_NAME"));
        user.setUSER_GROUP_ID((String)row.get("USER_GROUP_ID"));
        
        return user;
    }
}