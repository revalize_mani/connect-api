package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.api.routes.C1RestRoutesBuilder;
import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Customer;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.apache.camel.util.KeyValueHolder;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author cloud
 */
public class C1RestRoutesBuilderCustomersPatchTest extends CamelBlueprintTestSupport {

    public static final String CONNECT_C1APIGATEWAY_HOST = "localhost";
    public static final String UNDERLYING_ROUTE_ID = "updateCustomer";
    public static final String UNDERLYING_ROUTE_URI = C1RestRoutesBuilder.UPDATE_CUSTOMER_URI;
    public static final String MOCK_OBJECT_ID = "mockObjID";
    public static final String API_OBJECT_PATH = "/c1/rest/customers/" + MOCK_OBJECT_ID;
    public static final String OBJECT_ID_HEADER_NAME = "custRefNum";
    public static final Class<Customer> OBJECT_CLASS = Customer.class;
    private CloseableHttpClient httpClient;
    MockEndpoint mockUnderlyingRouteEndpoint;
    HttpPatch httpPatch;
    
    public void assertObjectsEqualFunction(Customer expectedT, Customer actualT) {
        assertEquals(expectedT.getAccountbalance(), actualT.getAccountbalance());
        assertEquals(expectedT.getAccountnum(), actualT.getAccountnum());
        assertEquals(expectedT.getCreditlimit(), actualT.getCreditlimit());
        assertEquals(expectedT.getCrmreferencenum(), actualT.getCrmreferencenum());
        assertEquals(expectedT.getCustlogo(), actualT.getCustlogo());
        assertEquals(expectedT.getCustname(), actualT.getCustname());
        assertEquals(expectedT.getCustomerdiscountgroup(), actualT.getCustomerdiscountgroup());
        assertEquals(expectedT.getCustrefnum(), actualT.getCustrefnum());
        assertEquals(expectedT.getDiscountamount(), actualT.getDiscountamount());
        assertEquals(expectedT.getEmailaddress(), actualT.getEmailaddress());
        assertEquals(expectedT.getEnforcecreditlimitind(), actualT.getEnforcecreditlimitind());
        assertEquals(expectedT.getErpreferencenum(), actualT.getErpreferencenum());
        assertEquals(expectedT.getPaymentterms(), actualT.getPaymentterms());
        assertEquals(expectedT.getPricebook(), actualT.getPricebook());
        assertEquals(expectedT.getShippingterms(), actualT.getShippingterms());
        assertEquals(expectedT.getShipvia(), actualT.getShipvia());
        assertEquals(expectedT.getTaxexempt(), actualT.getTaxexempt());
        assertEquals(expectedT.getTaxreferencenum(), actualT.getTaxreferencenum());
        assertEquals(expectedT.getWebsite(), actualT.getWebsite());
    }
    public Customer createMockTFunction(String suffix) {
        Customer mockCustomer = new Customer();
        mockCustomer.setAccountbalance((float)456.789);
        mockCustomer.setAccountnum("mockAcctNum" + suffix);
        mockCustomer.setCreditlimit((float)1.23);
        mockCustomer.setCrmreferencenum("mockCRMRefNum" + suffix);
        mockCustomer.setCustlogo("mockCustLogo" + suffix);
        mockCustomer.setCustname("mockCustName" + suffix);
        mockCustomer.setCustomerdiscountgroup("mockDiscountGrp" + suffix);
        mockCustomer.setCustrefnum("mockCustRefNum" + suffix);
        mockCustomer.setDiscountamount("mockDiscountAmt" + suffix);
        mockCustomer.setEmailaddress("mockEmailAddr" + suffix);
        mockCustomer.setEnforcecreditlimitind("mockCreditLimit" + suffix);
        mockCustomer.setErpreferencenum("mockERPRefNum" + suffix);
        mockCustomer.setPaymentterms("mockPaymentTerms" + suffix);
        mockCustomer.setPricebook("mockPricebook" + suffix);
        mockCustomer.setShippingterms("mockShipTerms" + suffix);
        mockCustomer.setShipvia("mockShipVia" + suffix);
        mockCustomer.setTaxexempt("mockTaxExempt" + suffix);
        mockCustomer.setTaxreferencenum("mockTaxRefNum" + suffix);
        mockCustomer.setUdf1("mockUdf1" + suffix);
        mockCustomer.setUdf2("mockUdf2" + suffix);
        mockCustomer.setUdf3("mockUdf3" + suffix);
        mockCustomer.setUdf4("mockUdf4" + suffix);
        mockCustomer.setUdf5("mockUdf5" + suffix);
        mockCustomer.setUdf6("mockUdf6" + suffix);
        mockCustomer.setUdf7("mockUdf7" + suffix);
        mockCustomer.setUdf8("mockUdf8" + suffix);
        mockCustomer.setUdf9("mockUdf9" + suffix);
        mockCustomer.setUdf10("mockUdf10" + suffix);
        mockCustomer.setWebsite("mockWebsite" + suffix);
        return mockCustomer;
    }
    
    @Override
    public boolean isCreateCamelContextPerClass() {
        return false;
    }
    
    @Override
    protected Properties useOverridePropertiesWithPropertiesComponent() {
        Properties prop = new Properties();
        //This property needs to be defined here since it is used with the initial Rest component configuration.
        prop.put("c1.rest.port", "8090");
        return prop;
    }

    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

    /**
     * Ensures that only the c1apigateway Camel context starts up so that
     * we don't test the wrong context.  CamelBlueprintTestSupport can only
     * test one context.
     * 
     * @return the bundle filter
     */
    @Override
    protected String getBundleFilter() {
        return "(&(Bundle-SymbolicName=*)(!(Bundle-SymbolicName=com.configureone.integration.connect.core*)))";
    }
    
    @Before
    public void setUpTests() throws Exception {
        super.setUp();
        if (httpClient == null) {
            httpClient = HttpClients.createDefault();
        }
        //Replace the get quote endpoint at the end of the route with a mock one
        context.getRouteDefinition(UNDERLYING_ROUTE_ID).adviceWith (
                context, new AdviceWithRouteBuilder() {
            
                @Override
                public void configure() throws Exception {
                    mockEndpointsAndSkip(UNDERLYING_ROUTE_URI);
                }
        });
        context.start();
        mockUnderlyingRouteEndpoint = getMockEndpoint("mock:" + UNDERLYING_ROUTE_URI);
        String restPort = context.resolvePropertyPlaceholders("{{c1.rest.port}}");
        httpPatch = new HttpPatch("http://" + CONNECT_C1APIGATEWAY_HOST + ":" + restPort + API_OBJECT_PATH);
        httpPatch.addHeader("content-type", "application/json");
    }

    @After
    public void closeHttpClient() throws IOException, Exception {
        httpClient.close();
        httpClient = null;
    }
    
    /**
     * Overrides CamelBlueprintTestSupport.addServicesOnStartup() to
     * define any necessary OSGi services required for use in the testing.
     * @param services
     */
    @Override
    protected void addServicesOnStartup(Map<String, KeyValueHolder<Object, Dictionary>> services) {
	services.put(ConnectCorePropertiesService.class.getName(), asService(new MockConnectCoreProperties(), null));
    }

    @Test
    public void testPatchMethod() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Customer mockObjToAdd = createMockTFunction("1");
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
        
        //Create the mock object to return from the endpoint for testing
        
        Customer mockObjToReturn = createMockTFunction("2");
        Optional optionalObjToReturn = Optional.of(mockObjToReturn);
        
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Customer receivedObj = exchange.getIn().getBody(OBJECT_CLASS);
            assertEquals(MOCK_OBJECT_ID, exchange.getIn().getHeader(OBJECT_ID_HEADER_NAME));
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setBody(optionalObjToReturn);
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        Customer returnedT = new Gson().fromJson(httpJsonResponse.getJsonResponse(), OBJECT_CLASS);
        
        assertMockEndpointsSatisfied();
        assertEquals(200, httpJsonResponse.getHttpResponseCode());
        //Check the good return values are as expected
        assertObjectsEqualFunction(mockObjToReturn, returnedT);
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPatchMethodExceptionResponse() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Customer mockObjToAdd = createMockTFunction("1");
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
        
        //Create the mock object to return from the endpoint for testing
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        ConnectBusinessException mockBusinessException = new ConnectBusinessException("mockBusinessException");
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Customer receivedObj = exchange.getIn().getBody(OBJECT_CLASS);
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            throw mockBusinessException;
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        assertEquals(mockBusinessException.getMessage(), errorResponse.getMessage());
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPatchMethodEmptyResponse() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Customer mockObjToAdd = createMockTFunction("1");
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Customer receivedObj = exchange.getIn().getBody(OBJECT_CLASS);
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(Optional.empty());
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        assertEquals(404, errorResponse.getHttpErrorCode());
        assertEquals("Entity Not Found.", errorResponse.getMessage());
        assertEquals(404, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPatchMethodMalformedResponse() throws Exception {
        
                
        //Create the mock json to send to the endpoint for testing.
        Customer mockObjToAdd = createMockTFunction("1");
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Customer receivedObj = exchange.getIn().getBody(OBJECT_CLASS);
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(new ArrayList<Object>());
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        String expectedErrorMessagePrefix = "Invalid response from endpoint.";
        assertTrue("Expected message starting with [" + expectedErrorMessagePrefix + "] but was [" + errorResponse.getMessage() + "].", errorResponse.getMessage().startsWith(expectedErrorMessagePrefix));
        assertEquals(500, errorResponse.getHttpErrorCode());
        assertEquals(500, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPatchMethodBadInput() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        String mockObjJson = "{somebadjson: [[]]]]}}}}}";
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        String expectedErrorMessagePrefix = "com.google.gson.stream.MalformedJsonException";
        assertTrue("Expected message starting with [" + expectedErrorMessagePrefix + "] but was [" + errorResponse.getMessage() + "].", errorResponse.getMessage().startsWith(expectedErrorMessagePrefix));
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPatchMethodBadEnumVal() throws Exception {
                        
        String mockCustRefNum = "mockCustRefNum";
        String mockCustRefName = "mockCustRefName";
        String mockAddrRefNum = "mockAddrRefNum";
        String mockPrimaryInd = "mockPrimaryInd";
        String mockBadEnumVal = "BAD";
        
        String contactBadEnumJson = "{\n"
            + "\"custrefnum\": \"" + mockCustRefNum + "\",\n" +
            "  \"addrrefnum\": \"" + mockAddrRefNum + "\",\n" +
            "  \"primaryind\": \"" + mockPrimaryInd + "\",\n" +
            "  \"typecd\": \"" + mockBadEnumVal + "\"\n"
            + "}";
        
        String badEnumJson= "{\n" +
            "  \"custrefnum\": \"" + mockCustRefNum + "\",\n" +
            "  \"custname\": \"" + mockCustRefName + "\",\n" +
            "  \"contacts\": [" + contactBadEnumJson + "]\n" +
            "}";
        
        
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(0);
                        
        //test a bad enum message
        StringEntity contentEntity = new StringEntity(badEnumJson);
        httpPatch.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPatch);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
                
        String expectedErrorMessage = "[JsonParseException] Error deserializing Json enum: No enum constant "
                + "com.configureone.integration.connect.core.model.Contact.TypeCode." + mockBadEnumVal;
        assertEquals(expectedErrorMessage, errorResponse.getMessage());
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    public HttpJsonResponse makeJsonHttpCall(HttpUriRequest uriRequest) throws IOException {
        HttpJsonResponse httpJsonResponse = new HttpJsonResponse();
        try (CloseableHttpResponse response = httpClient.execute(uriRequest)) {
            InputStream responseStream = response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            httpJsonResponse.setJsonResponse(writer.toString());
            httpJsonResponse.setHttpResponseCode(response.getStatusLine().getStatusCode());
            httpJsonResponse.setHeaders(response.getAllHeaders());
        }
        return httpJsonResponse;
    }
}
