/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.properties.ConnectCoreProperties;
import com.configureone.integration.connect.core.services.ConnectRoute;

/**
 *
 * @author rmoutray
 */
public class MockConnectCoreProperties extends ConnectCoreProperties
{

    public MockConnectCoreProperties()
    {
        super();
        setGetQuoteResponseRoute(new ConnectRoute("direct:amqp:getQuoteResponse", "mock:direct:amqp:getQuoteResponse"));
        setDeadLetterQueueRoute(new ConnectRoute("direct:amqp:deadLetterQueue", "mock:direct:amqp:deadLetterQueue"));
        setGetSingleDocumentRoute(new ConnectRoute("direct-vm:getSingleDocument", "mock:direct-vm:getSingleDocument"));
        setCallConfigureOneSyncRoute(new ConnectRoute("direct-vm:CallConfigureOne", "mock:direct-vm:CallConfigureOne"));
    }
}