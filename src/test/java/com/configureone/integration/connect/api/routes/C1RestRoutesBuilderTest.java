package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Customer;
import com.configureone.integration.connect.core.model.Quote;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.ws.Input;
import com.configureone.ws.Value;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.apache.camel.util.KeyValueHolder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpException;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author cloud
 */
public class C1RestRoutesBuilderTest extends CamelBlueprintTestSupport {

    public static final String CONNECT_C1APIGATEWAY_HOST = "localhost";
    //public static final int CONNECT_C1APIGATEWAY_PORT = 8089;
    public static final String CONNECT_C1APIGATEWAY_API_DOCS_PATH =
            "c1/api-docs/connect-api";
    public static final String CONNECT_C1APIGATEWAY_QUOTES_PATH = "/c1/rest/quotes";
    public static final String CONNECT_C1APIGATEWAY_CUSTOMERS_PATH = "/c1/rest/customers";
    private CloseableHttpClient httpClient;

    @Override
    protected Properties useOverridePropertiesWithPropertiesComponent() {
        Properties prop = new Properties();
        //This property needs to be defined here since it is used with the initial Rest component configuration.
        prop.put("c1.rest.port", "8090");
        return prop;
    }

    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

    /**
     * Ensures that only the c1apigateway Camel context starts up so that
     * we don't test the wrong context.  CamelBlueprintTestSupport can only
     * test one context.
     *
     * @return the bundle filter
     */
    @Override
    protected String getBundleFilter() {
        return "(&(Bundle-SymbolicName=*)(!(Bundle-SymbolicName=com.configureone.integration.connect.core*)))";
    }

    @Before
    public void setUpTests() throws Exception {
        if (httpClient == null) {
            httpClient = HttpClients.createDefault();
        }
    }

    @After
    public void closeHttpClient() throws IOException, Exception {
        httpClient.close();
        httpClient = null;
    }

    /**
     * Overrides CamelBlueprintTestSupport.addServicesOnStartup() to
     * define any necessary OSGi services required for use in the testing.
     * @param services
     */
    @Override
    protected void addServicesOnStartup(Map<String, KeyValueHolder<Object, Dictionary>> services) {
	services.put(ConnectCorePropertiesService.class.getName(), asService(new MockConnectCoreProperties(), null));
    }

    @Test
    public void test0ExistanceOfSwaggerSpec() throws Exception {

        context.start();
        String restPort = context.resolvePropertyPlaceholders("{{c1.rest.port}}");

        String url = String.format("http://%s:%s/%s",
                CONNECT_C1APIGATEWAY_HOST,
                restPort,
                CONNECT_C1APIGATEWAY_API_DOCS_PATH);
        HttpGet httpGet = new HttpGet(url);
        File targetJsonFile = new File("target/connect-api-swagger-spec.json");
        try(CloseableHttpResponse response = httpClient.execute(httpGet)) {
            response.getEntity().writeTo(new FileOutputStream(targetJsonFile));
        }
        String swaggerSpecJSON = FileUtils.readFileToString(targetJsonFile, "UTF-8");
        assertTrue(swaggerSpecJSON.contains("swagger"));
    }

    @Test
    public void testQuotePostMethod() throws Exception {
        //Replace the get quote endpoint at the end of the route with a mock one
        context.getRouteDefinition("postQuote").adviceWith (

                context, new AdviceWithRouteBuilder() {

                @Override
                public void configure() throws Exception {
                    mockEndpointsAndSkip("direct-vm:processQuote");
                }
        });
        context.start();


        //Create the mock returned quote object / json to return from the endpoint for testing.
        String mockReturnId = "mockReturnId";
        String mockReturnUserId = "mockReturnUserId";
        String mockReturnSerialNum = "mockReturnSerialNum";
        String mockReturnMaintainBuild = "true";
        String mockReturnBillToERPRefNum = "mockReturnBillToERPRefNum";
        String mockReturnCustomerRefNum = "mockReturnCustRefNum";
        String mockReturnQuoteName = "mockReturnQuoteName";
        String mockReturnPricebookName = "mockReturnPricebookName";
        String mockReturnShipToCRMRefNum = "mockReturnShipToCRMRefNum";
        String mockReturnStatus = "mockReturnStatus";
        String mockReturnTemplateName = "mockReturnTemplateName";
        String mockReturnQuoteInputName = "mockReturnInputName";
        String mockReturnQuoteInputValue = "mockReturnInputValue";
        Input mockReturnQuoteInput = new Input();
        mockReturnQuoteInput.setName(mockReturnQuoteInputName);
        Value mockReturnQuoteValue = new Value();
        mockReturnQuoteValue.setName(mockReturnQuoteInputValue);
        mockReturnQuoteInput.getValue().add(mockReturnQuoteValue);

        Quote mockReturnQuote = new Quote();
        mockReturnQuote.setId(mockReturnId);
        mockReturnQuote.setUserId(mockReturnUserId);
        mockReturnQuote.setBillToERPContactRefNum(mockReturnBillToERPRefNum);
        mockReturnQuote.setCustRefNum(mockReturnCustomerRefNum);
        mockReturnQuote.setName(mockReturnQuoteName);
        mockReturnQuote.setPricebookName(mockReturnPricebookName);
        mockReturnQuote.setShipToCRMContactRefNum(mockReturnShipToCRMRefNum);
        mockReturnQuote.setStatus(mockReturnStatus);
        mockReturnQuote.setTemplateName(mockReturnTemplateName);
        mockReturnQuote.setSerialNumber(mockReturnSerialNum);
        mockReturnQuote.setMaintainBuild(mockReturnMaintainBuild);
        mockReturnQuote.setInputs(new ArrayList<Input>());
        mockReturnQuote.getInputs().add(mockReturnQuoteInput);

        MockEndpoint mockProcessQuoteEndpoint = getMockEndpoint("mock:direct-vm:processQuote");

        String restPort = context.resolvePropertyPlaceholders("{{c1.rest.port}}");

        //Create the mock quote object / json to send to the endpoint for testing.
        String mockUserId = "mockUserId";
        String mockSerialNum = "mockSerialNum";
        String mockMaintainBuild = "true";
        String mockBillToRefNum = "mockBillToRefNum";
        String mockCustomerCRMRefNum = "mockCustCRMRefNum";
        String mockQuoteName = "mockQuoteName";
        String mockPricebookName = "mockPricebookName";
        String mockShipToERPRefNum = "mockShipToERPRefNum";
        String mockStatus = "mockStatus";
        String mockTemplateName = "mockTemplateName";
        String mockQuoteInputName = "mockInputName";
        String mockQuoteInputValue = "mockInputValue";

        Input mockQuoteInput = new Input();
        mockQuoteInput.setName(mockQuoteInputName);
        Value mockQuoteValue = new Value();
        mockQuoteValue.setName(mockQuoteInputValue);
        mockQuoteInput.getValue().add(mockQuoteValue);

        Quote mockQuote = new Quote();
        mockQuote.setUserId(mockUserId);
        mockQuote.setBillToRefNum(mockBillToRefNum);
        mockQuote.setCrmReferenceNum(mockCustomerCRMRefNum);
        mockQuote.setName(mockQuoteName);
        mockQuote.setPricebookName(mockPricebookName);
        mockQuote.setShipToERPContactRefNum(mockShipToERPRefNum);
        mockQuote.setStatus(mockStatus);
        mockQuote.setTemplateName(mockTemplateName);
        mockQuote.setSerialNumber(mockSerialNum);
        mockQuote.setMaintainBuild(mockMaintainBuild);
        mockReturnQuote.getInputs().add(mockQuoteInput);

        Gson gson = new Gson();
        String mockQuoteJson = gson.toJson(mockQuote);



        mockProcessQuoteEndpoint.expectedMessageCount(3);
        //Test 1 - Assert good data request matches for 1st message
        mockProcessQuoteEndpoint.whenExchangeReceived(1, (Exchange exchange) -> {
            Quote quote = exchange.getIn().getBody(Quote.class);
            assertEquals(mockUserId, quote.getUserId());
            assertEquals(mockBillToRefNum, quote.getBillToRefNum());
            assertEquals(mockCustomerCRMRefNum, quote.getCrmReferenceNum());
            assertEquals(mockShipToERPRefNum, quote.getShipToERPContactRefNum());
            assertEquals(mockQuoteName, quote.getName());
            exchange.getIn().setBody(mockReturnQuote);
        });
        //Test 2 - throw a 400 level exception (ConnectBusinessExceptoin)
        String mockC1ErrorMessage = "mockC1ErrorMessage";
        String mockBusinessExceptionMessage = "mockBusinessException";
        ConnectBusinessException mockC1400FailureResponse = new ConnectBusinessException(mockBusinessExceptionMessage, mockC1ErrorMessage);
        mockProcessQuoteEndpoint.whenExchangeReceived(2, (Exchange exchange) -> {
            throw mockC1400FailureResponse;
            //mockC1400FailureResponse.setOnExchange(exchange);
            //exchange.setException(mockC1400FailureResponse);
            //assertTrue(exchange.isFailed());
        });
        //Test 5 - throw a http exception from underlying route
        String mock500Error = "mock500Error";
        HttpException mock500Exception = new HttpException(mock500Error);
        mockProcessQuoteEndpoint.whenExchangeReceived(3, (Exchange exchange) -> {
            throw mock500Exception;
        });

        HttpPost httpPost = new HttpPost("http://" + CONNECT_C1APIGATEWAY_HOST + ":" + restPort + CONNECT_C1APIGATEWAY_QUOTES_PATH);
        httpPost.addHeader("content-type", "application/json");

        //Test 1 - test a good message
        StringEntity contentEntity = new StringEntity(mockQuoteJson);
        httpPost.setEntity(contentEntity);
        String goodResponseContent;
        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            InputStream responseStream = response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            goodResponseContent = writer.toString();
        }
        Quote actualGoodQuoteReturn = gson.fromJson(goodResponseContent, Quote.class);

        //Test 2 - Test a bad message with CIBusinessException error returned by underlying route
        String bad400ResponseContent;
        StatusLine C1400ResponseStatus;
        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            InputStream responseStream = response.getEntity().getContent();
            C1400ResponseStatus = response.getStatusLine();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            bad400ResponseContent = writer.toString();
        }
        RestErrorResponse bad400RestResponse = new Gson().fromJson(bad400ResponseContent, RestErrorResponse.class);

        //Test 4 - test a bad message with a malformed json error
        String malformedJson = "{{{{{{{]]]]]]]]]]]]]";
        StringEntity malformedJsonContentEntity = new StringEntity(malformedJson);
        httpPost.setEntity(malformedJsonContentEntity);

        String malformedJsonResponseContent;
        StatusLine malformedJsonResponseStatus;
        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            InputStream responseStream = response.getEntity().getContent();
            malformedJsonResponseStatus = response.getStatusLine();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            malformedJsonResponseContent = writer.toString();
        }
        RestErrorResponse malformedJsonRestResponse = new Gson().fromJson(malformedJsonResponseContent, RestErrorResponse.class);

        //5 - test handling of an exception thrown by underlying route.
        contentEntity = new StringEntity(mockQuoteJson);
        httpPost.setEntity(contentEntity);
        String error500ResponseContent;
        StatusLine error500ResponseStatus;
        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            InputStream responseStream = response.getEntity().getContent();
            error500ResponseStatus = response.getStatusLine();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            error500ResponseContent = writer.toString();
        }
        RestErrorResponse error500RestResponse = new Gson().fromJson(error500ResponseContent, RestErrorResponse.class);

        assertMockEndpointsSatisfied();

        //Test 1 - Check the good return values are as expected
        assertEquals(mockReturnQuote.getId(), actualGoodQuoteReturn.getId());
        assertEquals(mockReturnQuote.getSerialNumber(), actualGoodQuoteReturn.getSerialNumber());
        assertEquals(mockReturnQuote.getName(), actualGoodQuoteReturn.getName());
        assertEquals(mockReturnQuote.getUserId(), actualGoodQuoteReturn.getUserId());
        assertEquals(mockReturnQuote.getBillToERPContactRefNum(), actualGoodQuoteReturn.getBillToERPContactRefNum());
        assertEquals(mockReturnQuote.getCustRefNum(), actualGoodQuoteReturn.getCustRefNum());
        assertEquals(mockReturnQuote.getPricebookName(), actualGoodQuoteReturn.getPricebookName());
        assertEquals(mockReturnQuote.getShipToCRMContactRefNum(), actualGoodQuoteReturn.getShipToCRMContactRefNum());
        assertEquals(mockReturnQuote.getStatus(), actualGoodQuoteReturn.getStatus());
        assertEquals(mockReturnQuote.getTemplateName(), actualGoodQuoteReturn.getTemplateName());

        Map<String, String> actualQuoteInputsMap = new HashMap<>();
        for (Input actualQi : actualGoodQuoteReturn.getInputs()) {
            actualQuoteInputsMap.put(actualQi.getName(), actualQi.getValue().get(0).getName());
        }

        assertEquals(mockReturnQuote.getInputs().size(), mockReturnQuote.getInputs().size());
        for (Input mockQi : mockReturnQuote.getInputs()) {
            assertTrue(actualQuoteInputsMap.containsKey(mockQi.getName()));
            assertEquals(mockQi.getValue().get(0).getName(), actualQuoteInputsMap.get(mockQi.getName()));
        }

        //Test 2 - Check the 400 error returns as expected
        assertEquals(400, C1400ResponseStatus.getStatusCode());
        assertEquals(mockC1400FailureResponse.getMessage(), bad400RestResponse.getMessage());
        assertEquals(mockC1400FailureResponse.getDetails(), bad400RestResponse.getDetails());

        //Test 4 - Check the malformed json error returns as expected
        assertEquals(400, malformedJsonResponseStatus.getStatusCode());
        assertEquals("com.google.gson.stream.MalformedJsonException: Expected name at line 1 column 2 path $.", malformedJsonRestResponse.getMessage());
        assertEquals("MalformedJsonException: Expected name at line 1 column 2 path $.", malformedJsonRestResponse.getDetails());

        //Test 5 - Check the 500 error json returns as expected
        assertEquals(500, error500ResponseStatus.getStatusCode());
        assertEquals(mock500Error, error500RestResponse.getMessage());
        assertEquals("HttpException: mock500Error", error500RestResponse.getDetails());
    }

    protected Customer createMockCustomer(String suffix) {
        Customer mockCustomer = new Customer();
        mockCustomer.setAccountbalance((float)456.789);
        mockCustomer.setAccountnum("mockAcctNum" + suffix);
        mockCustomer.setCreditlimit((float)1.23);
        mockCustomer.setCrmreferencenum("mockCRMRefNum" + suffix);
        mockCustomer.setCustlogo("mockCustLogo" + suffix);
        mockCustomer.setCustname("mockCustName" + suffix);
        mockCustomer.setCustomerdiscountgroup("mockDiscountGrp" + suffix);
        mockCustomer.setCustrefnum("mockCustRefNum" + suffix);
        mockCustomer.setDiscountamount("mockDiscountAmt" + suffix);
        mockCustomer.setEmailaddress("mockEmailAddr" + suffix);
        mockCustomer.setEnforcecreditlimitind("mockCreditLimit" + suffix);
        mockCustomer.setErpreferencenum("mockERPRefNum" + suffix);
        mockCustomer.setPaymentterms("mockPaymentTerms" + suffix);
        mockCustomer.setPricebook("mockPricebook" + suffix);
        mockCustomer.setShippingterms("mockShipTerms" + suffix);
        mockCustomer.setShipvia("mockShipVia" + suffix);
        mockCustomer.setTaxexempt("mockTaxExempt" + suffix);
        mockCustomer.setTaxreferencenum("mockTaxRefNum" + suffix);
        mockCustomer.setUdf1("mockUdf1" + suffix);
        mockCustomer.setUdf2("mockUdf2" + suffix);
        mockCustomer.setUdf3("mockUdf3" + suffix);
        mockCustomer.setUdf4("mockUdf4" + suffix);
        mockCustomer.setUdf5("mockUdf5" + suffix);
        mockCustomer.setUdf6("mockUdf6" + suffix);
        mockCustomer.setUdf7("mockUdf7" + suffix);
        mockCustomer.setUdf8("mockUdf8" + suffix);
        mockCustomer.setUdf9("mockUdf9" + suffix);
        mockCustomer.setUdf10("mockUdf10" + suffix);
        mockCustomer.setWebsite("mockWebsite" + suffix);
        return mockCustomer;
    }

    protected void deepCompareCustomer(Customer expectedCustomer, Customer actualCustomer) {
        assertEquals(expectedCustomer.getAccountbalance(), actualCustomer.getAccountbalance());
        assertEquals(expectedCustomer.getAccountnum(), actualCustomer.getAccountnum());
        assertEquals(expectedCustomer.getCreditlimit(), actualCustomer.getCreditlimit());
        assertEquals(expectedCustomer.getCrmreferencenum(), actualCustomer.getCrmreferencenum());
        assertEquals(expectedCustomer.getCustlogo(), actualCustomer.getCustlogo());
        assertEquals(expectedCustomer.getCustname(), actualCustomer.getCustname());
        assertEquals(expectedCustomer.getCustomerdiscountgroup(), actualCustomer.getCustomerdiscountgroup());
        assertEquals(expectedCustomer.getCustrefnum(), actualCustomer.getCustrefnum());
        assertEquals(expectedCustomer.getDiscountamount(), actualCustomer.getDiscountamount());
        assertEquals(expectedCustomer.getEmailaddress(), actualCustomer.getEmailaddress());
        assertEquals(expectedCustomer.getEnforcecreditlimitind(), actualCustomer.getEnforcecreditlimitind());
        assertEquals(expectedCustomer.getErpreferencenum(), actualCustomer.getErpreferencenum());
        assertEquals(expectedCustomer.getPaymentterms(), actualCustomer.getPaymentterms());
        assertEquals(expectedCustomer.getPricebook(), actualCustomer.getPricebook());
        assertEquals(expectedCustomer.getShippingterms(), actualCustomer.getShippingterms());
        assertEquals(expectedCustomer.getShipvia(), actualCustomer.getShipvia());
        assertEquals(expectedCustomer.getTaxexempt(), actualCustomer.getTaxexempt());
        assertEquals(expectedCustomer.getTaxreferencenum(), actualCustomer.getTaxreferencenum());
        assertEquals(expectedCustomer.getWebsite(), actualCustomer.getWebsite());
    }
}
