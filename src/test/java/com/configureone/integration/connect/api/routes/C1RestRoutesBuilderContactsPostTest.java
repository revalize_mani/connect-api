package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Contact;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;
import com.configureone.ws.Address;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.apache.camel.util.KeyValueHolder;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author cloud
 */
public class C1RestRoutesBuilderContactsPostTest extends CamelBlueprintTestSupport {

    public static final String CONNECT_C1APIGATEWAY_HOST = "localhost";
    public static final String UNDERLYING_ROUTE_ID = "postCustomerContact";
    public static final String UNDERLYING_ROUTE_URI = "direct-vm:processContact";
    public static final String MOCK_CUSTOMER_FOR_CONTACT = "mockCustomerForContact";
    public static final String API_OBJECT_PATH = "/c1/rest/customers/" + MOCK_CUSTOMER_FOR_CONTACT + "/contacts";
    private CloseableHttpClient httpClient;
    MockEndpoint mockUnderlyingRouteEndpoint;
    HttpPost httpPost;
    
    public void assertObjectsEqualFunction(Contact expectedT, Contact actualT) {
        assertEquals(expectedT.getAddressline1(), actualT.getAddressline1());
        assertEquals(expectedT.getAddressline2(), actualT.getAddressline2());
        assertEquals(expectedT.getAddressline3(), actualT.getAddressline3());
        assertEquals(expectedT.getAddrrefnum(), actualT.getAddrrefnum());
        assertEquals(expectedT.getCity(), actualT.getCity());
        assertEquals(expectedT.getContactname(), actualT.getContactname());
        assertEquals(expectedT.getCountry(), actualT.getCountry());
        assertEquals(expectedT.getCrmcontactrefnum(), actualT.getCrmcontactrefnum());
        assertEquals(expectedT.getCustrefnum(), actualT.getCustrefnum());
        assertEquals(expectedT.getEmailaddress(), actualT.getEmailaddress());
        assertEquals(expectedT.getErpcontactrefnum(), actualT.getErpcontactrefnum());
        assertEquals(expectedT.getFaxnumber(), actualT.getFaxnumber());
        assertEquals(expectedT.getPhonenumber(), actualT.getPhonenumber());
        assertEquals(expectedT.getPostalcode(), actualT.getPostalcode());
        assertEquals(expectedT.getPrimaryind(), actualT.getPrimaryind());
        assertEquals(expectedT.getState(), actualT.getState());
        assertEquals(expectedT.getTypecd(), actualT.getTypecd());
    }
    public Contact createMockTFunction(String suffix) {
        Contact address = new Contact();
        address.setAddressline1("mockAddr1");
        address.setAddressline2("mockAddr2");
        address.setAddressline3("mockAddr3");
        address.setAddrrefnum("mockAddrRefNum");
        address.setCity("mockCity");
        address.setContactname("mockContact");
        address.setCountry("mockCountry");
        address.setCrmcontactrefnum("mockCrmCRef");
        address.setCustrefnum("mockCustRefNum");
        address.setEmailaddress("mockEmail");
        address.setErpcontactrefnum("mockERPRefNum");
        address.setFaxnumber("mockFax");
        address.setPhonenumber("mockPhone");
        address.setPostalcode("mockPostal");
        address.setPrimaryind("mockPrimaryInd");
        address.setState("mockState");
        address.setTypecd(Contact.TypeCode.B);
        return address;
    }
    
    @Override
    public boolean isCreateCamelContextPerClass() {
        return false;
    }
    
    @Override
    protected Properties useOverridePropertiesWithPropertiesComponent() {
        Properties prop = new Properties();
        //This property needs to be defined here since it is used with the initial Rest component configuration.
        prop.put("c1.rest.port", "8090");
        return prop;
    }

    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

    /**
     * Ensures that only the c1apigateway Camel context starts up so that
     * we don't test the wrong context.  CamelBlueprintTestSupport can only
     * test one context.
     * 
     * @return the bundle filter
     */
    @Override
    protected String getBundleFilter() {
        return "(&(Bundle-SymbolicName=*)(!(Bundle-SymbolicName=com.configureone.integration.connect.core*)))";
    }
    
    @Before
    public void setUpTests() throws Exception {
        super.setUp();
        if (httpClient == null) {
            httpClient = HttpClients.createDefault();
        }
        //Replace the get quote endpoint at the end of the route with a mock one
        context.getRouteDefinition(UNDERLYING_ROUTE_ID).adviceWith (
                context, new AdviceWithRouteBuilder() {
            
                @Override
                public void configure() throws Exception {
                    mockEndpointsAndSkip(UNDERLYING_ROUTE_URI);
                }
        });
        context.start();
        mockUnderlyingRouteEndpoint = getMockEndpoint("mock:" + UNDERLYING_ROUTE_URI);
        String restPort = context.resolvePropertyPlaceholders("{{c1.rest.port}}");
        httpPost = new HttpPost("http://" + CONNECT_C1APIGATEWAY_HOST + ":" + restPort + API_OBJECT_PATH);
        httpPost.addHeader("content-type", "application/json");
    }

    @After
    public void closeHttpClient() throws IOException, Exception {
        httpClient.close();
        httpClient = null;
    }
    
    /**
     * Overrides CamelBlueprintTestSupport.addServicesOnStartup() to
     * define any necessary OSGi services required for use in the testing.
     * @param services
     */
    @Override
    protected void addServicesOnStartup(Map<String, KeyValueHolder<Object, Dictionary>> services) {
	services.put(ConnectCorePropertiesService.class.getName(), asService(new MockConnectCoreProperties(), null));
    }

    @Test
    public void testPostMethod() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Contact mockObjToAdd = createMockTFunction("1");
        mockObjToAdd.setCustrefnum(null);
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
        
        //Create the mock object to return from the endpoint for testing
        
        Contact mockObjToReturn = createMockTFunction("2");
        Optional optionalObjToReturn = Optional.of(mockObjToReturn);
        
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        mockUnderlyingRouteEndpoint.expectedHeaderReceived("custRefNum", MOCK_CUSTOMER_FOR_CONTACT);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Contact receivedObj = exchange.getIn().getBody(Contact.class);
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setBody(optionalObjToReturn);
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        Contact returnedT = new Gson().fromJson(httpJsonResponse.getJsonResponse(), Contact.class);
        
        assertMockEndpointsSatisfied();
        assertEquals(200, httpJsonResponse.getHttpResponseCode());
        //Check the good return values are as expected
        assertObjectsEqualFunction(mockObjToReturn, returnedT);
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPostMethodExceptionResponse() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Contact mockObjToAdd = createMockTFunction("1");
        mockObjToAdd.setCustrefnum(null);
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
        
        //Create the mock object to return from the endpoint for testing
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        mockUnderlyingRouteEndpoint.expectedHeaderReceived("custRefNum", MOCK_CUSTOMER_FOR_CONTACT);
        ConnectBusinessException mockBusinessException = new ConnectBusinessException("mockBusinessException");
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Contact receivedObj = exchange.getIn().getBody(Contact.class);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            throw mockBusinessException;
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        assertEquals(mockBusinessException.getMessage(), errorResponse.getMessage());
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPostMethodEmptyResponse() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        Contact mockObjToAdd = createMockTFunction("1");
        mockObjToAdd.setCustrefnum(null);
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        mockUnderlyingRouteEndpoint.expectedHeaderReceived("custRefNum", MOCK_CUSTOMER_FOR_CONTACT);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Contact receivedObj = exchange.getIn().getBody(Contact.class);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(Optional.empty());
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        assertEquals(404, errorResponse.getHttpErrorCode());
        assertEquals("Entity Not Found.", errorResponse.getMessage());
        assertEquals(404, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPostMethodMalformedResponse() throws Exception {
        
                
        //Create the mock json to send to the endpoint for testing.
        Contact mockObjToAdd = createMockTFunction("1");
        mockObjToAdd.setCustrefnum(null);
        
        Gson gson = new Gson();
        String mockObjJson = gson.toJson(mockObjToAdd);
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        mockUnderlyingRouteEndpoint.expectedHeaderReceived("custRefNum", MOCK_CUSTOMER_FOR_CONTACT);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            Contact receivedObj = exchange.getIn().getBody(Contact.class);
            assertObjectsEqualFunction(mockObjToAdd, receivedObj);
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(new ArrayList<Object>());
        });
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        String expectedErrorMessagePrefix = "Invalid response from endpoint.";
        assertTrue("Expected message starting with [" + expectedErrorMessagePrefix + "] but was [" + errorResponse.getMessage() + "].", errorResponse.getMessage().startsWith(expectedErrorMessagePrefix));
        assertEquals(500, errorResponse.getHttpErrorCode());
        assertEquals(500, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
    
    @Test
    public void testPostMethodBadInput() throws Exception {
        
                
        //Create the mock object json to send to the endpoint for testing.
        String mockObjJson = "{somebadjson: [[]]]]}}}}}";
        
        //test a good message
        StringEntity contentEntity = new StringEntity(mockObjJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
        String expectedErrorMessagePrefix = "com.google.gson.stream.MalformedJsonException";
        assertTrue("Expected message starting with [" + expectedErrorMessagePrefix + "] but was [" + errorResponse.getMessage() + "].", errorResponse.getMessage().startsWith(expectedErrorMessagePrefix));
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }

    // @Test
    // public void testPostMethodNoInput() throws Exception {
        
                
    //     //Create the mock object json to send to the endpoint for testing.
    //     String mockObjJson = "";
        
    //     //test a good message
    //     StringEntity contentEntity = new StringEntity(mockObjJson);
    //     httpPost.setEntity(contentEntity);
    //     HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
    //     System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
    //     RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
    //     assertMockEndpointsSatisfied();
    //     assertEquals("Entity Not Provided.", errorResponse.getMessage());
    //     assertEquals(400, errorResponse.getHttpErrorCode());
    //     assertEquals(400, httpJsonResponse.getHttpResponseCode());
    //     assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    // }
    
    @Test
    public void testPostMethodBadEnumVal() throws Exception {
                        
        String mockCustRefNum = "mockCustRefNum";
        String mockAddrRefNum = "mockAddrRefNum";
        String mockPrimaryInd = "mockPrimaryInd";
        String mockBadEnumVal = "BAD";
        
        String badEnumJson = "{\n"
            + "\"custrefnum\": \"" + mockCustRefNum + "\",\n" +
            "  \"addrrefnum\": \"" + mockAddrRefNum + "\",\n" +
            "  \"primaryind\": \"" + mockPrimaryInd + "\",\n" +
            "  \"typecd\": \"" + mockBadEnumVal + "\"\n"
            + "}";
                
        mockUnderlyingRouteEndpoint.reset();
        
        mockUnderlyingRouteEndpoint.expectedMessageCount(0);
                        
        //test a bad enum message
        StringEntity contentEntity = new StringEntity(badEnumJson);
        httpPost.setEntity(contentEntity);
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpPost);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
        
        assertMockEndpointsSatisfied();
                
        String expectedErrorMessage = "[JsonParseException] Error deserializing Json enum: No enum constant "
                + "com.configureone.integration.connect.core.model.Contact.TypeCode." + mockBadEnumVal;
        assertEquals(expectedErrorMessage, errorResponse.getMessage());
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }

    
    public HttpJsonResponse makeJsonHttpCall(HttpUriRequest uriRequest) throws IOException {
        HttpJsonResponse httpJsonResponse = new HttpJsonResponse();
        try (CloseableHttpResponse response = httpClient.execute(uriRequest)) {
            InputStream responseStream = response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            httpJsonResponse.setJsonResponse(writer.toString());
            httpJsonResponse.setHttpResponseCode(response.getStatusLine().getStatusCode());
            httpJsonResponse.setHeaders(response.getAllHeaders());
        }
        return httpJsonResponse;
    }
}
