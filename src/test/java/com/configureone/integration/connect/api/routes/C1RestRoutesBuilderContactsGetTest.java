package com.configureone.integration.connect.api.routes;

import com.configureone.integration.connect.core.model.ConnectBusinessException;
import com.configureone.integration.connect.core.model.Contact;
import com.configureone.integration.connect.core.model.RestErrorResponse;
import com.configureone.integration.connect.core.services.ConnectCorePropertiesService;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.apache.camel.util.KeyValueHolder;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author cloud
 */
public class C1RestRoutesBuilderContactsGetTest extends CamelBlueprintTestSupport {

    public static final String CONNECT_C1APIGATEWAY_HOST = "localhost";
    public static final String UNDERLYING_ROUTE_ID = "getContactByAddressRefNum";
    public static final String UNDERLYING_ROUTE_URI = "direct-vm:getContactByAddressRefNum";
    public static final String MOCK_OBJECT_ID = "mockObjID";
    public static final String MOCK_PARENT_OBJECT_ID = "mockParentObjID";
    public static final String API_OBJECT_PATH = "/c1/rest/customers/" + MOCK_PARENT_OBJECT_ID + "/contacts/" + MOCK_OBJECT_ID;
    public static final String OBJECT_ID_HEADER_NAME = "contactRefNum";
    public static final Class<Contact> OBJECT_CLASS = Contact.class;
    private CloseableHttpClient httpClient;
    MockEndpoint mockUnderlyingRouteEndpoint;
    HttpGet httpGet;
 
    public void assertObjectsEqualFunction(Contact expectedT, Contact actualT) {
        assertEquals(expectedT.getAddressline1(), actualT.getAddressline1());
        assertEquals(expectedT.getAddressline2(), actualT.getAddressline2());
        assertEquals(expectedT.getAddressline3(), actualT.getAddressline3());
        assertEquals(expectedT.getAddrrefnum(), actualT.getAddrrefnum());
        assertEquals(expectedT.getCity(), actualT.getCity());
        assertEquals(expectedT.getContactname(), actualT.getContactname());
        assertEquals(expectedT.getCountry(), actualT.getCountry());
        assertEquals(expectedT.getCrmcontactrefnum(), actualT.getCrmcontactrefnum());
        assertEquals(expectedT.getCustrefnum(), actualT.getCustrefnum());
        assertEquals(expectedT.getEmailaddress(), actualT.getEmailaddress());
        assertEquals(expectedT.getErpcontactrefnum(), actualT.getErpcontactrefnum());
        assertEquals(expectedT.getFaxnumber(), actualT.getFaxnumber());
        assertEquals(expectedT.getPhonenumber(), actualT.getPhonenumber());
        assertEquals(expectedT.getPostalcode(), actualT.getPostalcode());
        assertEquals(expectedT.getPrimaryind(), actualT.getPrimaryind());
        assertEquals(expectedT.getState(), actualT.getState());
        assertEquals(expectedT.getTypecd(), actualT.getTypecd());
    }
    public Contact createMockTFunction(String suffix) {
        Contact contact = new Contact();
        contact.setAddressline1("mockAddr1");
        contact.setAddressline2("mockAddr2");
        contact.setAddressline3("mockAddr3");
        contact.setAddrrefnum("mockAddrRefNum");
        contact.setCity("mockCity");
        contact.setContactname("mockContact");
        contact.setCountry("mockCountry");
        contact.setCrmcontactrefnum("mockCrmCRef");
        contact.setCustrefnum("mockCustRefNum");
        contact.setEmailaddress("mockEmail");
        contact.setErpcontactrefnum("mockERPRefNum");
        contact.setFaxnumber("mockFax");
        contact.setPhonenumber("mockPhone");
        contact.setPostalcode("mockPostal");
        contact.setPrimaryind("mockPrimaryInd");
        contact.setState("mockState");
        //contact.setTypecd(Contact.TypeCode.B);
        return contact;
    }
 
    @Override
    public boolean isCreateCamelContextPerClass() {
        return false;
    }
 
    @Override
    protected Properties useOverridePropertiesWithPropertiesComponent() {
        Properties prop = new Properties();
        //This property needs to be defined here since it is used with the initial Rest component configuration.
        prop.put("c1.rest.port", "8090");
        return prop;
    }

    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

    /**
     * Ensures that only the c1apigateway Camel context starts up so that
     * we don't test the wrong context.  CamelBlueprintTestSupport can only
     * test one context.
     * 
     * @return the bundle filter
     */
    @Override
    protected String getBundleFilter() {
        return "(&(Bundle-SymbolicName=*)(!(Bundle-SymbolicName=com.configureone.integration.connect.core*)))";
    }
 
    @Before
    public void setUpTests() throws Exception {
        super.setUp();
        if (httpClient == null) {
            httpClient = HttpClients.createDefault();
        }
        //Replace the get quote endpoint at the end of the route with a mock one
        context.getRouteDefinition(UNDERLYING_ROUTE_ID).adviceWith (
                context, new AdviceWithRouteBuilder() {
            
                @Override
                public void configure() throws Exception {
                    mockEndpointsAndSkip(UNDERLYING_ROUTE_URI);
                }
        });
        context.start();
        mockUnderlyingRouteEndpoint = getMockEndpoint("mock:" + UNDERLYING_ROUTE_URI);
        String restPort = context.resolvePropertyPlaceholders("{{c1.rest.port}}");
        httpGet = new HttpGet("http://" + CONNECT_C1APIGATEWAY_HOST + ":" + restPort + API_OBJECT_PATH);
    }

    @After
    public void closeHttpClient() throws IOException, Exception {
        if (httpClient != null) {
            httpClient.close();
            httpClient = null;
        }
    }

    /**
     * Overrides CamelBlueprintTestSupport.addServicesOnStartup() to
     * define any necessary OSGi services required for use in the testing.
     * @param services
     */
    @Override
    protected void addServicesOnStartup(Map<String, KeyValueHolder<Object, Dictionary>> services) {
	services.put(ConnectCorePropertiesService.class.getName(), asService(new MockConnectCoreProperties(), null));
    }

    @Test
    public void testGetMethod() throws Exception {
        System.out.println("\ntestGetMethod");
        //Create the mock object to return from the endpoint for testing
        Contact mockObjToReturn = createMockTFunction("2");
        Optional optionalObjToReturn = Optional.of(mockObjToReturn);
 
        mockUnderlyingRouteEndpoint.reset();
 
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            assertEquals(MOCK_OBJECT_ID, exchange.getIn().getHeader(OBJECT_ID_HEADER_NAME));
            exchange.getIn().setBody(optionalObjToReturn);
        });
 
        //test a good message
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpGet);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        Contact returnedT = new Gson().fromJson(httpJsonResponse.getJsonResponse(), OBJECT_CLASS);
 
        assertMockEndpointsSatisfied();
        assertEquals(200, httpJsonResponse.getHttpResponseCode());
        //Check the good return values are as expected
        assertObjectsEqualFunction(mockObjToReturn, returnedT);
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
 
    @Test
    public void testGetMethodExceptionResponse() throws Exception {
        System.out.println("\ntestGetMethodExceptionResponse");
        //Create the mock object to return from the endpoint for testing
 
        mockUnderlyingRouteEndpoint.reset();

        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        ConnectBusinessException mockBusinessException = new ConnectBusinessException("mockBusinessException");
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            throw mockBusinessException;
        });
 
        //test a good message
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpGet);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
 
        assertMockEndpointsSatisfied();
        assertEquals(mockBusinessException.getMessage(), errorResponse.getMessage());
        assertEquals(400, errorResponse.getHttpErrorCode());
        assertEquals(400, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
 
    @Test
    public void testGetMethodEmptyResponse() throws Exception {
        System.out.println("\ntestGetMethodEmptyResponse");
        mockUnderlyingRouteEndpoint.reset();
 
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(Optional.empty());
        });
 
        //test a good message
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpGet);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
 
        assertMockEndpointsSatisfied();
        assertEquals(404, errorResponse.getHttpErrorCode());
        assertEquals("Entity Not Found.", errorResponse.getMessage());
        assertEquals(404, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
 
    @Test
    public void testGetMethodMalformedResponse() throws Exception {
        System.out.println("\ntestGetMethodMalformedResponse");
        mockUnderlyingRouteEndpoint.reset();
 
        mockUnderlyingRouteEndpoint.expectedMessageCount(1);
        //Assert good data request matches for 1st message
        mockUnderlyingRouteEndpoint.whenAnyExchangeReceived((Exchange exchange) -> {
            exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/xml");
            exchange.getIn().setBody(new ArrayList<>());
        });
 
        //test a good message
        HttpJsonResponse httpJsonResponse = makeJsonHttpCall(httpGet);
        System.out.print("RESPONSECONTENT: " + httpJsonResponse.getJsonResponse());
        RestErrorResponse errorResponse = new Gson().fromJson(httpJsonResponse.getJsonResponse(), RestErrorResponse.class);
 
        assertMockEndpointsSatisfied();
        String expectedErrorMessagePrefix = "Invalid response from endpoint.";
        assertTrue("Expected message starting with [" + expectedErrorMessagePrefix + "] but was [" + errorResponse.getMessage() + "].", errorResponse.getMessage().startsWith(expectedErrorMessagePrefix));
        assertEquals(500, errorResponse.getHttpErrorCode());
        assertEquals(500, httpJsonResponse.getHttpResponseCode());
        assertEquals("application/json", httpJsonResponse.getHeadersMap().get(Exchange.CONTENT_TYPE));
    }
 
    public HttpJsonResponse makeJsonHttpCall(HttpUriRequest uriRequest) throws IOException {
        HttpJsonResponse httpJsonResponse = new HttpJsonResponse();
        try (CloseableHttpResponse response = httpClient.execute(uriRequest)) {
            InputStream responseStream = response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(responseStream, writer, StandardCharsets.UTF_8.name());
            httpJsonResponse.setJsonResponse(writer.toString());
            httpJsonResponse.setHttpResponseCode(response.getStatusLine().getStatusCode());
            httpJsonResponse.setHeaders(response.getAllHeaders());
        }
        return httpJsonResponse;
    }
}
