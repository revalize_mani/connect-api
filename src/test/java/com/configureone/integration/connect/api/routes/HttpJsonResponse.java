/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.configureone.integration.connect.api.routes;

import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;

/**
 *
 * @author rmoutray
 */
public class HttpJsonResponse {
    private int httpResponseCode;
    private String jsonResponse;
    private Header[] headers;
    private Map<String, String> headersMap = new HashMap<>();

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public String getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    void setHeaders(Header[] allHeaders) {
        this.headers = allHeaders;
        for (Header header : this.headers) {
            headersMap.put(header.getName(), header.getValue());
        }
    }
    
    public Map<String, String> getHeadersMap() {
        return this.headersMap;
    }
}
